﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction.DataDefinitionModel;

namespace SolvesIT.DbAbstraction
{
    public interface IDdService
    {
        Task<DdCatalog> LoadDatabaseAsync(SqlConnection sqlConnection);

        Task DropAllExceptTables(SqlConnection sqlConnection);
    }
}
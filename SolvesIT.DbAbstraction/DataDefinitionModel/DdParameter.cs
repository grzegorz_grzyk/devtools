﻿using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public class DdParameter<TParent> : DdObject<TParent>
        where TParent : DdObject<DdSchema>, IHaveParameters<TParent>
    {
        public DdParameter(string name)
            : base(name)
        {
        }

        /// <summary>
        /// ID of the type of the parameter (sys.types.user_type_id).
        /// </summary>
        public int TypeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public DdType Type
        {
            get { return Parent.Parent.Types.Single(t => t.Id == TypeId); }
        }

        /// <summary>
        ///  Maximum length (in bytes) of the column.
        /// -1 = Column data type is varchar(max), nvarchar(max), varbinary(max), or xml.
        /// For text columns, the max_length value will be 16 or the value set by sp_tableoption 'text in row'.
        /// </summary>
        public short MaxLength { get; set; }

        /// <summary>
        /// Precision of the column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Precision { get; set; }

        /// <summary>
        /// Scale of column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Scale { get; set; }

        /// <summary>
        /// 1 = Parameter is OUTPUT or RETURN; otherwise, 0.
        /// </summary>
        public bool IsOutput { get; set; }

        /// <summary>
        /// 1 = Parameter has default value.
        /// </summary>
        public bool HasDefaultValue { get; set; }

        /// <summary>
        /// 1 = Content is a complete XML document.
        /// 0 = Content is a document fragment, or the data type of the column is not xml.
        /// </summary>
        public bool IsXmlDocument { get; set; }

        /// <summary>
        /// If has_default_value is 1, the value of this column is the value of the default for the parameter; otherwise, NULL.
        /// </summary>
        public object DefaultValues { get; set; }

        /// <summary>
        /// 1 = Parameter is READONLY; otherwise, 0.
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// 1 = Parameter is nullable. (the default).
        /// </summary>
        public bool IsNullable { get; set; }
    }
}
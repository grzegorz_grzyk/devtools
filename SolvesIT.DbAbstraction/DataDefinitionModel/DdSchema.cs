﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdSchema : DdObject<DdCatalog>
    {
        public DdSchema(string name)
            : base(name)
        {
            Functions = Enumerable.Empty<DdFunction>();
            Procedures = Enumerable.Empty<DdProcedure>();
            Tables = Enumerable.Empty<DdTable>();
            TableTypes = Enumerable.Empty<DdTableType>();
            Types = Enumerable.Empty<DdType>();
            Views = Enumerable.Empty<DdView>();
        }

        public IEnumerable<DdForeignKey> ForeignKeys
        {
            get { return Tables.SelectMany(t => t.ForeignKeys).ToList(); }
        }

        public IEnumerable<DdFunction> Functions { get; set; }

        public IEnumerable<DdIndex> Indexes
        {
            get { return Tables.SelectMany(t => t.Indexes).ToList(); }
        }

        public IEnumerable<DdProcedure> Procedures { get; set; }

        public IEnumerable<DdTable> Tables { get; set; }

        public IEnumerable<DdTableType> TableTypes { get; set; }

        public IEnumerable<DdType> Types { get; set; }

        public IEnumerable<DdView> Views { get; set; }
    }
}
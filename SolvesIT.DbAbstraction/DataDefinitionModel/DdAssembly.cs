﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdAssembly
        : DdObject<DdCatalog>
    {
        public DdAssembly(string name)
            : base(name)
        {
            AssemblyFiles = Enumerable.Empty<DdAssemblyFile>();
        }

        public int ReferenceAssemblyId { get; set; }

        public DdAssembly ReferenceAssembly
        {
            get
            {
                return
                    Parent.Assemblies
                        .Single(a => a.Id == ReferenceAssemblyId);
            }
        }

        public string Permission { get; set; }

        public bool IsVisible { get; set; }

        public int PrincipalId { get; set; }

        public DdPrincipal Principal
        {
            get
            {
                return
                    Parent.Principals
                        .Single(a => a.Id == PrincipalId);
            }
        }

        public IEnumerable<DdAssemblyFile> AssemblyFiles { get; set; }
    }
}

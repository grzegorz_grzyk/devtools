﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public enum IndexType
    {
        Undefined = 0,
        Heap,
        Clustered,
        Nonclustered,
        Xml,
        Spatial,
        ClusteredColumnstore,
        NonclusteredColumnstore,
        NonclusteredHash
    }
}

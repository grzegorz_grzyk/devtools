﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdFunction : DdObject<DdSchema>, IHaveParameters<DdFunction>
    {
        public DdFunction(string name)
            : base(name)
        {
            Parameters = Enumerable.Empty<DdParameter<DdFunction>>();
        }

        public string FunctionType { get; set; }

        public int AssemblyId { get; set; }

        public DdAssembly Assembly
        {
            get
            {
                return Parent.Parent.Assemblies.Single(a => a.Id == AssemblyId);
            }
        }

        public string AssemblyClass { get; set; }

        public string AssemblyMethod { get; set; }

        public IEnumerable<DdParameter<DdFunction>> Parameters { get; set; }

        public string Definition { get; set; }
    }
}
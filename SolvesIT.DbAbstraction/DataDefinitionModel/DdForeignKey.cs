﻿using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdForeignKey : DdObject<DdTable>
    {
        public DdForeignKey(string name)
            : base(name)
        {
        }

        public bool IsDisabled { get; set; }

        public DdColumn<DdTable> ParentColumn
        {
            get
            {
                return Parent.Columns.Single(c => c.Id == ParentColumnId);
            }
        }

        public int ParentColumnId { get; set; }

        public int ReferenceTableId { get; set; }

        public DdTable ReferenceTable
        {
            get { return Parent.Schema.Parent.Tables.Single(x => x.Id == ReferenceTableId); }
        }

        public int ReferenceColumnId { get; set; }

        public DdColumn<DdTable> ReferenceColumn
        {
            get
            {
                return ReferenceTable.Columns.Single(c => c.Id == ReferenceColumnId);
            }
        }
    }
}
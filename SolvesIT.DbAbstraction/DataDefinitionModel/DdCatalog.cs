﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdCatalog : DdObject
    {
        public DdCatalog(string name)
            : base(name)
        {
            Assemblies = Enumerable.Empty<DdAssembly>();
            Principals = Enumerable.Empty<DdPrincipal>();
            Schemata = Enumerable.Empty<DdSchema>();
        }

        public IEnumerable<DdAssembly> Assemblies { get; set; }

        public IEnumerable<DdPrincipal> Principals { get; set; }

        public IEnumerable<DdSchema> Schemata { get; set; }

        public IEnumerable<DdTable> Tables
        {
            get { return Schemata.SelectMany(s => s.Tables).ToList(); }
        }

        public IEnumerable<DdType> Types
        {
            get { return Schemata.SelectMany(s => s.Types).ToList(); }
        }
        public IEnumerable<DdView> Views
        {
            get { return Schemata.SelectMany(s => s.Views).ToList(); }
        }

        public IEnumerable<DdTableType> TableTypes
        {
            get { return Schemata.SelectMany(s => s.TableTypes).ToList(); }
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdTableType : DdObject<DdSchema>, IHaveColumns<DdTableType>
    {
        public DdTableType(string name)
            : base(name)
        {
            Columns = Enumerable.Empty<DdColumn<DdTableType>>();
        }

        public int? PrincipalId { get; set; }

        public DdPrincipal Principal
        {
            get { return Parent.Parent.Principals.SingleOrDefault(x => PrincipalId.HasValue && x.Id == PrincipalId.Value); }
        }

        public IEnumerable<DdColumn<DdTableType>> Columns { get; set; }

        public DdSchema Schema
        {
            get { return Parent; }
        }
    }
}
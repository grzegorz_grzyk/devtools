﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IDdColumn : IDdObject<IDdObject>
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Default")]
        string Default { get; }

        string Definition { get; }

        bool IsComputed { get; }

        bool IsIdentity { get; }

        bool IsNullable { get; }

        bool IsPersisted { get; }

        short MaxLength { get; }

        byte Precision { get; }

        byte Scale { get; }

        int TypeId { get; }

        bool IsUnicode { get; }

        DdType Type { get; }

        DdForeignKey ReferencedForeignKey { get; }
    }
}
﻿using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdPrincipal : DdObject<DdCatalog>
    {
        public DdPrincipal(string name)
            : base(name)
        {
        }

        public int SchemaId { get; set; }

        public DdSchema DefaultSchema
        {
            get
            {
                return Parent.Schemata
                    .Single(s => s.Id == SchemaId);
            }
        }
    }
}

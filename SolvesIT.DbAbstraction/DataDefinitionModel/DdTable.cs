﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdTable : DdObject<DdSchema>, IHaveColumns<DdTable>, IHaveTrigger<DdTable>
    {
        public DdTable(string name)
            : base(name)
        {
            Columns = Enumerable.Empty<DdColumn<DdTable>>();
            ForeignKeys = Enumerable.Empty<DdForeignKey>();
            Indexes = Enumerable.Empty<DdIndex>();
        }

        public int? PrincipalId { get; set; }

        public DdPrincipal Principal
        {
            get { return Parent.Parent.Principals.SingleOrDefault(x => PrincipalId.HasValue && x.Id == PrincipalId.Value); }
        }

        public IEnumerable<DdColumn<DdTable>> Columns { get; set; }

        public IEnumerable<DdForeignKey> ForeignKeys { get; set; }

        public IEnumerable<DdIndex> Indexes { get; set; }

        public DdTable CustomFieldTargetTable
        {
            get
            {
                var referenceTableName = new Regex("CustomField$").Replace(Name, string.Empty);

                var fkey = ForeignKeys
                    .Where(fk => fk.ParentColumn.Name != "ChangerId")
                    .SingleOrDefault(fk => fk.ReferenceTable.Name == referenceTableName);
                return fkey != null ? fkey.ReferenceTable : null;
            }
        }

        public DdSchema Schema
        {
            get { return Parent; }
        }

        public IEnumerable<DdTrigger<DdTable>> Triggers { get; set; }
    }
}
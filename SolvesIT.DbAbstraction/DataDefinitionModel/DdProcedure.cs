﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdProcedure : DdObject<DdSchema>, IHaveParameters<DdProcedure>
    {
        public DdProcedure(string name)
            : base(name)
        {
            Parameters = Enumerable.Empty<DdParameter<DdProcedure>>();
        }

        public IEnumerable<DdParameter<DdProcedure>> Parameters { get; set; }

        public string Definition { get; set; }
    }
}
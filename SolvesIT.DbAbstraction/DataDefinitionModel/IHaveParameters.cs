﻿using System.Collections.Generic;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IHaveParameters<TSelf>
        where TSelf : DdObject<DdSchema>, IHaveParameters<TSelf>
    {
        IEnumerable<DdParameter<TSelf>> Parameters { get; set; }
    }
}

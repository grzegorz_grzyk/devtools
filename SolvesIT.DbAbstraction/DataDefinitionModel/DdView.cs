﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdView : DdObject<DdSchema>, IHaveColumns<DdView>, IHaveTrigger<DdView>
    {
        public DdView(string name)
            : base(name)
        {
            Columns = Enumerable.Empty<DdColumn<DdView>>();
            Indexes = Enumerable.Empty<DdIndex>();
            Triggers = Enumerable.Empty<DdTrigger<DdView>>();
        }

        public int? PrincipalId { get; set; }

        public DdPrincipal Principal
        {
            get { return Parent.Parent.Principals.SingleOrDefault(x => PrincipalId.HasValue && x.Id == PrincipalId.Value); }
        }

        public IEnumerable<DdColumn<DdView>> Columns { get; set; }

        public IEnumerable<DdIndex> Indexes { get; set; }

        public IEnumerable<DdTrigger<DdView>> Triggers { get; set; }
    }
}
﻿using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdType : DdObject<DdSchema>
    {
        public DdType(string name) : base(name)
        {
        }

        /// <summary>
        /// ID of the individual owner if different from schema owner. By default, schema-contained objects are owned by the schema owner. However, an alternate owner can be specified by using the ALTER AUTHORIZATION statement to change ownership.
        /// </summary>
        public int? PrincipalId { get; set; }

        public DdPrincipal Principal
        {
            get
            {
                return Parent.Parent.Principals
                    .SingleOrDefault(x => PrincipalId.HasValue && x.Id == PrincipalId);
            }
        }

        /// <summary>
        ///  Maximum length (in bytes) of the column.
        /// -1 = Column data type is varchar(max), nvarchar(max), varbinary(max), or xml.
        /// For text columns, the max_length value will be 16 or the value set by sp_tableoption 'text in row'.
        /// </summary>
        public short MaxLength { get; set; }

        /// <summary>
        /// Precision of the column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Precision { get; set; }

        /// <summary>
        /// Scale of column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Scale { get; set; }

        /// <summary>
        /// 1 = Column is nullable.
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// 1 = User-defined type. 
        /// 0 = SQL Server system data type.
        /// </summary>
        public bool IsUserDefined { get; set; }

        /// <summary>
        /// 1 = Implementation of the type is defined in a CLR assembly. 
        /// 0 = Type is based on a SQL Server system data type.
        /// </summary>
        public bool IsAssemblyType { get; set; }

        /// <summary>
        /// Default value.
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// Indicates the type is a table.
        /// </summary>
        public bool IsTableType { get; set; }

        /// <summary>
        /// If not null gets id of parent type. For user defined value types determines if it acutalu is eg. varchar or int etc.
        /// </summary>
        public byte? ParentTypeId { get; set; }
    }
}

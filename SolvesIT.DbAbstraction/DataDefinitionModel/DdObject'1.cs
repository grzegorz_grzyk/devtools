﻿using System.Diagnostics;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public abstract class DdObject<TParent> : DdObject, IDdObject<TParent>
        where TParent : IDdObject
    {
        protected DdObject(string name)
            : base(name)
        {
        }

        public TParent Parent { get; set; }
    }
}
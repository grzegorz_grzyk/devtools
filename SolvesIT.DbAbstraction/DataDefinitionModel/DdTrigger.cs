﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdTrigger<TParent> : DdObject<TParent>
        where TParent : DdObject<DdSchema>, IHaveTrigger<TParent>
    {
        public DdTrigger(string name)
            : base(name)
        {
        }

        public bool IsEnabled { get; set; }

        public bool IsInsteadOf { get; set; }
    }
}
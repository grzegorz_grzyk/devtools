﻿using System.Collections.Generic;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IHaveColumns<TSelf>
        where TSelf : IDdObject<DdSchema>, IHaveColumns<TSelf>
    {
        IEnumerable<DdColumn<TSelf>> Columns { get; set; }
    }
}

﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ddl")]
    public sealed class DdAssemblyFile : DdObject<DdAssembly>
    {
        public DdAssemblyFile(string name)
            : base(name)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public byte[] Content { get; set; }
    }
}

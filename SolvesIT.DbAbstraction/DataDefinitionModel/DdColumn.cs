﻿using System;
using System.Data;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public class DdColumn<TParent> : DdObject<TParent>, IDdColumn
        where TParent : IDdObject<DdSchema>, IHaveColumns<TParent>
    {
        public DdColumn(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Default value.
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// SQL text that defines this computed-column.
        /// </summary>
        public string Definition { get; set; }

        /// <summary>
        /// 1 = Column is a computed column.
        /// </summary>
        public bool IsComputed { get; set; }

        /// <summary>
        /// 1 = Column has identity values.
        /// </summary>
        public bool IsIdentity { get; set; }

        /// <summary>
        /// 1 = Column is nullable.
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// Computed column is persisted.
        /// </summary>
        public bool IsPersisted { get; set; }

        /// <summary>
        ///  Maximum length (in bytes) of the column.
        /// -1 = Column data type is varchar(max), nvarchar(max), varbinary(max), or xml.
        /// For text columns, the max_length value will be 16 or the value set by sp_tableoption 'text in row'.
        /// </summary>
        public short MaxLength { get; set; }

        /// <summary>
        /// Precision of the column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Precision { get; set; }

        /// <summary>
        /// Scale of column if numeric-based; otherwise, 0.
        /// </summary>
        public byte Scale { get; set; }

        /// <summary>
        /// ID of the type of the parameter (sys.types.user_type_id).
        /// </summary>
        public int TypeId { get; set; }

        public bool IsUnicode
        {
            get
            {
                return new[]
                {
                    SqlDbType.NText,
                    SqlDbType.NChar,
                    SqlDbType.NVarChar
                }.Any(x => string.Equals(x.ToString(), Type.Name, StringComparison.OrdinalIgnoreCase));
            }
        }

        ////public bool IsPartOfVersioning
        ////{
        ////    get { return Constraints.VersionedColumns.Any(x => x.Equals(this)); }
        ////}

        public DdType Type
        {
            get { return Parent.Parent.Parent.Types.Single(t => t.Id == TypeId); }
        }

        public DdForeignKey ReferencedForeignKey
        {
            get
            {
                var table = Parent as DdTable;
                if (table != null)
                    return table.ForeignKeys.SingleOrDefault(x => x.ParentColumnId == Id);
                return null;
            }
        }

        public override bool Equals(object obj)
        {
            var x = obj as DdColumn<TParent>;
            if (x == null)
                return false;

            return string.Equals(Name, x.Name, StringComparison.OrdinalIgnoreCase)
                   && string.Equals(Default, x.Default, StringComparison.OrdinalIgnoreCase)
                   && string.Equals(Definition, x.Definition, StringComparison.OrdinalIgnoreCase)
                   && IsComputed == x.IsComputed
                   && IsIdentity == x.IsIdentity
                   && IsNullable == x.IsNullable
                   && IsPersisted == x.IsPersisted
                   && MaxLength == x.MaxLength
                   && Precision == x.Precision
                   && Scale == x.Scale
                   && TypeId == x.TypeId;
        }

        public override int GetHashCode()
        {
            return (Parent.Id * 1000) + Id;
        }

        IDdObject IDdObject<IDdObject>.Parent
        {
            get { return Parent; }
        }
    }
}
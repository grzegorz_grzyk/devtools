﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IHaveIndex<TSelf>
        where TSelf : DdObject<DdSchema>, IHaveIndex<TSelf>
    {
        ////IEnumerable<DdlIndex<TSelf>> Indexes { get; set; }
    }
}

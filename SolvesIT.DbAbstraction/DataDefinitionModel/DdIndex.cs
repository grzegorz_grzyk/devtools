﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdIndex : DdObject<DdTable>
    {
        public DdIndex(string name)
            : base(name)
        {
            IndexColumns = Enumerable.Empty<DdIndexColumn>();
        }

        /// <summary>
        /// Type of index.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        public IndexType Type { get; set; }

        /// <summary>
        /// Description of index type.
        /// </summary>
        public string TypeDescription { get; set; }

        /// <summary>
        /// 1 = Index is unique.
        /// Always 0 for clustered columnstore indexes.
        /// </summary>
        public bool IsUnique { get; set; }

        /// <summary>
        /// 1 = Index is part of a PRIMARY KEY constraint.
        /// Always 0 for clustered columnstore indexes.
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// 1 = Index is part of a UNIQUE constraint.
        /// Always 0 for clustered columnstore indexes.
        /// </summary>
        public bool IsUniqueConstraint { get; set; }

        /// <summary>
        /// 1 = Index is disabled.
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 1 = Index has a filter and only contains rows that satisfy the filter definition.
        /// </summary>
        public bool HasFilter { get; set; }

        /// <summary>
        /// Expression for the subset of rows included in the filtered index.
        /// NULL for heap or non-filtered index.
        /// </summary>
        public string FilterDefinition { get; set; }

        public IEnumerable<DdIndexColumn> IndexColumns { get; set; }
    }
}
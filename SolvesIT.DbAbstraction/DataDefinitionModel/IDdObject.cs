﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IDdObject
    {
        int Id { get; set; }

        string Name { get; }
    }

    public interface IDdObject<out TParent>
        : IDdObject
        where TParent : IDdObject
    {
        TParent Parent { get; }
    }
}
﻿using System.Diagnostics;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    [DebuggerDisplay("{Name}")]
    public abstract class DdObject : IDdObject
    {
        protected DdObject(string name)
        {
            Name = name;
        }

        /// <summary>
        /// ID of the object.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the column. Is unique within the object.
        /// </summary>
        public string Name { get; private set; }
    }
}
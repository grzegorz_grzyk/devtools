﻿namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public sealed class DdIndexColumn : DdObject<DdIndex>
    {
        public DdIndexColumn(string name)
            : base(name)
        {
        }

        /// <summary>
        /// ID of the column in object_id. 
        /// 0 = Row Identifier (RID) in a nonclustered index.
        /// </summary>
        public int ColumnId { get; set; }

        ////public DdlColumn Column
        ////{
        ////    get { return Parent.Parent.Columns.Single(x => x.Id == ColumnId); }
        ////}

        /// <summary>
        /// 0 = Index key column has an ascending sort direction, or the column is part of a columnstore or hash index.
        /// </summary>
        public bool IsDescendingKey { get; set; }

        /// <summary>
        /// 1 = Column is a nonkey column added to the index by using the CREATE INDEX INCLUDE clause, or the column is part of a columnstore index.
        /// </summary>
        public bool IsIncludedColumn { get; set; }
    }
}
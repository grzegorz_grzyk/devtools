﻿using System.Collections.Generic;

namespace SolvesIT.DbAbstraction.DataDefinitionModel
{
    public interface IHaveTrigger<TSelf>
        where TSelf : DdObject<DdSchema>, IHaveTrigger<TSelf>
    {
        IEnumerable<DdTrigger<TSelf>> Triggers { get; set; }
    }
}

﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities.Tests.ReadersTests
{
    [TestClass]
    public class CatalogReadersTests : DdObjectsReadersBase
    {
        protected override string InitSqlScript
        {
            get { return string.Empty; }
        }

        protected override string ReadParentQuery
        {
            get { return string.Empty; }
        }

        [TestMethod]
        public async Task LoadShouldReadIdAndNameOfCurrentDatabaseAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await LocalDbService.InitializeDbAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();
                ICatalogReader catalogReader = new CatalogReader(MockRepository.Object);
                var result = await catalogReader.LoadAsync(sqlConnection);

                (result.Id > 0).ShouldBeTrue("Database Id should be greather than 0.");
                result.Name.ShouldBeEqual(databaseName);
            }
        }

        [TestMethod]
        public async Task LoadShouldRunSubReadersAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await LocalDbService.InitializeDbAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();
                ICatalogReader catalogReader = new CatalogReader(MockRepository.Object);
                var catalog = await catalogReader.LoadAsync(sqlConnection);

                MockRepository.Verify(x => x.LoadAsync<DdCatalog, DdAssembly>(sqlConnection, catalog));
                MockRepository.Verify(x => x.LoadAsync<DdCatalog, DdPrincipal>(sqlConnection, catalog));
                MockRepository.Verify(x => x.LoadAsync<DdCatalog, DdSchema>(sqlConnection, catalog));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdType>(sqlConnection, It.Is((DdSchema z) => z.Name == "dbo")));
            }
        }
    }
}
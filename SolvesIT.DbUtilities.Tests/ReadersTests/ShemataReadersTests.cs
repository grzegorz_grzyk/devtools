﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities.Tests.ReadersTests
{
    [TestClass]
    public class ShemataReadersTests : DdObjectsReadersBase
    {
        protected override string InitSqlScript
        {
            get
            {
                return @"
CREATE SCHEMA [xxxx];
";
            }
        }

        protected override string ReadParentQuery
        {
            get
            {
                return @"
SELECT
    CAST(DB_ID() AS INT) AS [Id]
  , DB_NAME() As [Name]
";
            }
        }

        [TestMethod]
        public async Task LoadShouldReadOnlyUserCreatedSchemataAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await PrepareDatabaseAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();

                var parent = await ReadParentAsync<DdCatalog>(sqlConnection);

                IDdObjectReader schemaReader = new SchemaReader(MockRepository.Object);

                await schemaReader.LoadAsync(
                    sqlConnection,
                    parent);

                parent.Schemata.Count().ShouldBeEqual(1);
                parent.Schemata.First().Name.ShouldBeEqual("xxxx");
            }
        }

        [TestMethod]
        public async Task LoadShouldRunSubReadersAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await PrepareDatabaseAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();

                var parent = await ReadParentAsync<DdCatalog>(sqlConnection);

                IDdObjectReader schemaReader = new SchemaReader(MockRepository.Object);

                await schemaReader.LoadAsync(
                    sqlConnection,
                    parent);

                var schema = parent.Schemata.First();
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdFunction>(sqlConnection, schema));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdProcedure>(sqlConnection, schema));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdTable>(sqlConnection, schema));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdTableType>(sqlConnection, schema));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdType>(sqlConnection, schema));
                MockRepository.Verify(x => x.LoadAsync<DdSchema, DdView>(sqlConnection, schema));
            }
        }
    }
}
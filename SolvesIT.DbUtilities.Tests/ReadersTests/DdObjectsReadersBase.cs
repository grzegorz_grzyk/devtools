﻿using System;
using System.Data.SqlClient;
using System.Data.SqlLocalDb;
using System.Threading.Tasks;
using CommonServiceLocator;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Utilities.Ioc.Unity;
using SolvesIT.Common;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using Unity;
using Unity.Lifetime;
using Unity.ServiceLocation;

namespace SolvesIT.DbUtilities.Tests.ReadersTests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable"), TestClass]
    public abstract class DdObjectsReadersBase
    {
        private UnityContainer _container;

        protected ILocalDbService LocalDbService { get; private set; }

        protected Mock<IDdRepository> MockRepository { get; private set; }

        protected abstract string InitSqlScript { get; }

        protected abstract string ReadParentQuery { get; }

        [TestInitialize]
        public void Init()
        {
            _container = new UnityContainer();
            var locator = _container.Resolve<UnityServiceLocator>();
            ServiceLocator.SetLocatorProvider(() => locator);

            var registrar = new UnityRegistrar(_container);
            registrar.EnableEnumerableResolution();

            var solvesitCommon = new Common.Bootstrapper();
            solvesitCommon.Bootstrap(registrar);

            registrar.RegisterSingle<ISqlLocalDbApi, SqlLocalDbApiWrapper>();
            registrar.RegisterSingle<ISqlLocalDbProvider, SqlLocalDbProvider>();

            registrar.RegisterAsSingleton<ILocalDbService, LocalDbService>();

            LocalDbService = _container.Resolve<ILocalDbService>();

            MockRepository = new Mock<IDdRepository>();
        }

        protected async Task PrepareDatabaseAsync(
            string databaseName)
        {
            await LocalDbService.InitializeDbAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();
                await sqlConnection.ExecuteScriptAsync(InitSqlScript);
            }
        }

        protected async Task<TParent> ReadParentAsync<TParent>(
            SqlConnection sqlConnection)
            where TParent : DdObject
        {
            using (var command = new SqlCommand(
                ReadParentQuery,
                sqlConnection))
            {
                using (var reader = await command.ExecuteReaderAsync())
                {
                    await reader.ReadAsync();
                    var parent = (TParent)Activator.CreateInstance(typeof(TParent), new[] { reader["Name"] });
                    parent.Id = (int)reader["Id"];
                    return parent;
                }
            }
        }
    }
}
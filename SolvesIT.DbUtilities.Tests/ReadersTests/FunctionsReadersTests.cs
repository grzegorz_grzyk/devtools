﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities.Tests.ReadersTests
{
    [TestClass]
    public class FunctionsReadersTests : DdObjectsReadersBase
    {
        protected override string InitSqlScript
        {
            get
            {
                return @"
CREATE SCHEMA [xxxx];
GO

CREATE Function [xxxx].[testfunction]
(
    @Param INT
)
RETURNS TABLE
AS
RETURN
    SELECT [a]
    FROM( VALUES( @Param ),
                ( 2 )) AS V( [a] );
GO

CREATE Function [dbo].[testfunction]
(
    @ParamY VARCHAR(10)
)
RETURNS TABLE
AS
RETURN
    SELECT [a]
    FROM( VALUES( @ParamY ),
                ( 'ulo' )) AS V( [a] );
";
            }
        }

        protected override string ReadParentQuery
        {
            get
            {
                return @"
SELECT
    [s].[schema_id] AS [Id]
  , [s].[name] AS [Name]
FROM [sys].[schemas] s
WHERE [s].[name] = 'xxxx'
";
            }
        }

        [TestMethod]
        public async Task LoadShouldReadOnlyFunctionsCreatedInGivenParentAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await PrepareDatabaseAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();

                var parent = await ReadParentAsync<DdSchema>(sqlConnection);

                IDdObjectReader functionReader = new FunctionReader(MockRepository.Object);

                await functionReader.LoadAsync(
                    sqlConnection,
                    parent);

                parent.Functions.Count().ShouldBeEqual(1);

                var function = parent.Functions.First();
                function.Name.ShouldBeEqual("testfunction");
                function.FunctionType.ShouldBeEqual("IF");
                function.AssemblyId.ShouldBeEqual(0);
                function.AssemblyClass.ShouldBeNull();
                function.AssemblyMethod.ShouldBeNull();
                function.Parameters.ShouldBeEqual(Enumerable.Empty<DdParameter<DdFunction>>());
                var expectedDefinition = @"
CREATE Function [xxxx].[testfunction]
(
    @Param INT
)
RETURNS TABLE
AS
RETURN
    SELECT [a]
    FROM( VALUES( @Param ),
                ( 2 )) AS V( [a] );
".Trim();
                function.Definition.Trim().ShouldBeEqual(expectedDefinition);
            }
        }

        [TestMethod]
        public async Task LoadShouldRunSubReadersAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await PrepareDatabaseAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();

                var parent = await ReadParentAsync<DdSchema>(sqlConnection);

                IDdObjectReader functionReader = new FunctionReader(MockRepository.Object);

                await functionReader.LoadAsync(
                    sqlConnection,
                    parent);

                var function = parent.Functions.First();
                MockRepository.Verify(x => x.LoadAsync<DdFunction, DdParameter<DdFunction>>(sqlConnection, function));
            }
        }
    }
}
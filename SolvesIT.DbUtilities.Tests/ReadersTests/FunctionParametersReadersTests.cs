﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities.Tests.ReadersTests
{
    [TestClass]
    public class FunctionParametersReadersTests : DdObjectsReadersBase
    {
        protected override string InitSqlScript
        {
            get
            {
                return @"
CREATE SCHEMA [xxxx];
GO

CREATE Function [xxxx].[testfunction]
(
    @Param1 INT,
    @param2 INT
)
RETURNS TABLE
AS
RETURN
    SELECT [a]
    FROM( VALUES( @Param1 ),
                ( @Param2 )) AS V( [a] );
";
            }
        }

        protected override string ReadParentQuery
        {
            get
            {
                return @"
SELECT
    [o].[object_id] AS [Id],
    [o].[Name] AS [Name]
FROM sys.objects as [o]
WHERE [o].[type] IN (N'AF', N'FN', N'FS', N'FT', N'IF', N'TF')
  AND [o].[Name] = 'testfunction'
";
            }
        }

        [TestMethod]
        public async Task LoadShouldReadFunctionParametersAsync()
        {
            var databaseName = Guid.NewGuid().ToString("N");
            await PrepareDatabaseAsync(databaseName);

            using (var sqlConnection = LocalDbService.GetDbConnection(databaseName))
            {
                sqlConnection.Open();

                var parent = await ReadParentAsync<DdFunction>(sqlConnection);

                IDdObjectReader functionReader = new ParameterReader<DdFunction, DdParameter<DdFunction>>(MockRepository.Object);

                await functionReader.LoadAsync(
                    sqlConnection,
                    parent);

                parent.Parameters.Count().ShouldBeEqual(2);

                var parameter = parent.Parameters.First();
                parameter.Name.ShouldBeEqual("@Param1");
                parameter.TypeId.ShouldBeEqual(56);
                parameter.MaxLength.ShouldBeEqual((short)4);
                parameter.Precision.ShouldBeEqual((byte)10);
                parameter.Scale.ShouldBeEqual((byte)0);
                parameter.IsOutput.ShouldBeFalse();
                parameter.HasDefaultValue.ShouldBeFalse();
                parameter.IsXmlDocument.ShouldBeFalse();
                parameter.DefaultValues.ShouldBeNull();
                parameter.IsReadOnly.ShouldBeFalse();
                parameter.IsNullable.ShouldBeTrue();
            }
        }
    }
}
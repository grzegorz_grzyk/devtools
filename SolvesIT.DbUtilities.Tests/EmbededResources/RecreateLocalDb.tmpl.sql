DECLARE @IsDbFileExists INT;
EXEC master.dbo.xp_fileexist '{1}', @IsDbFileExists OUTPUT

IF DB_ID('{0}') IS NOT NULL
BEGIN
    IF @IsDbFileExists = 1
    BEGIN
        ALTER DATABASE [{0}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
    END
    DROP DATABASE [{0}]
END
CREATE DATABASE [{0}] on (name='{0}', filename='{1}');
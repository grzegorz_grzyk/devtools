﻿using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SolvesIT.DbUtilities.Tests
{
    public interface ILocalDbService
    {
        Task InitializeDbAsync(string databaseName);

        SqlConnection GetDbConnection(string databaseName);
    }
}

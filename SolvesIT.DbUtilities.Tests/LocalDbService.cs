﻿using System;
using System.Data.SqlClient;
using System.Data.SqlLocalDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SolvesIT.Common;
using SolvesIT.DbUtilities.Tests.Properties;

namespace SolvesIT.DbUtilities.Tests
{
    internal class LocalDbService : ILocalDbService
    {
        private readonly ISqlLocalDbProvider _localDbProvider;
        private readonly IPathHelpers _pathHelpers;

        private ISqlLocalDbInstance _localDbInstance;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "LocalDb")]
        public LocalDbService(
            ISqlLocalDbApi localDbApi,
            ISqlLocalDbProvider localDbProvider,
            IPathHelpers pathHelpers)
        {
            _localDbProvider = localDbProvider;
            _pathHelpers = pathHelpers;

            if (!localDbApi.IsLocalDBInstalled() && _localDbProvider.GetVersions().All(v => v.Version.Major < 12))
            {
                throw new InvalidOperationException("SQL Server 2014 (or newer) Express LocalDb not installed");
            }
        }

        private ISqlLocalDbInstance GetInstance
        {
            get
            {
                if (_localDbInstance != null)
                {
                    return _localDbInstance;
                }

                _localDbInstance = _localDbProvider.GetOrCreateInstance(_pathHelpers.ApplicationName);

                var info = _localDbInstance.GetInstanceInfo();

                if (!info.IsRunning)
                    _localDbInstance.Start();

                _localDbInstance.Start();

                return _localDbInstance;
            }
        }

        public SqlConnection GetDbConnection(string databaseName)
        {
            var connectionStringBuilder = GetInstance.CreateConnectionStringBuilder();
            connectionStringBuilder.InitialCatalog = databaseName;
            connectionStringBuilder.MultipleActiveResultSets = true;
            var connectionString = connectionStringBuilder.ToString();
            return new SqlConnection(connectionString);
        }

        public async Task InitializeDbAsync(string databaseName)
        {
            using (var connection = GetInstance.CreateConnection())
            {
                await connection.OpenAsync();

                var databaseFilePath = Path.Combine(_pathHelpers.TempApplicationPath, databaseName + ".mdf");

                var query = string.Format(Resources.RecreateLocalDb_tmpl, databaseName, databaseFilePath);

                using (var command = new SqlCommand(query, connection))
                {
                    await command.ExecuteNonQueryAsync();
                }
            }
        }
    }
}
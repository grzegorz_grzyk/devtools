﻿
 
namespace SolvesIT.DbUtils.Tests.Integration.BasicEntities.Entities
{
	using SolvesIT.DbUtils.Tests.Integration.BasicEntities.ValueTypes;
	public class AWBuildVersionEntity
	{ 
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte SystemInformationID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Database_Version { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime VersionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class DatabaseLogEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 DatabaseLogID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime PostTime { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String DatabaseUser { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Event { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Schema { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Object { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String TSQL { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String XmlEvent { get; set; }
	}

	public class ErrorLogEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorLogID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ErrorTime { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String UserName { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorSeverity { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorState { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ErrorProcedure { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorLine { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ErrorMessage { get; set; }
	}

	public class UserEntityEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String Name { get; set; }
	}

	public class DepartmentEntity
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 DepartmentID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name GroupName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class EmployeeDepartmentHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 DepartmentID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ShiftID { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ShiftEntity
	{ 
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ShiftID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Time,True, 5)]
		
	public System.TimeSpan StartTime { get; set; }
		//[SqlType(System.Data.SqlDbType.Time,True, 5)]
		
	public System.TimeSpan EndTime { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class JobCandidateEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 JobCandidateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Resume { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class EmployeePayHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime RateChangeDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Rate { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte PayFrequency { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class EmployeeEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String NationalIDNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String LoginID { get; set; }
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId OrganizationNode { get; set; }
			private System.Int16 _OrganizationLevel;
			private bool _OrganizationLevel_wasInitialized;
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		//[SqlReadOnly]
		public System.Int16 OrganizationLevel 
		{ 
			get
			{
				return _OrganizationLevel; 
			}
			set
			{
				if(_OrganizationLevel_wasInitialized)
				{
					throw new System.InvalidOperationException("Property OrganizationLevel cannot be changed once initialized column is computed.");
				}
				_OrganizationLevel_wasInitialized = true;
				_OrganizationLevel = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String JobTitle { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime BirthDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String MaritalStatus { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Gender { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime HireDate { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag SalariedFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 VacationHours { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 SickLeaveHours { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag CurrentFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class BusinessEntityEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PasswordEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String PasswordHash { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String PasswordSalt { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class EmailAddressEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmailAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String EmailAddress { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PersonEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String PersonType { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public NameStyle NameStyle { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Title { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name FirstName { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name MiddleName { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name LastName { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Suffix { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmailPromotion { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String AdditionalContactInfo { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Demographics { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PersonPhoneEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 50)]
		
	public Phone PhoneNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PhoneNumberTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PhoneNumberTypeEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PhoneNumberTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class AddressEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String AddressLine1 { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String AddressLine2 { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String City { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String PostalCode { get; set; }
		//[SqlType(column.Type.Name, True, -1)]
		
	public System.Spatial.Geography SpatialLocation { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class BusinessEntityAddressEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CountryRegionEntity
	{ 
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class StateProvinceEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String StateProvinceCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag IsOnlyStateProvinceFlag { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class BusinessEntityContactEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ContactTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ContactTypeEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ContactTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class AddressTypeEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class BillOfMaterialsEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BillOfMaterialsID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductAssemblyID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ComponentID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 BOMLevel { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal PerAssemblyQty { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CultureEntity
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CultureID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class DocumentEntity
	{ 
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }
			private System.Int16 _DocumentLevel;
			private bool _DocumentLevel_wasInitialized;
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		//[SqlReadOnly]
		public System.Int16 DocumentLevel 
		{ 
			get
			{
				return _DocumentLevel; 
			}
			set
			{
				if(_DocumentLevel_wasInitialized)
				{
					throw new System.InvalidOperationException("Property DocumentLevel cannot be changed once initialized column is computed.");
				}
				_DocumentLevel_wasInitialized = true;
				_DocumentLevel = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Title { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Owner { get; set; }
		//[SqlType(System.Data.SqlDbType.Bit,True, 1)]
		
	public System.Boolean FolderFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String FileName { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String FileExtension { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Revision { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ChangeNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String DocumentSummary { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] Document { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class IllustrationEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 IllustrationID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Diagram { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class LocationEntity
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal CostRate { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal Availability { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductCategoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductCategoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductCostHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductDocumentEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductInventoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Shelf { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Bin { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductListPriceHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ListPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductModelIllustrationEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 IllustrationID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductPhotoEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductPhotoID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] ThumbNailPhoto { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ThumbnailPhotoFileName { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] LargePhoto { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String LargePhotoFileName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductProductPhotoEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductPhotoID { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag Primary { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductReviewEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductReviewID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name ReviewerName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ReviewDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String EmailAddress { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Rating { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Comments { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductSubcategoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductSubcategoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductCategoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ScrapReasonEntity
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrapReasonID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class TransactionHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TransactionID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderLineID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime TransactionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String TransactionType { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class TransactionHistoryArchiveEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TransactionID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderLineID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime TransactionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String TransactionType { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class UnitMeasureEntity
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class WorkOrderEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 WorkOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 OrderQty { get; set; }
			private System.Int32 _StockedQty;
			private bool _StockedQty_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		//[SqlReadOnly]
		public System.Int32 StockedQty 
		{ 
			get
			{
				return _StockedQty; 
			}
			set
			{
				if(_StockedQty_wasInitialized)
				{
					throw new System.InvalidOperationException("Property StockedQty cannot be changed once initialized column is computed.");
				}
				_StockedQty_wasInitialized = true;
				_StockedQty = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrappedQty { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrapReasonID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class WorkOrderRoutingEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 WorkOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OperationSequence { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ScheduledStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ScheduledEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ActualStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ActualEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal ActualResourceHrs { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal PlannedCost { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductDescriptionEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductDescriptionID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Description { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductModelProductDescriptionCultureEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductDescriptionID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CultureID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ProductNumber { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag MakeFlag { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag FinishedGoodsFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Color { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 SafetyStockLevel { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ReorderPoint { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardCost { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ListPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Size { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String SizeUnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String WeightUnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal Weight { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 DaysToManufacture { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String ProductLine { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Class { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Style { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductSubcategoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime SellStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime SellEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DiscontinuedDate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductModelEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String CatalogDescription { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Instructions { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class VendorEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 30)]
		
	public AccountNumber AccountNumber { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte CreditRating { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag PreferredVendorStatus { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag ActiveFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String PurchasingWebServiceURL { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ProductVendorEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AverageLeadTime { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal LastReceiptCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime LastReceiptDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MinOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MaxOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 OnOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PurchaseOrderDetailEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderDetailID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPrice { get; set; }
			private System.Decimal _LineTotal;
			private bool _LineTotal_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal LineTotal 
		{ 
			get
			{
				return _LineTotal; 
			}
			set
			{
				if(_LineTotal_wasInitialized)
				{
					throw new System.InvalidOperationException("Property LineTotal cannot be changed once initialized column is computed.");
				}
				_LineTotal_wasInitialized = true;
				_LineTotal = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal ReceivedQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal RejectedQty { get; set; }
			private System.Decimal _StockedQty;
			private bool _StockedQty_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		//[SqlReadOnly]
		public System.Decimal StockedQty 
		{ 
			get
			{
				return _StockedQty; 
			}
			set
			{
				if(_StockedQty_wasInitialized)
				{
					throw new System.InvalidOperationException("Property StockedQty cannot be changed once initialized column is computed.");
				}
				_StockedQty_wasInitialized = true;
				_StockedQty = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PurchaseOrderHeaderEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte RevisionNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmployeeID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 VendorID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime OrderDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ShipDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SubTotal { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal TaxAmt { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Freight { get; set; }
			private System.Decimal _TotalDue;
			private bool _TotalDue_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal TotalDue 
		{ 
			get
			{
				return _TotalDue; 
			}
			set
			{
				if(_TotalDue_wasInitialized)
				{
					throw new System.InvalidOperationException("Property TotalDue cannot be changed once initialized column is computed.");
				}
				_TotalDue_wasInitialized = true;
				_TotalDue = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ShipMethodEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ShipBase { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ShipRate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CustomerEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CustomerID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StoreID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
			private System.String _AccountNumber;
			private bool _AccountNumber_wasInitialized;
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		//[SqlReadOnly]
		public System.String AccountNumber 
		{ 
			get
			{
				return _AccountNumber; 
			}
			set
			{
				if(_AccountNumber_wasInitialized)
				{
					throw new System.InvalidOperationException("Property AccountNumber cannot be changed once initialized column is computed.");
				}
				_AccountNumber_wasInitialized = true;
				_AccountNumber = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CountryRegionCurrencyEntity
	{ 
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CreditCardEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CardType { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CardNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ExpMonth { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ExpYear { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CurrencyEntity
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CurrencyCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class CurrencyRateEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CurrencyRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime CurrencyRateDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String FromCurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String ToCurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal AverageRate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal EndOfDayRate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class PersonCreditCardEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesOrderDetailEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderDetailID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CarrierTrackingNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPriceDiscount { get; set; }
			private System.Decimal _LineTotal;
			private bool _LineTotal_wasInitialized;
		//[SqlType(column.Type.Name, True, 17)]
		//[SqlReadOnly]
		public System.Decimal LineTotal 
		{ 
			get
			{
				return _LineTotal; 
			}
			set
			{
				if(_LineTotal_wasInitialized)
				{
					throw new System.InvalidOperationException("Property LineTotal cannot be changed once initialized column is computed.");
				}
				_LineTotal_wasInitialized = true;
				_LineTotal = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesOrderHeaderSalesReasonEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesReasonID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesPersonQuotaHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime QuotaDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesQuota { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesPersonEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesQuota { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Bonus { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal CommissionPct { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesReasonEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesReasonID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name ReasonType { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesTerritoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Group { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal CostYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal CostLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesTaxRateEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesTaxRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte TaxType { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal TaxRate { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesOrderHeaderEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte RevisionNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime OrderDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ShipDate { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag OnlineOrderFlag { get; set; }
			private System.String _SalesOrderNumber;
			private bool _SalesOrderNumber_wasInitialized;
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		//[SqlReadOnly]
		public System.String SalesOrderNumber 
		{ 
			get
			{
				return _SalesOrderNumber; 
			}
			set
			{
				if(_SalesOrderNumber_wasInitialized)
				{
					throw new System.InvalidOperationException("Property SalesOrderNumber cannot be changed once initialized column is computed.");
				}
				_SalesOrderNumber_wasInitialized = true;
				_SalesOrderNumber = value;;
			}
		}
		//[SqlType(column.Type.Name, True, 50)]
		
	public OrderNumber PurchaseOrderNumber { get; set; }
		//[SqlType(column.Type.Name, True, 30)]
		
	public AccountNumber AccountNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CustomerID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesPersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BillToAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipToAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String CreditCardApprovalCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CurrencyRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SubTotal { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal TaxAmt { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Freight { get; set; }
			private System.Decimal _TotalDue;
			private bool _TotalDue_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal TotalDue 
		{ 
			get
			{
				return _TotalDue; 
			}
			set
			{
				if(_TotalDue_wasInitialized)
				{
					throw new System.InvalidOperationException("Property TotalDue cannot be changed once initialized column is computed.");
				}
				_TotalDue_wasInitialized = true;
				_TotalDue = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Comment { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SalesTerritoryHistoryEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ShoppingCartItemEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShoppingCartItemID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ShoppingCartID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DateCreated { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SpecialOfferEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Description { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal DiscountPct { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Type { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Category { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MinQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MaxQty { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class SpecialOfferProductEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class StoreEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesPersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Demographics { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}

	public class ExecuteScriptTestsEntity
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Id { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime Timestamp { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid TestUid { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Data { get; set; }
	}

 
}

namespace SolvesIT.DbUtils.Tests.Integration.BasicEntities.ViewTypes
{
	using SolvesIT.DbUtils.Tests.Integration.BasicEntities.ValueTypes;
	public class AWBuildVersionDto
	{ 
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte SystemInformationID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Database_Version { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime VersionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class DatabaseLogDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 DatabaseLogID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime PostTime { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String DatabaseUser { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Event { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Schema { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String Object { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String TSQL { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String XmlEvent { get; set; }
	}
	public class ErrorLogDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorLogID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ErrorTime { get; set; }
		//[SqlType(column.Type.Name, False, 256)]
		
	public System.String UserName { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorSeverity { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorState { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ErrorProcedure { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ErrorLine { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ErrorMessage { get; set; }
	}
	public class UserEntityDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String Name { get; set; }
	}
	public class DepartmentDto
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 DepartmentID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name GroupName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class EmployeeDepartmentHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 DepartmentID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ShiftID { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ShiftDto
	{ 
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ShiftID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Time,True, 5)]
		
	public System.TimeSpan StartTime { get; set; }
		//[SqlType(System.Data.SqlDbType.Time,True, 5)]
		
	public System.TimeSpan EndTime { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class JobCandidateDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 JobCandidateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Resume { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class EmployeePayHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime RateChangeDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Rate { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte PayFrequency { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class EmployeeDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String NationalIDNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String LoginID { get; set; }
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId OrganizationNode { get; set; }
			private System.Int16 _OrganizationLevel;
			private bool _OrganizationLevel_wasInitialized;
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		//[SqlReadOnly]
		public System.Int16 OrganizationLevel 
		{ 
			get
			{
				return _OrganizationLevel; 
			}
			set
			{
				if(_OrganizationLevel_wasInitialized)
				{
					throw new System.InvalidOperationException("Property OrganizationLevel cannot be changed once initialized column is computed.");
				}
				_OrganizationLevel_wasInitialized = true;
				_OrganizationLevel = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String JobTitle { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime BirthDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String MaritalStatus { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Gender { get; set; }
		//[SqlType(System.Data.SqlDbType.Date,True, 3)]
		
	public System.DateTime HireDate { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag SalariedFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 VacationHours { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 SickLeaveHours { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag CurrentFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class BusinessEntityDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PasswordDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String PasswordHash { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String PasswordSalt { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class EmailAddressDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmailAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String EmailAddress { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PersonDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String PersonType { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public NameStyle NameStyle { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Title { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name FirstName { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name MiddleName { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name LastName { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Suffix { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmailPromotion { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String AdditionalContactInfo { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Demographics { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PersonPhoneDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 50)]
		
	public Phone PhoneNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PhoneNumberTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PhoneNumberTypeDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PhoneNumberTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class AddressDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String AddressLine1 { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String AddressLine2 { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String City { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String PostalCode { get; set; }
		//[SqlType(column.Type.Name, True, -1)]
		
	public System.Spatial.Geography SpatialLocation { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class BusinessEntityAddressDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CountryRegionDto
	{ 
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class StateProvinceDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String StateProvinceCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag IsOnlyStateProvinceFlag { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class BusinessEntityContactDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ContactTypeID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ContactTypeDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ContactTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class AddressTypeDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AddressTypeID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class BillOfMaterialsDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BillOfMaterialsID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductAssemblyID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ComponentID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 BOMLevel { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal PerAssemblyQty { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CultureDto
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CultureID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class DocumentDto
	{ 
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }
			private System.Int16 _DocumentLevel;
			private bool _DocumentLevel_wasInitialized;
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		//[SqlReadOnly]
		public System.Int16 DocumentLevel 
		{ 
			get
			{
				return _DocumentLevel; 
			}
			set
			{
				if(_DocumentLevel_wasInitialized)
				{
					throw new System.InvalidOperationException("Property DocumentLevel cannot be changed once initialized column is computed.");
				}
				_DocumentLevel_wasInitialized = true;
				_DocumentLevel = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Title { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Owner { get; set; }
		//[SqlType(System.Data.SqlDbType.Bit,True, 1)]
		
	public System.Boolean FolderFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String FileName { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String FileExtension { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Revision { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ChangeNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String DocumentSummary { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] Document { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class IllustrationDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 IllustrationID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Diagram { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class LocationDto
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal CostRate { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal Availability { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductCategoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductCategoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductCostHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductDocumentDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 892)]
		
	public  Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductInventoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Shelf { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Bin { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductListPriceHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ListPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductModelIllustrationDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 IllustrationID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductPhotoDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductPhotoID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] ThumbNailPhoto { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ThumbnailPhotoFileName { get; set; }
		//[SqlType(System.Data.SqlDbType.VarBinary,True, 8000)]
		
	public System.Byte[] LargePhoto { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String LargePhotoFileName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductProductPhotoDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductPhotoID { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag Primary { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductReviewDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductReviewID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name ReviewerName { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ReviewDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String EmailAddress { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Rating { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Comments { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductSubcategoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductSubcategoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductCategoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ScrapReasonDto
	{ 
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrapReasonID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class TransactionHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TransactionID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderLineID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime TransactionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String TransactionType { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class TransactionHistoryArchiveDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TransactionID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ReferenceOrderLineID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime TransactionDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String TransactionType { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class UnitMeasureDto
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class WorkOrderDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 WorkOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 OrderQty { get; set; }
			private System.Int32 _StockedQty;
			private bool _StockedQty_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		//[SqlReadOnly]
		public System.Int32 StockedQty 
		{ 
			get
			{
				return _StockedQty; 
			}
			set
			{
				if(_StockedQty_wasInitialized)
				{
					throw new System.InvalidOperationException("Property StockedQty cannot be changed once initialized column is computed.");
				}
				_StockedQty_wasInitialized = true;
				_StockedQty = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrappedQty { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ScrapReasonID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class WorkOrderRoutingDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 WorkOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OperationSequence { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 LocationID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ScheduledStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ScheduledEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ActualStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ActualEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal ActualResourceHrs { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal PlannedCost { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ActualCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductDescriptionDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductDescriptionID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Description { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductModelProductDescriptionCultureDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductDescriptionID { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CultureID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ProductNumber { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag MakeFlag { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag FinishedGoodsFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Color { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 SafetyStockLevel { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ReorderPoint { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardCost { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ListPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Size { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String SizeUnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String WeightUnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal Weight { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 DaysToManufacture { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String ProductLine { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Class { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String Style { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductSubcategoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime SellStartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime SellEndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DiscontinuedDate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductModelDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductModelID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String CatalogDescription { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Instructions { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class VendorDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 30)]
		
	public AccountNumber AccountNumber { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte CreditRating { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag PreferredVendorStatus { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag ActiveFlag { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String PurchasingWebServiceURL { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ProductVendorDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 AverageLeadTime { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal StandardPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal LastReceiptCost { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime LastReceiptDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MinOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MaxOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 OnOrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String UnitMeasureCode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PurchaseOrderDetailDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderDetailID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPrice { get; set; }
			private System.Decimal _LineTotal;
			private bool _LineTotal_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal LineTotal 
		{ 
			get
			{
				return _LineTotal; 
			}
			set
			{
				if(_LineTotal_wasInitialized)
				{
					throw new System.InvalidOperationException("Property LineTotal cannot be changed once initialized column is computed.");
				}
				_LineTotal_wasInitialized = true;
				_LineTotal = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal ReceivedQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		
	public System.Decimal RejectedQty { get; set; }
			private System.Decimal _StockedQty;
			private bool _StockedQty_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Decimal,True, 17)]
		//[SqlReadOnly]
		public System.Decimal StockedQty 
		{ 
			get
			{
				return _StockedQty; 
			}
			set
			{
				if(_StockedQty_wasInitialized)
				{
					throw new System.InvalidOperationException("Property StockedQty cannot be changed once initialized column is computed.");
				}
				_StockedQty_wasInitialized = true;
				_StockedQty = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PurchaseOrderHeaderDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PurchaseOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte RevisionNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 EmployeeID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 VendorID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime OrderDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ShipDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SubTotal { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal TaxAmt { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Freight { get; set; }
			private System.Decimal _TotalDue;
			private bool _TotalDue_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal TotalDue 
		{ 
			get
			{
				return _TotalDue; 
			}
			set
			{
				if(_TotalDue_wasInitialized)
				{
					throw new System.InvalidOperationException("Property TotalDue cannot be changed once initialized column is computed.");
				}
				_TotalDue_wasInitialized = true;
				_TotalDue = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ShipMethodDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ShipBase { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal ShipRate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CustomerDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CustomerID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 PersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StoreID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
			private System.String _AccountNumber;
			private bool _AccountNumber_wasInitialized;
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		//[SqlReadOnly]
		public System.String AccountNumber 
		{ 
			get
			{
				return _AccountNumber; 
			}
			set
			{
				if(_AccountNumber_wasInitialized)
				{
					throw new System.InvalidOperationException("Property AccountNumber cannot be changed once initialized column is computed.");
				}
				_AccountNumber_wasInitialized = true;
				_AccountNumber = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CountryRegionCurrencyDto
	{ 
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CreditCardDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CardType { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CardNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte ExpMonth { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 ExpYear { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CurrencyDto
	{ 
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String CurrencyCode { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class CurrencyRateDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CurrencyRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime CurrencyRateDate { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String FromCurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NChar,True, 8000)]
		
	public System.String ToCurrencyCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal AverageRate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal EndOfDayRate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class PersonCreditCardDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesOrderDetailDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderDetailID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CarrierTrackingNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallInt,True, 2)]
		
	public System.Int16 OrderQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPrice { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal UnitPriceDiscount { get; set; }
			private System.Decimal _LineTotal;
			private bool _LineTotal_wasInitialized;
		//[SqlType(column.Type.Name, True, 17)]
		//[SqlReadOnly]
		public System.Decimal LineTotal 
		{ 
			get
			{
				return _LineTotal; 
			}
			set
			{
				if(_LineTotal_wasInitialized)
				{
					throw new System.InvalidOperationException("Property LineTotal cannot be changed once initialized column is computed.");
				}
				_LineTotal_wasInitialized = true;
				_LineTotal = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesOrderHeaderSalesReasonDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesReasonID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesPersonQuotaHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime QuotaDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesQuota { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesPersonDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesQuota { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Bonus { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal CommissionPct { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesReasonDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesReasonID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name ReasonType { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesTerritoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String CountryRegionCode { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Group { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SalesLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal CostYTD { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal CostLastYear { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesTaxRateDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesTaxRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 StateProvinceID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte TaxType { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal TaxRate { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesOrderHeaderDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesOrderID { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte RevisionNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime OrderDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DueDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ShipDate { get; set; }
		//[SqlType(System.Data.SqlDbType.TinyInt,True, 1)]
		
	public System.Byte Status { get; set; }
		//[SqlType(column.Type.Name, False, 1)]
		
	public Flag OnlineOrderFlag { get; set; }
			private System.String _SalesOrderNumber;
			private bool _SalesOrderNumber_wasInitialized;
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		//[SqlReadOnly]
		public System.String SalesOrderNumber 
		{ 
			get
			{
				return _SalesOrderNumber; 
			}
			set
			{
				if(_SalesOrderNumber_wasInitialized)
				{
					throw new System.InvalidOperationException("Property SalesOrderNumber cannot be changed once initialized column is computed.");
				}
				_SalesOrderNumber_wasInitialized = true;
				_SalesOrderNumber = value;;
			}
		}
		//[SqlType(column.Type.Name, True, 50)]
		
	public OrderNumber PurchaseOrderNumber { get; set; }
		//[SqlType(column.Type.Name, True, 30)]
		
	public AccountNumber AccountNumber { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CustomerID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesPersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BillToAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipToAddressID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShipMethodID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CreditCardID { get; set; }
		//[SqlType(System.Data.SqlDbType.VarChar,True, 8000)]
		
	public System.String CreditCardApprovalCode { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 CurrencyRateID { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal SubTotal { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal TaxAmt { get; set; }
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		
	public System.Decimal Freight { get; set; }
			private System.Decimal _TotalDue;
			private bool _TotalDue_wasInitialized;
		//[SqlType(System.Data.SqlDbType.Money,True, 8)]
		//[SqlReadOnly]
		public System.Decimal TotalDue 
		{ 
			get
			{
				return _TotalDue; 
			}
			set
			{
				if(_TotalDue_wasInitialized)
				{
					throw new System.InvalidOperationException("Property TotalDue cannot be changed once initialized column is computed.");
				}
				_TotalDue_wasInitialized = true;
				_TotalDue = value;;
			}
		}
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Comment { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SalesTerritoryHistoryDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 TerritoryID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ShoppingCartItemDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ShoppingCartItemID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String ShoppingCartID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Quantity { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime DateCreated { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SpecialOfferDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Description { get; set; }
		//[SqlType(System.Data.SqlDbType.SmallMoney,True, 4)]
		
	public System.Decimal DiscountPct { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Type { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Category { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime StartDate { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime EndDate { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MinQty { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 MaxQty { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class SpecialOfferProductDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SpecialOfferID { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 ProductID { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class StoreDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 BusinessEntityID { get; set; }
		//[SqlType(column.Type.Name, True, 100)]
		
	public Name Name { get; set; }
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 SalesPersonID { get; set; }
		//[SqlType(System.Data.SqlDbType.Xml,True, -1)]
		
	public System.String Demographics { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid rowguid { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime ModifiedDate { get; set; }
	}
	public class ExecuteScriptTestsDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 Id { get; set; }
		//[SqlType(System.Data.SqlDbType.DateTime,True, 8)]
		
	public System.DateTime Timestamp { get; set; }
		//[SqlType(System.Data.SqlDbType.UniqueIdentifier,True, 16)]
		
	public System.Guid TestUid { get; set; }
		//[SqlType(System.Data.SqlDbType.NVarChar,True, 8000)]
		
	public System.String Data { get; set; }
	}
}

namespace SolvesIT.DbUtils.Tests.Integration.BasicEntities.ValueTypes
{
	public class AccountNumber
	{
		public AccountNumber(System.String value)
		{
			Value = value ?? throw new System.ArgumentNullException(nameof(value));
		}
		
		public System.String Value { get; private set; }

		public static implicit operator AccountNumber(System.String value)
		{ 
			var converted = new AccountNumber(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as AccountNumber;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
		public class Flag
	{
		public Flag(System.Boolean value)
		{
			Value = value;
		}
		
		public System.Boolean Value { get; private set; }

		public static implicit operator Flag(System.Boolean value)
		{ 
			var converted = new Flag(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as Flag;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
		public class Name
	{
		public Name(System.String value)
		{
			Value = value ?? throw new System.ArgumentNullException(nameof(value));
		}
		
		public System.String Value { get; private set; }

		public static implicit operator Name(System.String value)
		{ 
			var converted = new Name(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as Name;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
		public class NameStyle
	{
		public NameStyle(System.Boolean value)
		{
			Value = value;
		}
		
		public System.Boolean Value { get; private set; }

		public static implicit operator NameStyle(System.Boolean value)
		{ 
			var converted = new NameStyle(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as NameStyle;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
		public class OrderNumber
	{
		public OrderNumber(System.String value)
		{
			Value = value ?? throw new System.ArgumentNullException(nameof(value));
		}
		
		public System.String Value { get; private set; }

		public static implicit operator OrderNumber(System.String value)
		{ 
			var converted = new OrderNumber(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as OrderNumber;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
		public class Phone
	{
		public Phone(System.String value)
		{
			Value = value ?? throw new System.ArgumentNullException(nameof(value));
		}
		
		public System.String Value { get; private set; }

		public static implicit operator Phone(System.String value)
		{ 
			var converted = new Phone(value);
			return converted;
		}

		public override bool Equals(object obj)
	    {
	        var other = obj as Phone;
	        if (other != null)
	        {
	            return Value.Equals(other.Value);
	        }

	        return base.Equals(obj);
	    }

	    public override int GetHashCode()
	    {
	        return Value.GetHashCode();
	    }

	    public override string ToString()
	    {
	        return Value.ToString();
	    }
	}
	}


namespace SolvesIT.DbUtils.Tests.Integration.BasicEntities.TableTypes
{
	using SolvesIT.DbUtils.Tests.Integration.BasicEntities.ValueTypes;
	public class IntListDto
	{ 
		//[SqlType(System.Data.SqlDbType.Int,True, 4)]
		
	public System.Int32 id { get; set; }
	}

}
 
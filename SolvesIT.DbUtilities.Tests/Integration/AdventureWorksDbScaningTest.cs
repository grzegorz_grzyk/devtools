﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Utilities.Ioc.Unity;
using SolvesIT.DbAbstraction;
using Unity;

namespace SolvesIT.DbUtilities.Tests.Integration
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage(
        "Microsoft.Design", 
        "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable",
        Justification = "Object dispposed in test cleanup.")]
    [TestClass]
    public class AdventureWorksDbScaningTest
    {
        private UnityContainer _container;

        [TestInitialize]
        public void Init()
        {
            _container = new UnityContainer();
            var registrar = new UnityRegistrar(_container);
            registrar.EnableEnumerableResolution();
            new Bootstrapper().Bootstrap(registrar);
        }

        [TestMethod]
        public async Task ScanDatabase()
        {
            var service = _container.Resolve<IDdService>();
            var database = ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString;
            using (var sqlConnection = new SqlConnection(database))
            {
                sqlConnection.Open();
                var catalog = await service.LoadDatabaseAsync(sqlConnection);
                catalog.Name.ShouldBeEqual("AdventureWorks2012");
                catalog.Schemata.Count().ShouldBeEqual(8);
                catalog.Tables.Count().ShouldBeEqual(73);
                catalog.Types.Count().ShouldBeEqual(6);
                catalog.TableTypes.Count().ShouldBeEqual(1);
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            _container.Dispose();
            _container = null;
        }
    }
}

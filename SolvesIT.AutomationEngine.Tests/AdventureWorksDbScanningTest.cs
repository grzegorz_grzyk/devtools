﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CommonServiceLocator;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Utilities.Ioc.Unity;
using SolvesIT.Automation.Templating.Generators.AW2012;
using SolvesIT.AutomationEngine.Tests.Properties;
using SolvesIT.Common;
using SolvesIT.DbAbstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using Unity;
using Unity.ServiceLocation;
using Bootstrapper = SolvesIT.Automation.Templating.Bootstrapper;

namespace SolvesIT.AutomationEngine.Tests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    [TestClass]
    public class AdventureWorksDbScanningTest
    {
        private UnityContainer _container;

        [TestInitialize]
        public void Init()
        {
            _container = new UnityContainer();
            var serviceLocator = new UnityServiceLocator(_container);
            ServiceLocator.SetLocatorProvider(() => serviceLocator);
            var registrar = new UnityRegistrar(_container);
            registrar.EnableEnumerableResolution();
            new Bootstrapper().Bootstrap(registrar);
            new SolvesIT.DbUtilities.Bootstrapper().Bootstrap(registrar);
            new SolvesIT.Common.Bootstrapper().Bootstrap(registrar);
            new SolvesIT.Automation.Templating.Generators.AW2012.Bootstrapper().Bootstrap(registrar);
        }

        [TestMethod]
        public async Task ScanDatabase()
        {
            var service = _container.Resolve<IDdService>();
            var database = ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString;
            DdCatalog catalog;

            using (var sqlConnection = new SqlConnection(database))
            {
                sqlConnection.Open();
                catalog = await service.LoadDatabaseAsync(sqlConnection);
            }

            var generator = _container.Resolve<IGeneratorRunner>();
            var result = generator.Run(catalog);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _container.Dispose();
            _container = null;
        }
    }
}

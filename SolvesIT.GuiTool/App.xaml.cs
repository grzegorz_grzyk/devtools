﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace SolvesIT.DevTools
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //var x = 1;
            //throw new NotImplementedException();
            Application.Current.Shutdown();
        }

    }
}
﻿using System.Windows;
using System.Windows.Controls;
using SolvesIT.DevTools.ViewModels;

namespace SolvesIT.DevTools.Views
{
    /// <summary>
    /// Interaction logic for QueryEditorView.xaml
    /// </summary>
    public partial class QueryEditorView : UserControl
    {
        public QueryEditorView()
        {
            InitializeComponent();

            DataContextChanged += QueryEditorView_DataContextChanged;
        }

        private void QueryEditorView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var context = (QueryEditorViewModel)e.NewValue;
            TextEditor.TextArea.TextView.LineTransformers.Insert(0, context.Transformer);
        }
    }
}
﻿using Caliburn.Micro;
using SolvesIT.DevTools.ViewModels;
using System.Windows;

namespace SolvesIT.DevTools
{
    internal class AppWindowManager
        : WindowManager
    {
        protected override System.Windows.Window EnsureWindow(object model, object view, bool isDialog)
        {
            var window = base.EnsureWindow(model, view, isDialog);

            if (model is MainViewModel)
            {
                window.SizeToContent = SizeToContent.Manual;

                window.Title = "SolvesIT.DevTools";
                window.Width = 1200;
                window.Height = 1000;
            }

            return window;
        }
    }
}
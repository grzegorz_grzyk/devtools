﻿using System;
using System.Windows.Data;

namespace SolvesIT.GuiTool.Assets.Converters
{
    internal class BooleanToValueConverter
        : IValueConverter
    {
        public object IfFalse { get; set; }

        public object IfTrue { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((bool)value)
                return IfTrue;
            else
                return IfFalse;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
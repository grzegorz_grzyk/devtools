﻿using System.Collections.Generic;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public class BssCsBuilder : BaseBuilder, ICsBuilder
    {

        public BssCsBuilder(ITemplateService templateService)
            : base(templateService)
        {
        }

        protected override string GeneratorName
        {
            get { return "BssCsCodeGenerator"; }
        }

        public void RunGenerator(
            DdCatalog databaseObject,
            ICollection<ValidationMessage> validationMessages)
        {
            var @void = GenerateCode(databaseObject, validationMessages);
        }
    }
}

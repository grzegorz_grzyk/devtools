﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public class ConstsBuilder : BaseBuilder, IConstsBuilder
    {
        public ConstsBuilder(ITemplateService templateService) : base(templateService)
        {
        }

        protected override string GeneratorName
        {
            get { return "BssConstCodeGenerator"; }
        }

        public void RunGenerator(
            SqlConnection sqlConnection,
            ICollection<ValidationMessage> validationMessages)
        {

            var @void = GenerateCode(sqlConnection, validationMessages);
        }
    }
}

﻿using System.Collections.Generic;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public interface ICsBuilder
    {
        void RunGenerator(
            DdCatalog databaseObject,
            ICollection<ValidationMessage> validationMessages);
    }
}

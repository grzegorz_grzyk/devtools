﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public interface IConstsBuilder
    {
        void RunGenerator(
            SqlConnection sqlConnection,
            ICollection<ValidationMessage> validationMessages);
    }
}

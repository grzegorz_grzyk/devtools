﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Utils;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Logic.DdlReaders;
using SolvesIT.DevTools.Logic.SolutionService;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model;
using SolvesIT.DevTools.Model.SolutionModel;
using SolvesIT.DevTools.Model.TemplateModel;
using SolvesIT.GuiTool.Properties;

namespace SolvesIT.DevTools.Logic.Builders
{
    public class BssSqlBuilder : BaseBuilder, ISqlBuilder
    {
        private const string ScriptSeparator = "GO";
        private readonly IDdlService _ddlService;
        private readonly ILocalDbService _localDbService;
        private readonly IRevisionControlService _revisionControlService;

        public BssSqlBuilder(
            ILocalDbService localDbService,
            ITemplateService templateService,
            IDdlService ddlService, IRevisionControlService revisionControlService)
            : base(templateService)
        {
            _localDbService = localDbService;
            _ddlService = ddlService;
            _revisionControlService = revisionControlService;
        }

        protected override string GeneratorName
        {
            get { return "BssSqlCodeGenerator"; }
        }

        public async Task<string> CreateDatabaseScript(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages,
            IReadOnlyCollection<ModuleBaseInfo> currentModulesInfo = null)
        {
            var createDbScript = GenerateDatatbase(solutionSqlProjects, validationMessages);
            string generatedScript;

            await _localDbService.InitializeDbAsync();
            using (var localDbConnection = _localDbService.GetDbConnection())
            {
                await localDbConnection.OpenAsync();
                await localDbConnection.ExecuteSrciptAsync(createDbScript);
                var ddlCatalog = await _ddlService.LoadDatabase(localDbConnection);

                generatedScript = GenerateCode(ddlCatalog, validationMessages);
            }

            var script = new StringBuilder();
            if (currentModulesInfo.EmptyIfNull().Any())
            {
                var updateDbScript = GenerateDatatbase(solutionSqlProjects, validationMessages, currentModulesInfo);
                script.AppendLine(updateDbScript);
            }
            else
            {
                script.AppendLine(createDbScript);
            }

            ////if (currentModulesInfo == null ||
            ////    currentModulesInfo.Any(x => solutionSqlProjects.Single(y => y.ModuleKey == x.Key).Version != x.Version)
            ////    )
            ////{
            script.AppendLine(generatedScript);
            ////}

            return script.ToString();
        }

        public async Task<DdCatalog> GetDdlCatalogFromSln(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages)
        {
            var script = GenerateDatatbase(solutionSqlProjects, validationMessages);
            await _localDbService.InitializeDbAsync();
            using (var localDbConnection = _localDbService.GetDbConnection())
            {
                await localDbConnection.OpenAsync();
                await localDbConnection.ExecuteSrciptAsync(script);
                var ddlCatalog = await _ddlService.LoadDatabase(localDbConnection);

                var generatedScript = GenerateCode(ddlCatalog, validationMessages);
                await localDbConnection.ExecuteSrciptAsync(generatedScript);
                ddlCatalog = await _ddlService.LoadDatabase(localDbConnection);
                return ddlCatalog;
            }
        }

        public async Task RebuildLocalDbFromSln(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages)
        {
            var script = GenerateDatatbase(solutionSqlProjects, validationMessages);
            await _localDbService.InitializeDbAsync();
            using (var localDbConnection = _localDbService.GetDbConnection())
            {
                await localDbConnection.OpenAsync();
                await localDbConnection.ExecuteSrciptAsync(script);
                var ddlCatalog = await _ddlService.LoadDatabase(localDbConnection);

                var generatedScript = GenerateCode(ddlCatalog, validationMessages);
                await localDbConnection.ExecuteSrciptAsync(generatedScript);
            }
        }

        private static void AppendModuleDevUpdateScript(
            SqlProject project,
            StringBuilder output,
            ModuleBaseInfo moduleInfo = null)
        {
            var currentDevVersion = moduleInfo != null
                ? int.Parse(moduleInfo.Version.ToString(CultureInfo.InvariantCulture).Split('.')[1],
                    CultureInfo.InvariantCulture)
                : 0;

            var devUpdatedToRun = project.DevUpdatesFilePath.OrderBy(x => x)
                .Where(x =>
                    // ReSharper disable AssignNullToNotNullAttribute
                    int.Parse(Path.GetFileNameWithoutExtension(x), CultureInfo.InvariantCulture) >
                        // ReSharper restore AssignNullToNotNullAttribute
                    currentDevVersion)
                .ToArray();

            foreach (var devUpdate in devUpdatedToRun)
            {
                output.AppendLine(File.ReadAllText(devUpdate));
                output.AppendLine(ScriptSeparator);

                var updateDevVersionCommand = string.Format(
                    CultureInfo.InvariantCulture,
                    Resources.UpdateDevVersion_tmpl,
                    int.Parse(Path.GetFileNameWithoutExtension(devUpdate), CultureInfo.InvariantCulture),
                    project.ModuleKey);
                output.AppendLine(updateDevVersionCommand);
                output.AppendLine(ScriptSeparator);
            }
        }

        private static void AppendModuleProgramabilityScript(
            SqlProject project,
            StringBuilder output)
        {
            foreach (var filePath in project.TypesFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.FunctionsFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.TablesChecksFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.TablesForeignKeysFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.TablesIndicesFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.TablesTriggersFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.ViewsBodyFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.ViewsIndicesFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.ViewsTriggersFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }

            foreach (var filePath in project.ProceduresFilePath)
            {
                output.AppendLine(File.ReadAllText(filePath));
                output.AppendLine(ScriptSeparator);
            }
        }

        private void AppendModuleInitialScript(
            SqlProject project,
            StringBuilder output,
            ModuleBaseInfo moduleInfo = null)
        {
            var currentDbVersion = moduleInfo != null ? moduleInfo.Version : 0;

            if (currentDbVersion == 0)
            {
                output.AppendLine(File.ReadAllText(project.SchemaFilePath));
                output.AppendLine(ScriptSeparator);

                foreach (var filePath in project.TablesBodyFilePath)
                {
                    output.AppendLine(File.ReadAllText(filePath));
                    output.AppendLine(ScriptSeparator);
                }

                output.AppendLine(File.ReadAllText(project.InitialDataFilePath));
                output.AppendLine(ScriptSeparator);
            }
            else if ((int)currentDbVersion != (int)project.Version)
            {
                output.AppendLine(File.ReadAllText(project.MajorUpdateFilePath));
                output.AppendLine(ScriptSeparator);
            }
        }

        private string GenerateDatatbase(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages,
            IReadOnlyCollection<ModuleBaseInfo> currentModulesInfo = null)
        {
            var projectsDone = new List<SqlProject>();
            var script = new StringBuilder();

            var revisionNumber = _revisionControlService.GetActualRevisionNumber;

            ////if (currentModulesInfo != null &&
            ////    currentModulesInfo.Any(x => solutionSqlProjects.Single(y => y.ModuleKey == x.Key).Version != x.Version)
            ////    )
            ////{
                script.AppendLine(Resources.DropProgramability);
                script.AppendLine(ScriptSeparator);
            ////}

            IEnumerable<SqlProject> projectsToDo;
            do
            {
                projectsToDo = solutionSqlProjects.Where(
                    x => x.ReferencesProject.EmptyIfNull().All(y => projectsDone.Any(z => z.ModuleKey == y.ModuleKey))
                         && projectsDone.All(z => z.ModuleKey != x.ModuleKey)).ToArray();
                foreach (var project in projectsToDo)
                {
                    var moduleInfo = currentModulesInfo.EmptyIfNull()
                        .SingleOrDefault(x => x.Key == project.ModuleKey);
                    var currentDbVersion = moduleInfo != null ? moduleInfo.Version : 0;
                    var currentDbRevision = moduleInfo != null ? moduleInfo.Revision : null;

                    if (currentDbVersion >= project.Version && revisionNumber == currentDbRevision)
                    {
                        validationMessages.Add(new ValidationMessage
                        {
                            ObjectName = null,
                            Message = string.Format("Module [{0}] is already up-to date.", project.ModuleKey),
                            MessageType = ValidationMessageType.Information
                        });
                        projectsDone.Add(project);
                        continue;
                    }

                    if (currentDbVersion > 0 && project.Version - currentDbVersion > 1)
                    {
                        validationMessages.Add(new ValidationMessage
                        {
                            ObjectName = null,
                            Message =
                                string.Format(
                                    "Module [{0}] is to old to update. Minimal required version is {1}. Update operation canceled",
                                    project.ModuleKey, (int)project.Version - 1),
                            MessageType = ValidationMessageType.Error
                        });

                        break;
                    }

                    AppendModuleInitialScript(project, script, moduleInfo);
                    if (currentDbVersion != project.Version)
                    {
                        AppendModuleDevUpdateScript(project, script, moduleInfo);
                    }

                    var updateRevisionNumberCommand = string.Format(
                        Resources.UpdateRevisionNumber_tmpl,
                        revisionNumber,
                        project.ModuleKey);
                    script.AppendLine(updateRevisionNumberCommand);
                    script.AppendLine(ScriptSeparator);

                    projectsDone.Add(project);
                }
            } while (projectsToDo.Any() && validationMessages.All(x => x.MessageType != ValidationMessageType.Error));


            ////if (currentModulesInfo == null ||
            ////   currentModulesInfo.Any(x => solutionSqlProjects.Single(y => y.ModuleKey == x.Key).Version != x.Version)
            ////   )
            ////{
                projectsDone = new List<SqlProject>();
                do
                {
                    projectsToDo = solutionSqlProjects.Where(
                        x => x.ReferencesProject.EmptyIfNull().All(y => projectsDone.Any(z => z.ModuleKey == y.ModuleKey))
                             && projectsDone.All(z => z.ModuleKey != x.ModuleKey)).ToArray();
                    foreach (var project in projectsToDo)
                    {
                        AppendModuleProgramabilityScript(project, script);
                        script.AppendLine(ScriptSeparator);
                        projectsDone.Add(project);
                    }
                } while (projectsToDo.Any() && validationMessages.All(x => x.MessageType != ValidationMessageType.Error));
            ////}

            return script.ToString();
        }
    }
}
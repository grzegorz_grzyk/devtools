﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Model;
using SolvesIT.DevTools.Model.SolutionModel;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public interface ISqlBuilder
    {
        Task<string> CreateDatabaseScript(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages,
            IReadOnlyCollection<ModuleBaseInfo> currentModulesInfo = null);

        Task<DdCatalog> GetDdlCatalogFromSln(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages);

        Task RebuildLocalDbFromSln(
            IReadOnlyCollection<SqlProject> solutionSqlProjects,
            ICollection<ValidationMessage> validationMessages);
    }
}

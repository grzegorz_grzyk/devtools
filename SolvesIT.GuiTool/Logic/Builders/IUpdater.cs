﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public interface IUpdater
    {
        Task<string> UpdateDatabase(ICollection<ValidationMessage> validationMessages);

        Task<string> UpdateClass(ICollection<ValidationMessage> validationMessages);

        Task<string> UpdateConsts(ICollection<ValidationMessage> validationMessages);
    }
}

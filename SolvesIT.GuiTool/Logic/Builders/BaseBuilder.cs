﻿using System.Collections.Generic;
using System.Linq;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.Logic.Builders
{
    public abstract class BaseBuilder
    {
        private readonly ITemplateService _templateService;

        protected abstract string GeneratorName { get; }

        protected BaseBuilder(ITemplateService templateService)
        {
            _templateService = templateService;
        }

        protected string GenerateCode(
            object parameter,
            ICollection<ValidationMessage> validationMessages)
        {
            var generators = _templateService.Load(validationMessages);
            var generator = generators.SingleOrDefault(x => x.Name == GeneratorName);
            if (generator == null)
            {
                validationMessages.Add(new ValidationMessage
                {
                    ObjectName = null,
                    Message = "Can't find generator.",
                    MessageType = ValidationMessageType.Error
                });

                return null;
            }
            var result = _templateService.RunGenerator(generator, parameter);
            return result;
        }
    }
}

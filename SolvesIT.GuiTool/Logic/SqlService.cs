﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony;
using SolvesIT.DevTools.Model;
using SolvesIT.DevTools.Model.SettingsModel;
using Resources = SolvesIT.GuiTool.Properties.Resources;

namespace SolvesIT.DevTools.Logic
{
    public class SqlService
        : ISqlService
    {
        private readonly Settings _settings;
        private readonly IHelpers _helpers;

        public SqlService(Settings settings, IHelpers helpers)
        {
            _settings = settings;
            _helpers = helpers;
        }

        public async Task<SqlCommandResult> ExecuteCommandAsync(
            string commandText,
            IReadOnlyCollection<SqlParameter> commandParameters = null)
        {
            return await Task.Run(() =>
            {
                using (var connection = new SqlConnection(GetConnectionString))
                {
                    var messageBuilder = new StringBuilder();
                    connection.Open();
                    connection.InfoMessage += (s, e) => messageBuilder.AppendLine(e.Message);
                    using (var command = new SqlCommand(commandText, connection))
                    {
                        if (commandParameters != null)
                        {
                            var sqlParameters = commandParameters.ToArray();
                            command.Parameters.AddRange(sqlParameters);
                        }
                        command.StatementCompleted +=
                            (s, e) => messageBuilder.AppendLine(
                                string.Format(CultureInfo.InvariantCulture, "({0} row(s) affected)", e.RecordCount));
                        using (var adapter = new SqlDataAdapter(command))
                        {
                            var result = new SqlCommandResult { Data = new DataSet() };

                            try
                            {
                                adapter.Fill(result.Data);
                            }
                            catch (SqlException e)
                            {
                                messageBuilder.AppendLine(
                                    string.Format(CultureInfo.InvariantCulture, "Msg {0}, Line {1}",
                                        e.Number,
                                        e.LineNumber)
                                    );
                                messageBuilder.AppendLine(e.Message);
                            }
                            result.Message = messageBuilder.ToString();
                            return result;
                        }
                    }
                }
            });
        }

        public async Task<bool> TryConnect()
        {
            return await Task.Run(() =>
            {
                var connectionStringBuilder = GetConnectionStringBuilder();
                connectionStringBuilder.InitialCatalog = "master";
                using (var connection = new SqlConnection(connectionStringBuilder.ToString()))
                {
                    try
                    {
                        connection.Open();
                    }
                    catch
                    {
                        return false;
                    }

                    return true;
                }
            });
        }

        public async Task<bool> CheckDatabaseExistence()
        {
            return await Task.Run(async () =>
            {
                var connectionStringBuilder = GetConnectionStringBuilder();
                var dbNameParameter = new SqlParameter("@DbName", connectionStringBuilder.InitialCatalog);
                connectionStringBuilder.InitialCatalog = "master";
                const string commandText = "SELECT CAST(IIF(DB_ID(@DbName) IS NOT NULL,1,0) AS BIT)";

                using (var connection = new SqlConnection(connectionStringBuilder.ToString()))
                using (var command = new SqlCommand(commandText, connection))
                {
                    connection.Open();
                    command.Parameters.Add(dbNameParameter);

                    var result = await command.ExecuteScalarAsync();
                    return (bool)result;
                }

            });
        }

        public async Task CreateDatabaseIfNotExists()
        {
            if (await CheckDatabaseExistence()) return;

            var connectionStringBuilder = GetConnectionStringBuilder();
            var commandText = string.Format("CREATE DATABASE [{0}]", connectionStringBuilder.InitialCatalog);
            connectionStringBuilder.InitialCatalog = "master";

            using (var connection = new SqlConnection(connectionStringBuilder.ToString()))
            using (var command = new SqlCommand(commandText, connection))
            {
                connection.Open();
                await command.ExecuteNonQueryAsync();
            }
        }

        public async Task<IReadOnlyCollection<ModuleBaseInfo>> GetDatabaseModulesVersionInfo()
        {
            var results = new List<ModuleBaseInfo>();
            var commandText = Resources.GetDatabaseModulesVersionInfo;

            if (!await TryConnect() || !await CheckDatabaseExistence()) return results;

            using (var connection = new SqlConnection(GetConnectionString))
            using (var command = new SqlCommand(commandText, connection))
            {
                connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        results.Add(new ModuleBaseInfo
                        {
                            Key = reader.GetString(0),
                            Version = reader.GetDecimal(1),
                            Revision = reader.IsDBNull(2) ? null : reader.GetString(2)
                        });
                    }
                }
            }
            return results;
        }

        private SqlConnectionStringBuilder GetConnectionStringBuilder()
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = _settings.Sql.InstanceName,
                InitialCatalog = _settings.Sql.DbName,
                IntegratedSecurity = _settings.Sql.IsIntegratedSecurity
            };
            if (!_settings.Sql.IsIntegratedSecurity)
            {
                connectionStringBuilder.UserID = _settings.Sql.User;
                connectionStringBuilder.Password = _settings.Sql.Password;
            }
            connectionStringBuilder.Pooling = false;
            connectionStringBuilder.ApplicationName = _helpers.GetApplicationName;

            return connectionStringBuilder;
        }

        public string GetConnectionString
        {
            get
            {
                var connectionStringBuilder = GetConnectionStringBuilder();

                return connectionStringBuilder.ToString();
            }
        }
    }
}
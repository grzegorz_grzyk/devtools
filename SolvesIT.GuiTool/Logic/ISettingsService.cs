﻿using SolvesIT.DevTools.Model.SettingsModel;

namespace SolvesIT.DevTools.Logic
{
    public interface ISettingsService
    {
        Settings Load();

        void Save();
    }
}
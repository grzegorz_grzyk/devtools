﻿using System.IO;
using System.Runtime.Serialization;
using SolvesIT.DevTools.Model.SettingsModel;

namespace SolvesIT.DevTools.Logic
{
    public class SettingsService
        : ISettingsService
    {
        private const string FileName = "Settings.xml";

        private Settings _settins;
        private readonly IHelpers _helpers;

        public SettingsService(IHelpers helpers)
        {
            _helpers = helpers;
        }

        public Settings Load()
        {
            var dataSerializer = new DataContractSerializer(typeof(Settings));
            var filePath = GetFilePath();
            if (File.Exists(filePath))
            {
                using (var file = File.OpenRead(filePath))
                {
                    try
                    {
                        _settins = (Settings)dataSerializer.ReadObject(file);
                    }
                    catch (SerializationException)
                    {
                        _settins = GetDefaultSetting();
                    }
                }
            }
            else
            {
                _settins = GetDefaultSetting();
            }
            return _settins;
        }

        public void Save()
        {
            var dataSerializer = new DataContractSerializer(typeof(Settings));
            var filePath = GetFilePath();
            using (var file = File.Create(filePath))
            {
                dataSerializer.WriteObject(file, _settins);
            }
        }

        private string GetFilePath()
        {
            return Path.Combine(_helpers.GetLocalAppDataPath, FileName);
        }

        private static Settings GetDefaultSetting()
        {
            var settings = new Settings
            {
                SolutionFile = @"C:\Repos\BSS\System.sln",
                Sql = new SqlSettings
                {
                    IsIntegratedSecurity = true,
                    DbName = "BSS_DB",
                    InstanceName = ".",
                    Password = null,
                    User = null
                },

                Template = new TemplatesSettings
                {
                    DirectoryPath = @"C:\Repos\Bss\BSSystem\CodeGeneratorsTemplates"
                }
            };

            return settings;
        }
    }
}
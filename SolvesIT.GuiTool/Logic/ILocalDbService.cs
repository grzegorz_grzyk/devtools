﻿using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SolvesIT.DevTools.Logic
{
    public interface ILocalDbService
    {
        Task InitializeDbAsync();

        Task InitializeDbAsync(string dbName);

        SqlConnection GetDbConnection();

        SqlConnection GetDbConnection(string dbName);
    }
}

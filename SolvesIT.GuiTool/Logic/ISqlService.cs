﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using SolvesIT.DevTools.Model;

namespace SolvesIT.DevTools.Logic
{
    public interface ISqlService
    {
        Task<SqlCommandResult> ExecuteCommandAsync(string commandText,
            IReadOnlyCollection<SqlParameter> commandParameters = null);

        string GetConnectionString { get; }

        Task<bool> TryConnect();

        Task<bool> CheckDatabaseExistence();

        //Task CreateDatabase();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        Task<IReadOnlyCollection<ModuleBaseInfo>> GetDatabaseModulesVersionInfo();

        Task CreateDatabaseIfNotExists();
    }
}
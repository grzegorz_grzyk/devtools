﻿namespace SolvesIT.DevTools.Logic
{
    public interface IHelpers
    {
        string GetAppDirectoryPath { get; }

        string GetApplicationName { get; }

        string GetAppTempPath { get; }

        string GetLocalAppDataPath { get; }

        string DecodePath(string path);

        string GetAbsolutePath(string filePath, string defaultRootPath);
    }
}
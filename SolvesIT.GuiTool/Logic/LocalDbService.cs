﻿using System;
using System.Data.SqlClient;
using System.Data.SqlLocalDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SolvesIT.GuiTool.Properties;

namespace SolvesIT.DevTools.Logic
{
    class LocalDbService : ILocalDbService
    {
        private const string DbName = "SIT_DevTool_temp";

        private readonly ISqlLocalDbProvider _localDbProvider;
        private readonly IHelpers _helpers;

        private ISqlLocalDbInstance _localDbInstance;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "LocalDb")]
        public LocalDbService(
            ISqlLocalDbApi localDbApi,
            ISqlLocalDbProvider localDbProvider,
            IHelpers helpers)
        {
            _localDbProvider = localDbProvider;
            _helpers = helpers;

            if (!localDbApi.IsLocalDBInstalled() && _localDbProvider.GetVersions().All(v => v.Version.Major < 12))
            {
                throw new InvalidOperationException("SQL Server 2014 (or newer) Express LocalDb not installed");
            }
        }

        private ISqlLocalDbInstance GetInstance
        {
            get
            {
                if (_localDbInstance != null)
                {
                    return _localDbInstance;
                }

                _localDbInstance = _localDbProvider.GetOrCreateInstance(_helpers.GetApplicationName);

                var info = _localDbInstance.GetInstanceInfo();

                if(!info.IsRunning)
                    _localDbInstance.Start();

                _localDbInstance.Start();

                return _localDbInstance;
            }
        }

        public async Task InitializeDbAsync()
        {
            await InitializeDbAsync(DbName);
        }

        public async Task InitializeDbAsync(string dbName)
        {
            using (var connection = GetInstance.CreateConnection())
            {
                await connection.OpenAsync();

                var dbFilePath = Path.Combine(_helpers.GetAppTempPath, dbName + ".mdf");

                var query = string.Format(Resources.RecreateLocalDb_tmpl, dbName, dbFilePath);

                using (var command = new SqlCommand(query, connection))
                {
                    await command.ExecuteNonQueryAsync();
                }
            }
        }


        public SqlConnection GetDbConnection()
        {
            return GetDbConnection(DbName);
        }

        public SqlConnection GetDbConnection(string dbName)
        {
                var connectionStringBuilder = GetInstance.CreateConnectionStringBuilder();
                connectionStringBuilder.InitialCatalog = dbName;
                connectionStringBuilder.MultipleActiveResultSets = true;
                var connectionString = connectionStringBuilder.ToString();
                return new SqlConnection(connectionString);
        }
    }
}

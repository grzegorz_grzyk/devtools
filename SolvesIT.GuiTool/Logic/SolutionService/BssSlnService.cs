﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using Qdarc.Utilities;
using SolvesIT.Common;
using SolvesIT.DevTools.Model;
using SolvesIT.DevTools.Model.SettingsModel;
using SolvesIT.DevTools.Model.SolutionModel;

namespace SolvesIT.DevTools.Logic.SolutionService
{
    public class BssSlnService : ISlnService
    {
        private static readonly XNamespace MsBuild = "http://schemas.microsoft.com/developer/msbuild/2003";

        private readonly Settings _settings;

        public BssSlnService(
            Settings settings)
        {
            _settings = settings;
        }

        public async Task<IReadOnlyCollection<string>> GetCsProjectsPaths(
            ICollection<ValidationMessage> validationMessages)
        {
            return await Task.Run(() =>
            {
                var projectsInfo = GetProjectsInfo(@"BSSystem\\(((?!Core).)+?)\\Abstraction\\(((?!Core).)+?)\.Abstraction\.csproj");
                return projectsInfo.Select(x => x.ProjectFilePath).ToList().AsReadOnly();
            });
        }

        public async Task<IReadOnlyCollection<SqlProject>> GetSqlProjects(
            ICollection<ValidationMessage> validationMessages)
        {
            return await Task.Run(() =>
            {
                var projectsInfo = GetProjectsInfo(@"BSSystem\\(((?!Core).)+?)\\Database\\(((?!Core).)+?)\.Database\.sqlproj");
                var projects = new List<SqlProject>();
                foreach (var projectInfo in projectsInfo)
                {
                    var project = ProcessSqlProject(projectInfo.ProjectFilePath, validationMessages);
                    project.ModuleKey = projectInfo.Key;
                    projects.Add(project);
                }

                foreach (var project in projects)
                {
                    if (project.ReferencesProjectFilePath.Any())
                    {
                        var referenceModulesKey = project.ReferencesProjectFilePath
                            .Select(x => Regex.Match(x, @".+\\(.+?)\.Database\.sqlproj$").Groups[1].Value)
                            .ToArray();
                        var referencedSqlProjects = projects.Where(p => referenceModulesKey.Contains(p.ModuleKey))
                            .ToArray();
                        project.ReferencesProject = referencedSqlProjects;
                    }
                }

                return projects;
            });
        }

        private IEnumerable<ModuleInfo> GetProjectsInfo(string filterRegEx)
        {
            var slnFile = File.ReadAllText(_settings.SolutionFile);
            var slnDirPath = Path.GetDirectoryName(_settings.SolutionFile) ?? "";

            var projectsInfo = Regex.Matches(slnFile, filterRegEx)
                .Cast<Match>().Select(x => new ModuleInfo
                {
                    Key = x.Groups[1].Value,
                    ProjectFilePath = Path.Combine(slnDirPath, x.Value),
                });
            return projectsInfo;
        }

        private static decimal GetSqlProjectVersion(
            SqlProject project,
            XDocument projectFile)
        {
            // ReSharper disable PossibleNullReferenceException
            var versionNode = projectFile.Element(MsBuild + "Project")
                // ReSharper restore PossibleNullReferenceException
                .Elements(MsBuild + "PropertyGroup")
                .Elements(MsBuild + "DacVersion")
                .FirstOrDefault();

            var majorVersion = versionNode != null
                ? decimal.Parse(versionNode.Value.Split('.')[0], CultureInfo.InvariantCulture)
                : 1;
            var lestDevUpdateFile = project.DevUpdatesFilePath.LastOrDefault();
            var minorVersion = lestDevUpdateFile != null
                ? decimal.Parse(Path.GetFileNameWithoutExtension(lestDevUpdateFile), CultureInfo.InvariantCulture)
                : 0;

            return majorVersion + minorVersion / 100;
        }

        private SqlProject ProcessSqlProject(
             string projectFilePath,
             ICollection<ValidationMessage> validationMessages)
        {
            var projFile = XDocument.Load(projectFilePath);
            // ReSharper disable PossibleNullReferenceException
            var itemGroups = projFile.Element(MsBuild + "Project")
                // ReSharper restore PossibleNullReferenceException
                .Elements(MsBuild + "ItemGroup").ToArray();
            var buildNodes = itemGroups.Elements(MsBuild + "Build");
            var noneNodes = itemGroups.Elements(MsBuild + "None");
            var referencesNodes = itemGroups.Elements(MsBuild + "ProjectReference");

            var projectDirectoryPath = Path.GetDirectoryName(projectFilePath) ?? "";

            var filesPaths = buildNodes.EmptyIfNull()
                .Concat(noneNodes.EmptyIfNull())
                .Select(x => Path.Combine(projectDirectoryPath, x.Attribute("Include").Value)).ToList();

            var result = GetSqlProject(filesPaths);

            result.Version = GetSqlProjectVersion(result, projFile);

            var referencesNodesFilePath = referencesNodes.EmptyIfNull()
                .Select(x => Path.Combine(projectDirectoryPath, x.Attribute("Include").Value)).ToList();
            result.ReferencesProjectFilePath = referencesNodesFilePath;

            if (!string.IsNullOrEmpty(result.SchemaFilePath) &&
                !string.IsNullOrEmpty(result.InitialDataFilePath) &&
                !string.IsNullOrEmpty(result.MajorUpdateFilePath))
            {
                return result;
            }

            validationMessages.Add(new ValidationMessage
            {
                ObjectName = Path.GetFileNameWithoutExtension(projectFilePath),
                Message = string.Format(CultureInfo.InvariantCulture, "Project '{0}' doesn't contain all required files.", Path.GetFileNameWithoutExtension(projectFilePath)),
                MessageType = ValidationMessageType.Error
            });
            return null;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        private static SqlProject GetSqlProject(IReadOnlyCollection<string> sqlScriptsPaths)
        {
            const RegexOptions regExOptions = RegexOptions.IgnoreCase;
            var result = new SqlProject()
            {
                SchemaFilePath = sqlScriptsPaths.SingleOrDefault(x => Regex.IsMatch(x, @"Schema\\Schema\.sql$", regExOptions)),
                TablesBodyFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Tables\\.+?\\Body\.sql$", regExOptions)).ToArray(),
                InitialDataFilePath = sqlScriptsPaths.SingleOrDefault(x => Regex.IsMatch(x, @"InitialData\.sql$", regExOptions)),
                DevUpdatesFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"DevUpdates\\.+?\.sql$", regExOptions)).OrderBy(x => x).ToArray(),
                TypesFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Types\\.+?\.sql$", regExOptions)).ToArray(),
                FunctionsFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Functions\\.+?\.sql$", regExOptions)).ToArray(),
                TablesForeignKeysFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Tables\\.+?\\ForeignKeys\.sql$", regExOptions)).ToArray(),
                TablesIndicesFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Tables\\.+?\\Indices\.sql$", regExOptions)).ToArray(),
                TablesTriggersFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Tables\\.+?\\Triggers\.sql$", regExOptions)).ToArray(),
                TablesChecksFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Tables\\.+?\\Checks\.sql$", regExOptions)).ToArray(),
                ViewsBodyFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Views\\.+?\\Body\.sql$", regExOptions)).ToArray(),
                ViewsIndicesFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Views\\.+?\\Indices\.sql$", regExOptions)).ToArray(),
                ViewsTriggersFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Views\\.+?\\Triggers\.sql$", regExOptions)).ToArray(),
                ProceduresFilePath = sqlScriptsPaths.Where(x => Regex.IsMatch(x, @"Schema\\Procedures\\.+?\.sql$", regExOptions)).ToArray(),
                MajorUpdateFilePath = sqlScriptsPaths.SingleOrDefault(x => Regex.IsMatch(x, @"InitialData\.sql$", regExOptions))
            };
            return result;
        }
    }
}

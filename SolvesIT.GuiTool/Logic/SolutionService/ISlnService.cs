﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SolvesIT.Common;
using SolvesIT.DevTools.Model.SolutionModel;

namespace SolvesIT.DevTools.Logic.SolutionService
{
    public interface ISlnService
    {
        //Task<Solution> GetSolution(
        //    ICollection<ValidationMessage> validationMessages);

        Task<IReadOnlyCollection<string>> GetCsProjectsPaths(
            ICollection<ValidationMessage> validationMessages);

        Task<IReadOnlyCollection<SqlProject>> GetSqlProjects(
            ICollection<ValidationMessage> validationMessages);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using LibGit2Sharp;
using SolvesIT.DevTools.Model.SettingsModel;

namespace SolvesIT.DevTools.Logic.SolutionService
{
    public class GitService : IRevisionControlService
    {
        private readonly Settings _settings;

        public GitService(Settings settings)
        {
            _settings = settings;
        }

        public void AddFileToRepository(string fullFilePath)
        {
            var fileRootPath = Path.GetPathRoot(fullFilePath);
            if (string.IsNullOrEmpty(fileRootPath) || fileRootPath == "\\")
                throw new ArgumentException("parameter value doesn't contain fullPath");

            var repositoryPath = Path.GetDirectoryName(_settings.SolutionFile);
            var relaliveFilePath = fullFilePath.Replace(repositoryPath + "\\", "");
            using (var repository = new Repository(repositoryPath))
            {
                repository.Index.Add(relaliveFilePath);
            }
        }

        public string GetActualRevisionNumber
        {
            get
            {
                var repositoryPath = Path.GetDirectoryName(_settings.SolutionFile);
                using (var repository = new Repository(repositoryPath))
                {
                    var result = repository.Commits.First().Sha.Substring(0,7);
                    return result;
                }
            }
        }
    }
}

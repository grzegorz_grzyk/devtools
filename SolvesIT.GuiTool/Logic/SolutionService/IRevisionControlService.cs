﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolvesIT.DevTools.Logic.SolutionService
{
    public interface IRevisionControlService
    {
        string GetActualRevisionNumber { get; }

        void AddFileToRepository(string fullFilePath);
    }
}

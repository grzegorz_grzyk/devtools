﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SolvesIT.DevTools.Logic
{
    public class Helpers
        : IHelpers
    {
        public string GetAppDirectoryPath
        {
            get
            {
                var appDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                if (appDirectoryPath == null)
                    throw new InvalidOperationException();
                return appDirectoryPath;
            }
        }

        public string GetApplicationName
        {
            get { return (Assembly.GetEntryAssembly() ?? Assembly.GetExecutingAssembly()).GetName().Name; }
        }

        public string GetAppTempPath
        {
            get
            {
                var tempDirPath = Path.Combine(
                    Path.GetTempPath(),
                    GetApplicationName);
                if (!Directory.Exists(tempDirPath))
                    Directory.CreateDirectory(tempDirPath);
                return tempDirPath;
            }
        }

        public string GetLocalAppDataPath
        {
            get
            {
                var appDataPath = Path.Combine(LocalAppDataPath, GetApplicationName);

                if (!Directory.Exists(appDataPath))
                    Directory.CreateDirectory(appDataPath);

                return appDataPath;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        private string LocalAppDataPath
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            }
        }

        public string DecodePath(string path)
        {
            var result = path;
            var variables = Regex.Matches(path, @"%\w+%").Cast<Match>().Select(x => x.Value).ToArray();

            if (!variables.Any()) return result;

            foreach (var variable in variables)
            {
                result = result.Replace(variable, Environment.GetEnvironmentVariable(variable.Replace("%", "")));
            }

            return result;
        }

        public string GetAbsolutePath(string filePath, string defaultRootPath)
        {
            var decodedPath = DecodePath(filePath);
            var result = decodedPath;
            if (string.IsNullOrEmpty(Path.GetPathRoot(decodedPath)))
            {
                result = Path.Combine(defaultRootPath, decodedPath);
            }
            return result;
        }

        public string GetResourceFile(Type resourceSourceIndicator, string fileName)
        {
            var assembly = resourceSourceIndicator.Assembly;
            var resourceName = string.Format(CultureInfo.InvariantCulture, "{0}.{1}", resourceSourceIndicator.Namespace, fileName);

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream == null)
                {
                    throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Resource not found: {0}", resourceName));
                }

                var reader = new StreamReader(stream);

                return reader.ReadToEnd();
            }
        }
    }
}
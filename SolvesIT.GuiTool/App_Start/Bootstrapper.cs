﻿using System.Data.SqlLocalDb;
using Irony.Parsing;
using Qdarc.Utilities.Ioc;
using Qdarc.Utils.Ioc;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Logic;
using SolvesIT.DevTools.Logic.Builders;
using SolvesIT.DevTools.Logic.DdlReaders;
using SolvesIT.DevTools.Logic.DdReaders;
using SolvesIT.DevTools.Logic.SolutionService;
using SolvesIT.DevTools.Logic.Templates;

namespace SolvesIT.GuiTool
{
    public class Bootstrapper : IBootstrapper
    {
        public double Order
        {
            get { return 0; }
        }

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterAsSingleton<ISettingsService, SettingsService>();
            registrar.RegisterAsSingleton<IHelpers, Helpers>();
            var fileSettingManager = registrar.Resolver.Resolve<ISettingsService>();
            var settings = fileSettingManager.Load();
            registrar.RegisterInstance(settings);

            RegisterDdlService(registrar);
            RegisterLocalDBService(registrar);
            RegisterTemplateService(registrar);

            registrar.RegisterAsSingleton<ISlnService, BssSlnService>();
            registrar.RegisterAsSingleton<ICsProjectService, CsProjectService>();
            registrar.RegisterAsSingleton<IRevisionControlService, GitService>();
            registrar.RegisterAsSingleton<ISqlService, SqlService>();

            RegisterBuilders(registrar);
        }

        private static void RegisterBuilders(IRegistrar registrar)
        {
            registrar.RegisterAsSingleton<ICsBuilder, BssCsBuilder>();
            registrar.RegisterAsSingleton<ISqlBuilder, BssSqlBuilder>();
            registrar.RegisterAsSingleton<IConstsBuilder, ConstsBuilder>();

            registrar.RegisterAsSingleton<IUpdater, Updater>();
        }

        private static void RegisterDdlService(IRegistrar registrar)
        {
            //TODO: Move to DbUtilities


        }

        private static void RegisterLocalDBService(IRegistrar registrar)
        {
            registrar.RegisterSingle<ISqlLocalDbApi,SqlLocalDbApiWrapper>();
            registrar.RegisterSingle<ISqlLocalDbProvider,SqlLocalDbProvider>();

            registrar.RegisterAsSingleton<ILocalDbService, LocalDbService>();
        }

        private static void RegisterTemplateService(IRegistrar registrar)
        {
            registrar.RegisterAsSingleton<ITemplateService, TemplateService>();
            registrar.RegisterAsSingleton<ITemplateRunner, TemplateRunner>();
            registrar.RegisterAsSingleton<Grammar, TemplateGrammar>();
        }
    }
}
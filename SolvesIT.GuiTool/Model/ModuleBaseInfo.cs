﻿namespace SolvesIT.DevTools.Model
{
    public class ModuleBaseInfo
    {
        public string Key { get; set; }

        public decimal Version { get; set; }

        public string Revision { get; set; }
    }
}

﻿using System.Runtime.Serialization;

namespace SolvesIT.DevTools.Model.SettingsModel
{
    [DataContract]
    public class Settings
    {
        [DataMember]
        public SqlSettings Sql { get; set; }

        [DataMember]
        public TemplatesSettings Template { get; set; }

        [DataMember]
        public string SolutionFile { get; set; }

    }
}

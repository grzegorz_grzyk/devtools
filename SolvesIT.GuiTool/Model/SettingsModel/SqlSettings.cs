﻿using System.Runtime.Serialization;

namespace SolvesIT.DevTools.Model.SettingsModel
{
    [DataContract]
    public class SqlSettings
    {
        [DataMember]
        public bool IsIntegratedSecurity { get; set; }

        [DataMember]
        public string DbName { get; set; }

        [DataMember]
        public string InstanceName { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
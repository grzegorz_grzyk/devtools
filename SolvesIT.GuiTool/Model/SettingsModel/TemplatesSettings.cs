﻿using System.Runtime.Serialization;

namespace SolvesIT.DevTools.Model.SettingsModel
{
    [DataContract]
    public class TemplatesSettings
    {
        [DataMember]
        public string DirectoryPath { get; set; }
    }
}

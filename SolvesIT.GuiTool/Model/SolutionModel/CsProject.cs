﻿using System.Collections.Generic;

namespace SolvesIT.DevTools.Model.SolutionModel
{
    public class CsProject : Project
    {
        public IEnumerable<string> DtosFilePath { get; set; }

        public IEnumerable<string> EntitiesFilePath { get; set; }

        public IEnumerable<string> FunctionsFilePath { get; set; }

        public IEnumerable<string> InterfacesFilePath { get; set; }

        public IEnumerable<string> MarkersFilePath { get; set; }

        public IEnumerable<string> SqlTypesFilePath { get; set; }
    }
}

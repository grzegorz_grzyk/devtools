﻿using System.Collections.Generic;

namespace SolvesIT.DevTools.Model.SolutionModel
{
    public class SqlProject : Project
    {
        public decimal Version { get; set; }

        public IEnumerable<string> ReferencesProjectFilePath { get; set; }

        public IEnumerable<SqlProject> ReferencesProject { get; set; }

        // static
        public string SchemaFilePath { get; set; }

        public IReadOnlyCollection<string> TablesBodyFilePath { get; set; }

        public string InitialDataFilePath { get; set; }

        //dev updates
        public IReadOnlyCollection<string> DevUpdatesFilePath { get; set; }

        //dropable
        public IReadOnlyCollection<string> TypesFilePath { get; set; }

        public IReadOnlyCollection<string> FunctionsFilePath { get; set; }

        public IReadOnlyCollection<string> TablesForeignKeysFilePath { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Indices")]
        public IReadOnlyCollection<string> TablesIndicesFilePath { get; set; }

        public IReadOnlyCollection<string> TablesTriggersFilePath { get; set; }

        public IReadOnlyCollection<string> TablesChecksFilePath { get; set; }


        public IReadOnlyCollection<string> ViewsBodyFilePath { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Indices")]
        public IReadOnlyCollection<string> ViewsIndicesFilePath { get; set; }

        public IReadOnlyCollection<string> ViewsTriggersFilePath { get; set; }

        public IReadOnlyCollection<string> ProceduresFilePath { get; set; }

        //major update
        public string MajorUpdateFilePath { get; set; }

    }
}

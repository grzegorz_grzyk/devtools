﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.DevTools.Model.SolutionModel
{
    public class Solution
    {
        public string FilePath { get; set; }

        //public string DirectoryPath { get; set; }
        public bool IsCommonModuleDbPresent
        {
            get { return SqlProjects.SingleOrDefault(x => x.ModuleKey == "Common") != null; }
        }

        private IReadOnlyCollection<CsProject> _csProjects;

        public IReadOnlyCollection<CsProject> CsProjects
        {
            get { return _csProjects ?? Enumerable.Empty<CsProject>().ToArray(); }
            set { _csProjects = value; }
        }

        private IReadOnlyCollection<SqlProject> _sqlProjects;

        public IReadOnlyCollection<SqlProject> SqlProjects
        {
            get { return _sqlProjects ?? Enumerable.Empty<SqlProject>().ToArray(); }
            set { _sqlProjects = value; }
        }

    }
}

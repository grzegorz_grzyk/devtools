﻿using System.Data;

namespace SolvesIT.DevTools.Model
{
    public class SqlCommandResult
    {
        public DataSet Data { get; set; }

        public string Message { get; set; }
    }
}
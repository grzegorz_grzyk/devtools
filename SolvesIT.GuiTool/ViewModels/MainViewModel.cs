﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Qdarc.Utils;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Logic;
using SolvesIT.DevTools.Logic.Builders;
using SolvesIT.DevTools.Logic.DdlReaders;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.ViewModels
{
    internal class MainViewModel
        : PropertyChangedBase
        , IHandle<PrintToConsole>
    {
        private readonly Func<AppSettingsViewModel> _appSettingsViewModelFactory;

        private readonly IDdlService _ddlService;

        private readonly ISqlService _sqlService;

        private readonly IUpdater _updater;

        private readonly IWindowManager _windowManager;

        private DdCatalog _dbObject;

        private bool _isRunButtonEnabled;

        private bool _isUpdateClassButtonEnabled = true;

        private bool _isUpdateDatabaseButtonEnabled = true;

        private QueryEditorViewModel _queryEditorViewModel;

        private ResultsViewModel _results = new ResultsViewModel();

        private ICollection<ValidationMessage> _validationMessages;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "SolvesIT.DevTools.ViewModels.ResultsViewModel.Update(System.String,System.Collections.Generic.IEnumerable<System.Object>)")]
        public MainViewModel(
            IEventAggregator eventAggregator,
            IWindowManager windowManager,
            Func<AppSettingsViewModel> appSettingsViewModelFactory,
            ISqlService sqlService,
            Func<GeneratorModel> generatorModelFactory,
            QueryEditorViewModel queryEditorViewModel,
            IDdlService ddlService,
            ITemplateService templateService,
            IUpdater updater)
        {
            QueryEditorViewModel = queryEditorViewModel;
            _ddlService = ddlService;
            _updater = updater;
            _windowManager = windowManager;
            _appSettingsViewModelFactory = appSettingsViewModelFactory;
            _sqlService = sqlService;

            _validationMessages = new Collection<ValidationMessage>();
            Generators = templateService.Load(_validationMessages).EmptyIfNull().Select(x =>
            {
                var f = generatorModelFactory();
                f.Generator = x;
                f.DdlCatalog = () => _dbObject;
                return f;
            });
            if (_validationMessages.Any())
            {
                Results.Update("Loading templates Error", _validationMessages);
            }
            else
            {
                Results.AppendOutputText("Loading templates OK");
            }

            eventAggregator.Subscribe(this);
            QueryEditorViewModel.PropertyChanged += (s, x) =>
            {
                if (x.PropertyName == "ConsoleText")
                {
                    IsRunButtonEnabled = !string.IsNullOrWhiteSpace(QueryEditorViewModel.ConsoleText);
                }
            };
        }

        public IEnumerable<GeneratorModel> Generators { get; set; }

        public bool IsRunButtonEnabled
        {
            get { return _isRunButtonEnabled; }
            set
            {
                _isRunButtonEnabled = value;
                NotifyOfPropertyChange(() => IsRunButtonEnabled);
            }
        }

        public bool IsUpdateClassButtonEnabled
        {
            get
            {
                return _isUpdateClassButtonEnabled;
            }
            set
            {
                _isUpdateClassButtonEnabled = value;
                NotifyOfPropertyChange(() => IsUpdateClassButtonEnabled);
            }
        }

        public bool IsUpdateDatabaseButtonEnabled
        {
            get
            {
                return _isUpdateDatabaseButtonEnabled;
            }
            set
            {
                _isUpdateDatabaseButtonEnabled = value;
                NotifyOfPropertyChange(() => IsUpdateDatabaseButtonEnabled);
            }
        }

        public QueryEditorViewModel QueryEditorViewModel
        {
            get
            {
                return _queryEditorViewModel;
            }
            set
            {
                _queryEditorViewModel = value;
                NotifyOfPropertyChange(() => QueryEditorViewModel);
            }
        }

        public ResultsViewModel Results
        {
            get { return _results; }
            set
            {
                _results = value;
                NotifyOfPropertyChange(() => Results);
            }
        }

        public async void CallSqlStructureReader()
        {
            _dbObject = await _ddlService.LoadDatabase();
            Results.AppendOutputText("Data base object Readed");
        }

        public async void CallUpdateClass()
        {
            IsUpdateClassButtonEnabled = false;
            var timer = new Stopwatch();
            timer.Start();
            _validationMessages.Clear();
            var message = await _updater.UpdateClass(_validationMessages);
            if (_validationMessages.Any())
            {
                Results.Update(message, _validationMessages);
            }
            else
            {
                Results.AppendOutputText(message);
            }

            await CallUpdateConsts();
            timer.Stop();
            Results.AppendOutputText(string.Format("CallUpdateClass elapsed time: {0:hh\\:mm\\:ss\\.fff}", timer.Elapsed));

            IsUpdateClassButtonEnabled = true;
        }

        public async void CallUpdateDatabase()
        {
            IsUpdateDatabaseButtonEnabled = false;
            var timer = new Stopwatch();
            timer.Start();

            _validationMessages.Clear();
            var message = await _updater.UpdateDatabase(_validationMessages);
            if (_validationMessages.Any())
            {
                Results.Update(message, _validationMessages);
            }
            else
            {
                Results.AppendOutputText(message);
            }

            timer.Stop();
            Results.AppendOutputText(string.Format("UpdateDatabase elapsed time: {0:hh\\:mm\\:ss\\.fff}", timer.Elapsed));
            IsUpdateDatabaseButtonEnabled = true;
        }

        public void Handle(PrintToConsole message)
        {
            QueryEditorViewModel.ConsoleText = message.Query;
        }

        public void OpenAppSettingsDialog()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.WindowStyle = WindowStyle.ToolWindow;
            settings.ShowInTaskbar = false;
            settings.Title = "Settings dialog";
            settings.ResizeMode = ResizeMode.NoResize;
            var dialog = _appSettingsViewModelFactory();
            _windowManager.ShowDialog(dialog, null, settings);
        }

        public async void Run()
        {
            IsRunButtonEnabled = false;
            var commanResult = await _sqlService.ExecuteCommandAsync(QueryEditorViewModel.ConsoleText);
            Results.Update(commanResult);
            IsRunButtonEnabled = true;
        }

        private async Task CallUpdateConsts()
        {
            //IsUpdateClassButtonEnabled = false;
            var timer = new Stopwatch();
            timer.Start();
            _validationMessages.Clear();
            var message = await _updater.UpdateConsts(_validationMessages);
            if (_validationMessages.Any())
            {
                Results.Update(message, _validationMessages);
            }
            else
            {
                Results.AppendOutputText(message);
            }

            timer.Stop();
            Results.AppendOutputText(string.Format("CallUpdateConsts elapsed time: {0:hh\\:mm\\:ss\\.fff}", timer.Elapsed));
            //IsUpdateClassButtonEnabled = true;
        }
    }
}
﻿using System.Collections.Generic;
using Caliburn.Micro;
using SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers;

namespace SolvesIT.DevTools.ViewModels
{
    internal class QueryEditorViewModel : PropertyChangedBase
    {
        private Colorizer _transformer;
        private string _consoleText;

        public QueryEditorViewModel(IEnumerable<IColorizerRule> colorizers)
        {
            Transformer = new Colorizer(colorizers);
        }

        public Colorizer Transformer
        {
            get
            {
                return _transformer;
            }
            set
            {
                _transformer = value;
                NotifyOfPropertyChange(() => Transformer);
            }
        }

        public string ConsoleText
        {
            get { return _consoleText; }
            set
            {
                _consoleText = value;
                NotifyOfPropertyChange(() => ConsoleText);
            }
        }
    }
}
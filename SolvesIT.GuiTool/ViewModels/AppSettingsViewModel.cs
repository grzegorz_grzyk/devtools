﻿using Caliburn.Micro;
using SolvesIT.DevTools.Logic;
using System;
using System.Collections.Generic;
using System.Windows;
using SolvesIT.GuiTool.Properties;
using Settings = SolvesIT.DevTools.Model.SettingsModel.Settings;

namespace SolvesIT.DevTools.ViewModels
{
    internal class AppSettingsViewModel
        : Screen
    {
        private const int IntegratedSecurity = 1;
        private const int SqlAuthentification = 2;
        private int _selectedAuthType;
        private readonly Settings _settings;
        private readonly ISettingsService _settingsService;


        public AppSettingsViewModel(
            Settings settings,
            ISettingsService settingsService)
        {
            _settings = settings;
            _settingsService = settingsService;
            AuthType = new[] {
                Tuple.Create(IntegratedSecurity,"Integrated Security"),
                Tuple.Create(SqlAuthentification,"SQL Server Authentification")
            };

            SqlInstanceName = _settings.Sql.InstanceName;
            SqlDbName = _settings.Sql.DbName;
            SelectedAuthType = _settings.Sql.IsIntegratedSecurity ? IntegratedSecurity : SqlAuthentification;
            SqlUser = _settings.Sql.User;
            SqlPassword = _settings.Sql.Password;


            SqlInstanceName = _settings.Sql.InstanceName;
            SqlDbName = _settings.Sql.DbName
                ;
            SelectedAuthType = _settings.Sql.IsIntegratedSecurity ? IntegratedSecurity : SqlAuthentification;
            SqlUser = _settings.Sql.User;
            SqlPassword = _settings.Sql.Password;

            SolutionFile = _settings.SolutionFile;
            TemplateDirectoryPath = _settings.Template.DirectoryPath;

        }




        private string _sqlInstanceName;
        public string SqlInstanceName
        {
            get { return _sqlInstanceName; }
            set
            {
                _sqlInstanceName = value;
                NotifyOfPropertyChange(() => SqlInstanceName);
            }
        }

        private bool _isIntegratedSecurity;
        public bool IsIntegratedSecurity
        {
            get { return _isIntegratedSecurity; }
            set
            {
                _isIntegratedSecurity = value;
                NotifyOfPropertyChange(() => IsIntegratedSecurity);
            }
        }
        public IEnumerable<Tuple<int, string>> AuthType { get; set; }

        public int SelectedAuthType
        {
            get { return _selectedAuthType; }
            set
            {
                _selectedAuthType = value;

                NotifyOfPropertyChange(() => SelectedAuthType);
                IsIntegratedSecurity = _selectedAuthType == SqlAuthentification;
            }
        }

        private string _sqlUser;
        public string SqlUser
        {
            get { return _sqlUser; }
            set
            {
                _sqlUser = value;
                NotifyOfPropertyChange(() => SqlUser);
            }
        }

        private string _sqlPassword;
        public string SqlPassword
        {
            get { return _sqlPassword; }
            set
            {
                _sqlPassword = value;
                NotifyOfPropertyChange(() => SqlPassword);
            }
        }

        private string _sqlDbName;
        public string SqlDbName
        {
            get { return _sqlDbName; }
            set
            {
                _sqlDbName = value;
                NotifyOfPropertyChange(() => SqlDbName);
            }
        }

        private string _solutionFile;

        public string SolutionFile
        {
            get { return _solutionFile; }

            set
            {
                _solutionFile = value;
                NotifyOfPropertyChange(() => SolutionFile);
            }
        }

        private string _templateDirectoryPath;

        public string TemplateDirectoryPath
        {
            get { return _templateDirectoryPath; }

            set
            {_templateDirectoryPath = value;
                NotifyOfPropertyChange(() => TemplateDirectoryPath);
            }
        }


        public void Cancel()
        {
            TryClose();
        }

        public void Save()
        {
            if (string.IsNullOrWhiteSpace(SqlInstanceName)
                || string.IsNullOrWhiteSpace(SqlDbName))
            {
                MessageBox.Show(Resources.Null_SqlInstance_value_or_DbName_value, Resources.Not_Declared_values);
                return;
            }

            if (IsIntegratedSecurity
                && string.IsNullOrWhiteSpace(SqlUser)
                && string.IsNullOrWhiteSpace(SqlPassword))
            {
                MessageBox.Show(Resources.Null_Login_value_or_Password_value, Resources.Not_Declared_values);
                return;
            }

            _settings.Sql.InstanceName = SqlInstanceName;
            _settings.Sql.DbName = SqlDbName;
            _settings.Sql.IsIntegratedSecurity = !IsIntegratedSecurity;
            _settings.Sql.User = SqlUser;
            _settings.Sql.Password = SqlPassword;

            _settings.SolutionFile = SolutionFile;
            _settings.Template.DirectoryPath = TemplateDirectoryPath;

            _settingsService.Save();

            TryClose();
        }
    }
}
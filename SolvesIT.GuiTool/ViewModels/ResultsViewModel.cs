﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Data;
using SolvesIT.DevTools.Model;

namespace SolvesIT.DevTools.ViewModels
{
    internal class ResultsViewModel
        : PropertyChangedBase
    {
        private string _outputText;

        private int _selectedTab;

        private ObservableCollection<DataView> _tables;

        public string OutputText
        {
            get { return _outputText; }
            set
            {
                _outputText = value;
                NotifyOfPropertyChange(() => OutputText);
            }
        }

        public int SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                _selectedTab = value;
                NotifyOfPropertyChange(() => SelectedTab);
            }
        }

        public ObservableCollection<DataView> Tables
        {
            get { return _tables; }
            set
            {
                _tables = value;
                NotifyOfPropertyChange(() => Tables);
            }
        }

        public void Update(SqlCommandResult commandResults)
        {
            AppendOutputText(commandResults.Message);

            var tables = new ObservableCollection<DataView>();
            foreach (DataTable table in commandResults.Data.Tables)
            {
                Tables.Add(table.AsDataView());
            }
            Tables = tables;
            SelectedTab = 0;
        }

        public void AppendOutputText(string message)
        {
            OutputText += message + Environment.NewLine;
        }

        public void Update(string message, IEnumerable<object> results)
        {
            AppendOutputText(message);

            var item = results.ElementAt(0);

            var properties = item.GetType().GetProperties().ToArray();

            using (var table = new DataTable())
            {
                table.Locale = CultureInfo.InvariantCulture;
                foreach (var property in properties)
                {
                    table.Columns.Add(property.Name, property.PropertyType);
                }
                foreach (var result in results)
                {
                    var c = new Collection<object>();
                    foreach (var property in properties)
                        c.Add(property.GetValue(result));
                    table.Rows.Add(c.ToArray());
                }


                if (Tables == null)
                {
                    Tables = new ObservableCollection<DataView> { table.AsDataView() };
                }
                else
                {
                    Tables.Add(table.AsDataView());
                }
            }
            SelectedTab = 0;
        }
    }
}
﻿namespace SolvesIT.DevTools.ViewModels
{
    public class PrintToConsole
    {
        private readonly string _query;

        public PrintToConsole(string query)
        {
            _query = query;
        }

        public string Query
        {
            get { return _query; }
        }
    }
}
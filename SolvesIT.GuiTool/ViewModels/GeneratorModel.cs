﻿using System;
using Caliburn.Micro;
using SolvesIT.AutomationEngine.Abstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DevTools.Logic.Templates;
using SolvesIT.DevTools.Model.TemplateModel;

namespace SolvesIT.DevTools.ViewModels
{
    class GeneratorModel
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ITemplateService _templateService;

        public GeneratorModel(
            IEventAggregator eventAggregator,
            ITemplateService templateService)
        {
            _eventAggregator = eventAggregator;
            _templateService = templateService;
        }

        public Generator Generator { get; set; }
        public Func<DdCatalog> DdlCatalog { get; set; }

        public void RunGenerator()
        {
            string code;
            if (DdlCatalog() != null)
            {

                code = _templateService.RunGenerator(Generator, DdlCatalog());
            }
            else
            {
                code = Generator.Name;
            }
            _eventAggregator.Publish(new PrintToConsole(code));
        }
    }
}

using System.Windows;
using System.Windows.Media;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    class HighlightingColor
    {
        public Brush Foreground { get; set; }

        public Brush Background { get; set; }

        public FontStyle? FontStyle { get; set; }

        public FontWeight? FontWeight { get; set; }

        public HighlightingColor Merge(HighlightingColor highlightingColor)
        {
            return new HighlightingColor
            {
                Foreground = Foreground ?? highlightingColor.Foreground,
                Background = Background ?? highlightingColor.Background,
                FontStyle = FontStyle ?? highlightingColor.FontStyle,
                FontWeight = FontWeight ?? highlightingColor.FontWeight,
            };
        }
    }
}
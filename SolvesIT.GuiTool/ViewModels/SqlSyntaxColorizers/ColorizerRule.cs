﻿using System.Collections.Generic;
using System.Linq;
using Qdarc.Utils;
using SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers;

namespace SolvesIT.GuiTool.ViewModels.SqlSyntaxColorizers
{
    abstract class ColorizerRule<TSelectionProvider> : IColorizerRule
    {

        private IEnumerable<TSelectionProvider> _currentItems = Enumerable.Empty<TSelectionProvider>();

        private IEnumerable<int> _changedLines;
        private readonly double _priority;

        public ColorizerRule(double priority)
        {
            _priority = priority;
        }

        public void DetectChanges(ParseReslut result)
        {
            var items = GetItems(result).ToArray();
            _changedLines = items.Except(
                                _currentItems,
                                GenericEqualityComparer.Create<TSelectionProvider>(Compare))
                                .SelectMany(GetLineNumbers)
                        .ToArray();

            _currentItems = items;
        }

        protected abstract IEnumerable<int> GetLineNumbers(TSelectionProvider item);

        protected abstract bool Compare(TSelectionProvider first, TSelectionProvider second);

        protected abstract IEnumerable<TSelectionProvider> GetItems(ParseReslut result);

        public IEnumerable<int> GetChangedLines()
        {
            return _changedLines;
        }

        public IEnumerable<IMarkedSegment> GetMarkedSegments(int lineNumber)
        {
            return _currentItems.Where(x => GetLineNumbers(x).Any(z => z == lineNumber))
                 .Select(CreateSegment)
                 .ToArray();
        }

        protected abstract IMarkedSegment CreateSegment(TSelectionProvider arg);

        public double Priority
        {
            get { return _priority; }
        }
    }
}



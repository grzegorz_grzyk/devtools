using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    class FunctionCallColorizer : SqlFragmentColorizer<Identifier>
    {
        public FunctionCallColorizer()
            : base(200)
        {
        }

        protected override HighlightingColor GetHighlighting(Identifier arg)
        {
            return new HighlightingColor
            {
                Foreground = new SolidColorBrush(Colors.Crimson),
                FontStyle = FontStyles.Italic
            };
        }

        protected override IEnumerable<Identifier> GetItems(ParseReslut result)
        {
            var visitor = new FunctionCallsFinder();
            result.Fragment.Accept(visitor);

            return visitor.SqlFragments;
        }
    }
}
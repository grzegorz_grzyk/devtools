using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    class AliasColorizer : SqlFragmentColorizer<Identifier>
    {

        public AliasColorizer()
            : base(200)
        {

        }
        protected override HighlightingColor GetHighlighting(Identifier arg)
        {
            return new HighlightingColor
            {
                Foreground = new SolidColorBrush(_colorsAssignment[arg.Value]),
            };
        }
        protected override IEnumerable<Identifier> GetItems(ParseReslut result)
        {
            var visitor = new AliasFinder();
            result.Fragment.Accept(visitor);

            var values = visitor.SqlFragments.Values
                .Where(x => x.IsTableAlias)
                .OrderBy(x => x.Main.StartOffset);

            _colorsAssignment = values.Select((x, i) => new { Id = i, AliasDescriptor = x })
                .ToDictionary(x => x.AliasDescriptor.Main.Value, x => _colors[x.Id % _colors.Count]);

            return values.SelectMany(x => x.Fragments);
        }


        Dictionary<string, Color> _colorsAssignment = new Dictionary<string, Color>();
        List<Color> _colors = new List<Color>
        {
            Colors.SteelBlue,
            Colors.BurlyWood,
            Colors.Chocolate,
            Colors.Crimson,
            Colors.Gold,
            Colors.DarkGray
        };
    }
}
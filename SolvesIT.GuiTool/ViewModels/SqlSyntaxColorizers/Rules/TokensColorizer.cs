using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using SolvesIT.GuiTool.ViewModels.SqlSyntaxColorizers;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    internal abstract class TokensColorizer : ColorizerRule<TSqlParserToken>
    {
        public TokensColorizer()
            : base(0)
        {
        }

        protected sealed override IEnumerable<TSqlParserToken> GetItems(ParseReslut result)
        {
            if (result.Fragment == null)
            {
                return Enumerable.Empty<TSqlParserToken>();
            }

            return GetHandledTokens(result);
        }

        protected abstract IEnumerable<TSqlParserToken> GetHandledTokens(ParseReslut result);

        protected override sealed IEnumerable<int> GetLineNumbers(TSqlParserToken item)
        {
            return Enumerable.Range(
                item.Line,
                item.Text.Count(x => x == '\r') + 1);

        }

        protected sealed override bool Compare(TSqlParserToken first, TSqlParserToken second)
        {
            return first.Line == second.Line && first.Column == second.Column && first.Text == second.Text;
        }
    }
}
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using SolvesIT.GuiTool.ViewModels.SqlSyntaxColorizers;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    internal sealed class ErrorsColorizer : ColorizerRule<ParseError>
    {
        public ErrorsColorizer()
            : base(double.MaxValue)
        {
        }

        protected override IEnumerable<int> GetLineNumbers(ParseError item)
        {
            return new[]
            {
                item.Line
            };
        }

        protected override bool Compare(ParseError first, ParseError second)
        {
            return first.Line == second.Line && first.Column == second.Column && first.Number == second.Number;
        }

        protected override IEnumerable<ParseError> GetItems(ParseReslut result)
        {
            return result.Errors;
        }

        protected override IMarkedSegment CreateSegment(ParseError arg)
        {
            return new MarkedSegment
            {
                EndOffset = arg.Offset + 1,
                Offset = arg.Offset - 1,
                Length = 1,
                Highlighting = new HighlightingColor
                {
                    Foreground = new SolidColorBrush(Colors.White),
                    Background = new SolidColorBrush(Colors.Red),
                    FontWeight = FontWeights.ExtraBold
                }
            };
        }
    }
}
﻿using System.Collections.Generic;
using Microsoft.SqlServer.TransactSql.ScriptDom;
using SolvesIT.GuiTool.ViewModels.SqlSyntaxColorizers;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    abstract class SqlFragmentColorizer<TSelectionProvider> : ColorizerRule<TSelectionProvider>
        where TSelectionProvider : TSqlFragment
    {
        protected SqlFragmentColorizer(double priority)
            : base(priority)
        {
        }

        protected override IMarkedSegment CreateSegment(TSelectionProvider arg)
        {
            return new MarkedSegment
            {
                EndOffset = arg.StartOffset + arg.FragmentLength,
                Offset = arg.StartOffset,
                Length = 1,
                Highlighting = GetHighlighting(arg)
            };
        }

        protected abstract HighlightingColor GetHighlighting(TSelectionProvider arg);


        protected override IEnumerable<int> GetLineNumbers(TSelectionProvider item)
        {
            return new[]
            {
                item.StartLine
            };
        }

        protected override bool Compare(TSelectionProvider first, TSelectionProvider second)
        {
            return first.StartOffset == second.StartOffset && first.FragmentLength == second.FragmentLength;
        }

    }
}
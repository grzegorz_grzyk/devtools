using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    sealed class KeywordsColorizer : TokensColorizer
    {
        protected override IEnumerable<TSqlParserToken> GetHandledTokens(ParseReslut result)
        {
            return result.Fragment
                .ScriptTokenStream
                .Where(x => x.IsKeyword())
                .ToArray();
        }

        protected override IMarkedSegment CreateSegment(TSqlParserToken arg)
        {
            return new MarkedSegment
            {
                EndOffset = arg.Offset + arg.Text.Length,
                Length = arg.Text.Length,
                Offset = arg.Offset,
                Highlighting = new HighlightingColor
                {
                    Foreground = new SolidColorBrush(Colors.Blue),
                    FontWeight = FontWeights.Bold
                }
            };
        }
    }
}
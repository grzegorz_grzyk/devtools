using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    sealed class StringLiteralsColorizer : TokensColorizer
    {
        protected override IEnumerable<TSqlParserToken> GetHandledTokens(ParseReslut result)
        {
            return result.Fragment
                .ScriptTokenStream
                .Where(x => x.TokenType == TSqlTokenType.UnicodeStringLiteral)
                .ToArray();
        }

        protected override IMarkedSegment CreateSegment(TSqlParserToken arg)
        {
            return new MarkedSegment
            {
                EndOffset = arg.Offset + arg.Text.Length,
                Length = arg.Text.Length,
                Offset = arg.Offset,
                Highlighting = new HighlightingColor
                {
                    Foreground = new SolidColorBrush(Colors.DarkRed),
                }
            };
        }
    }
}
using System.Collections.Generic;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    class AliasDescriptor
    {
        public List<Identifier> Fragments { get; private set; }

        public bool IsTableAlias { get; set; }

        public AliasDescriptor()
        {
            Fragments = new List<Identifier>();
        }

        public Identifier Main { get; set; }
    }
}
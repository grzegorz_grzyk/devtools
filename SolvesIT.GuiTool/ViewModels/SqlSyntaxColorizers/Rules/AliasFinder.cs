using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    class AliasFinder : TSqlFragmentVisitor
    {
        private readonly Dictionary<string, AliasDescriptor> _sqlFragments 
            = new Dictionary<string, AliasDescriptor>();

        public Dictionary<string, AliasDescriptor> SqlFragments
        {
            get
            {
                return _sqlFragments;
            }
        }

        public override void ExplicitVisit(SelectStatement node)
        {
            base.ExplicitVisit(node);
        }

        public override void ExplicitVisit(FunctionCall node)
        {
            base.ExplicitVisit(node);
        }

        public override void ExplicitVisit(ColumnReferenceExpression node)
        {
            if (node.MultiPartIdentifier.Count > 1)
                AddIdentifier(node.MultiPartIdentifier.Identifiers.Reverse().Skip(1).First());
            base.ExplicitVisit(node);
        }

        private void AddIdentifier(Identifier node)
        {
            if (!_sqlFragments.ContainsKey(node.Value))
            {
                _sqlFragments.Add(
                    node.Value,
                    new AliasDescriptor());
            }

            _sqlFragments[node.Value].Fragments.Add(node);
        }

        public override void ExplicitVisit(NamedTableReference node)
        {
            if (node.Alias != null)
            {
                AddIdentifier(node.Alias);

                if (_sqlFragments[node.Alias.Value].Main == null)
                {
                    _sqlFragments[node.Alias.Value].IsTableAlias = true;
                    _sqlFragments[node.Alias.Value].Main = node.Alias;
                }
            }
            base.ExplicitVisit(node);
        }

        public override void ExplicitVisit(QueryDerivedTable node)
        {
            if (node.Alias != null)
            {
                AddIdentifier(node.Alias);

                if (_sqlFragments[node.Alias.Value].Main == null)
                {
                    _sqlFragments[node.Alias.Value].IsTableAlias = true;
                    _sqlFragments[node.Alias.Value].Main = node.Alias;
                }
            }
            base.ExplicitVisit(node);
        }
    }
}
using System.Collections.Generic;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules
{
    class FunctionCallsFinder : TSqlFragmentVisitor
    {
        private readonly List<Identifier> _sqlFragments = new List<Identifier>();

        public IEnumerable<Identifier> SqlFragments
        {
            get
            {
                return _sqlFragments;
            }
        }

        public override void ExplicitVisit(SelectStatement node)
        {
            base.ExplicitVisit(node);
        }

        public override void ExplicitVisit(FunctionCall node)
        {
            _sqlFragments.Add(node.FunctionName);
            base.ExplicitVisit(node);
        }



    }
}
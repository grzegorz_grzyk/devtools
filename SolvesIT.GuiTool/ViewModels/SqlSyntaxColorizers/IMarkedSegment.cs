namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    interface IMarkedSegment
    {
        int Offset { get; }

        int Length { get; }

        int EndOffset { get; }

        HighlightingColor Highlighting { get; set; }
    }
}
using System.Collections.Generic;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    interface IColorizerRule
    {
        double Priority { get; }

        void DetectChanges(ParseReslut result);

        IEnumerable<int> GetChangedLines();

        IEnumerable<IMarkedSegment> GetMarkedSegments(int lineNumber);
    }
}
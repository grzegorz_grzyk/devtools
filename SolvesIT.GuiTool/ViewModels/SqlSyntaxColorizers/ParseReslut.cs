using System.Collections.Generic;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    class ParseReslut
    {
        public TSqlFragment Fragment { get; set; }
        public IEnumerable<ParseError> Errors { get; set; }
    }
}
namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    class MarkedSegment : IMarkedSegment
    {
        public int EndOffset { get; set; }

        public int Length { get; set; }

        public int Offset { get; set; }

        public HighlightingColor Highlighting { get; set; }
    }
}
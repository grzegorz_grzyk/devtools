using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Rendering;
using Microsoft.SqlServer.TransactSql.ScriptDom;

namespace SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers
{
    internal class Colorizer : DocumentColorizingTransformer
    {
        private string _previousText;
        private Task _process;
        private volatile bool _shouldRerun;
        private TextView _textView;

        private readonly IEnumerable<IColorizerRule> _rules;

        public Colorizer(IEnumerable<IColorizerRule> rules)
        {
            _rules = rules;
        }

        public async void Update(string text)
        {
            if (object.Equals(text, _previousText))
                return;

            _previousText = text;

            if (_process == null || _process.IsCompleted || _process.IsFaulted)
            {
                _process = Task.Run(() =>
                {
                    IList<ParseError> errors;
                    TSqlFragment fragment;

                    do
                    {
                        _shouldRerun = false;
                        var parser = new TSql110Parser(true);
                        fragment = parser.Parse(
                            new StringReader(text),
                            out errors);
                    }
                    while (_shouldRerun);

                    var result = new ParseReslut
                    {
                        Fragment = fragment,
                        Errors = errors
                    };

                    if (result.Fragment != null && result.Fragment.ScriptTokenStream == null)
                    {
                        result.Fragment.ScriptTokenStream = new TSqlParserToken[0];
                    }

                    foreach (var colorizerRule in _rules)
                    {
                        colorizerRule.DetectChanges(result);
                    }
                });

                await _process;

                var changedLines = _rules.SelectMany(x => x.GetChangedLines()).Distinct().ToArray();

                if (changedLines.Count() == 1)
                {
                    _textView.Redraw(_textView.Document.GetLineByNumber(changedLines.Single()));
                }
                else if (changedLines.Any())
                {
                    var fromLine = _textView.Document.GetLineByNumber(changedLines.Min());
                    var toLine = _textView.Document.GetLineByNumber(changedLines.Max());
                    int startOffset = fromLine.Offset;
                    _textView.Redraw(startOffset, toLine.EndOffset - startOffset);
                }
            }
            else
            {
                _shouldRerun = true;
            }
        }

        protected override void ColorizeLine(DocumentLine line)
        {
            var segments = _rules.SelectMany(x => x.GetMarkedSegments(line.LineNumber)
                .Where(z => z.EndOffset >= line.Offset || z.Offset <= line.EndOffset)
                .Select(z => new
                {
                    x.Priority,
                    Segment = z
                }))
                .GroupBy(x => new
                {
                    x.Segment.EndOffset,
                    x.Segment.Offset,
                    x.Segment.Length
                })
                .Select(x => x.OrderByDescending(z => z.Priority)
                    .Aggregate((IMarkedSegment)null,
                        (z, y) =>
                        {
                            if (z == null)
                            {
                                return y.Segment;
                            }

                            var highlighting = z.Highlighting.Merge(y.Segment.Highlighting);
                            z.Highlighting = highlighting;
                            return z;
                        }));

            foreach (var segment in segments)
            {
                var position = Math.Min(Math.Max(segment.Offset, line.Offset), line.EndOffset);
                var end = Math.Max(Math.Min(segment.EndOffset, line.EndOffset), position);
                var currentSegment = segment;
                base.ChangeLinePart(position, end,
                    element =>
                    {
                        if (currentSegment.Highlighting.Foreground != null)
                            element.TextRunProperties.SetForegroundBrush(currentSegment.Highlighting.Foreground);

                        if (currentSegment.Highlighting.Background != null)
                            element.TextRunProperties.SetBackgroundBrush(currentSegment.Highlighting.Background);


                        element.TextRunProperties.SetTypeface(
                            new Typeface(
                                element.TextRunProperties.Typeface.FontFamily,
                                currentSegment.Highlighting.FontStyle ?? element.TextRunProperties.Typeface.Style,
                                currentSegment.Highlighting.FontWeight ?? element.TextRunProperties.Typeface.Weight,
                                element.TextRunProperties.Typeface.Stretch));
                    });
            }
        }

        protected override void OnAddToTextView(TextView textView)
        {
            this._textView = textView;
            textView.DocumentChanged += textView_DocumentChanged;
            textView.VisualLineConstructionStarting += textView_VisualLineConstructionStarting;
            textView.VisualLinesChanged += textView_VisualLinesChanged;
        }

        protected override void OnRemoveFromTextView(TextView textView)
        {
            textView.DocumentChanged -= textView_DocumentChanged;
            textView.VisualLineConstructionStarting -= textView_VisualLineConstructionStarting;
            textView.VisualLinesChanged -= textView_VisualLinesChanged;
        }

        private void textView_DocumentChanged(object sender, EventArgs e)
        {
            Update(_textView.Document.Text);
        }

        private void textView_VisualLineConstructionStarting(object sender, VisualLineConstructionStartEventArgs e)
        {
            Update(_textView.Document.Text);
        }

        private void textView_VisualLinesChanged(object sender, EventArgs e)
        {
            Update(_textView.Document.Text);
        }
    }
}
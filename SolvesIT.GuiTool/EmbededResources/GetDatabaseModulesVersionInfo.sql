DECLARE @Sql VARCHAR(MAX)
      , @ModuleTable VARCHAR(MAX) = '[Common].[Module]'
SET @Sql = 'SELECT'

IF OBJECT_ID(@ModuleTable) IS NOT NULL
BEGIN
    SET @Sql = @Sql + ' [m].[Key] AS [Key], CAST((CAST([m].[MajorVersion] AS DECIMAL(28, 2)) + CAST([m].[DevVersion] AS DECIMAL(28, 2)) / 100)  AS DECIMAL(28, 2)) AS [Version]'
    IF COL_LENGTH(@ModuleTable,'RevisionNumber') IS NOT NULL
        SET @Sql = @Sql + ', [m].[RevisionNumber] AS [Revision]'
    ELSE
        SET @Sql = @Sql + ', NULL AS [Revision]'
    SET @Sql = @Sql + ' FROM '+@ModuleTable+' AS [m]'
END
ELSE
BEGIN
    SET @Sql = @Sql + ' NULL AS [Key], NULL AS [Version], NULL AS [Revision] FROM [sys].[objects] WHERE 1=0'
END

EXEC (@Sql);

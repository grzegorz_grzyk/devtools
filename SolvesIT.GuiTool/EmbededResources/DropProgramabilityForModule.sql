BEGIN -- DROP Stored Procedures
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
              [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [o].[schema_id] 
                                        AND [s].[name] = @SchemaName
            WHERE [o].[type] = 'p';
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('DROP PROCEDURE ['+@SchemaName+'].['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO  @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP Triggers
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
               [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [o].[schema_id]
                                        AND [s].[name] = @SchemaName

            WHERE [o].[type] = 'TR';
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('DROP TRIGGER ['+@SchemaName+'].['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP Indices
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ParentName VARCHAR(MAX), 
            @ObjectName VARCHAR(MAX), 
            @IsConstraint BIT;

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
                [o].[name] AS [ParentName]
              , [i].[name] AS [ObjectName]
              , [i].[is_unique_constraint] AS [IsConstraint]
            FROM [sys].[indexes] AS [i]
            JOIN [sys].[objects] AS [o] ON [o].object_id = [i].object_id
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [o].[schema_id]
                                        AND [s].[name] = @SchemaName
            WHERE [i].[is_primary_key] = 0
              AND [i].[index_id] > 0;
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO  @ParentName, @ObjectName, @IsConstraint;
    WHILE @@fetch_status = 0
        BEGIN
            IF @IsConstraint = 1
                BEGIN EXEC ('ALTER TABLE ['+@SchemaName+'].['+@ParentName+'] DROP CONSTRAINT ['+@ObjectName+']');
                END;
            ELSE
                BEGIN EXEC ('DROP INDEX ['+@ObjectName+'] ON ['+@SchemaName+'].['+@ParentName+']');
                END;

            FETCH NEXT FROM ObjectCursor INTO @ParentName, @ObjectName, @IsConstraint;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP ForeignKeys
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX), 
            @ParentName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
                [po].[Name] AS [ParentName]
              , [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[objects] AS [po] ON [po].object_id = [o].[parent_object_id]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [po].[schema_id]
                                        AND [s].[name] = @SchemaName
            WHERE [o].[type] = 'F';
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO  @ParentName, @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('ALTER TABLE ['+@SchemaName+'].['+@ParentName+'] DROP CONSTRAINT ['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO  @ParentName, @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP Constraints
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ParentName VARCHAR(MAX),
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
                [po].[Name] AS [ParentName]
              , [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[objects] AS [po] ON [po].object_id = [o].[parent_object_id]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [po].[schema_id]
                                        AND [s].[name] = @SchemaName
            WHERE [o].[type] = 'C';
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ParentName, @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('ALTER TABLE ['+@SchemaName+'].['+@ParentName+'] DROP CONSTRAINT ['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO @ParentName, @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END

GO
BEGIN -- DROP Views
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
                [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [o].[schema_id]
                                        AND [s].[name] = @SchemaName
            WHERE [o].[type] ='V';
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('DROP VIEW ['+@SchemaName+'].['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP Functions
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
                [o].[name] AS [ObjectName]
            FROM [sys].[objects] AS [o]
            JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [o].[schema_id]
                                        AND [s].[name] = @SchemaName
            WHERE [o].[type] IN (N'AF', N'FN', N'FS', N'FT', N'IF', N'TF');
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('DROP FUNCTION ['+@SchemaName+'].['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;

GO
BEGIN -- DROP Types
    DECLARE @SchemaName VARCHAR(MAX) = '##SchemaKey##', 
            @ObjectName VARCHAR(MAX);

    DECLARE ObjectCursor CURSOR FAST_FORWARD
        FOR SELECT
               [t].[name] AS [ObjectName]
              FROM sys.types t
              JOIN [sys].[schemas] AS [s] ON [s].[schema_id] = [t].[schema_id]
                                          AND [s].[name] = @SchemaName
    OPEN ObjectCursor;
    FETCH NEXT FROM ObjectCursor INTO @ObjectName;
    WHILE @@fetch_status = 0
        BEGIN
            EXEC ('DROP TYPE ['+@SchemaName+'].['+@ObjectName+']');
            FETCH NEXT FROM ObjectCursor INTO @ObjectName;
        END;
    CLOSE ObjectCursor;
    DEALLOCATE ObjectCursor;
END;
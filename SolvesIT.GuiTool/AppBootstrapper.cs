﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Qdarc.Utils.Ioc.Unity;
using SolvesIT.DevTools;
using SolvesIT.DevTools.ViewModels;
using SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers;
using SolvesIT.DevTools.ViewModels.SqlSyntaxColorizers.Rules;

namespace SolvesIT.GuiTool
{
    internal class AppBootstrapper: Bootstrapper<MainViewModel>, IDisposable
    {
        private UnityContainer _container;

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Dispose of Lifetimemanager is handled by _container")]
        protected override void Configure()
        {
            _container = new UnityContainer();
            var locator = _container.Resolve<UnityServiceLocator>();
            ServiceLocator.SetLocatorProvider(() => locator);

            _container.RegisterType<IWindowManager, AppWindowManager>();
            _container.RegisterType<IEventAggregator, EventAggregator>(new ContainerControlledLifetimeManager());

            RegisterSqlSyntaxColorizer();

            var bootstrapper = new Bootstrapper();
            var registrar = new UnityRegistrar(_container);
            registrar.EnableEnumerableResolution();

            bootstrapper.Bootstrap(registrar);
        }



        private void RegisterSqlSyntaxColorizer()
        {
            _container.RegisterType<IColorizerRule, KeywordsColorizer>("KeywordsColorizer");
            _container.RegisterType<IColorizerRule, ErrorsColorizer>("ErrorsColorizer");
            _container.RegisterType<IColorizerRule, StringLiteralsColorizer>("StringLiteralsColorizer");
            _container.RegisterType<IColorizerRule, AsciiStringLiteralsColorizer>("AsciiStringLiteralsColorizer");
            _container.RegisterType<IColorizerRule, CommentColorizer>("CommentColorizer");
            _container.RegisterType<IColorizerRule, AliasColorizer>("AliasColorizer");
            _container.RegisterType<IColorizerRule, FunctionCallColorizer>("FunctionCallColorizer");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.ResolveAll(service);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.Resolve(service, key);
        }

        public void Dispose()
        {
            _container.Dispose();
        }
    }
}
﻿using System.Runtime.Serialization;

namespace SolvesIT.Common
{
    [DataContract]
    public enum ValidationMessageType
    {
        [EnumMember]
        NaN = 0,

        [EnumMember]
        Information,

        [EnumMember]
        Warning,

        [EnumMember]
        Error
    }
}

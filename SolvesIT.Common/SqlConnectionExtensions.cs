﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SolvesIT.Common
{
    public static class SqlConnectionExtensions
    {
        public static IReadOnlyCollection<string> SplitSqlScriptByGoStatement(string sqlScript)
        {
            const string regExOptionaComment = @"(/\*(?>(?:(?>[^*]+)|\*(?!/))*)\*/)?";
            const string regExFinGoTemplate = @"^\s*{0}\s*GO\s*{0}\s*(\(\d+\))?\s*{0}\s*(\-\-.*?)?$";
            const RegexOptions regexOptions = RegexOptions.Multiline | RegexOptions.IgnoreCase;

            var regExFindGo = string.Format(regExFinGoTemplate, regExOptionaComment);

            var commands = Regex.Split(sqlScript, regExFindGo, regexOptions)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToArray();
            return commands;
        }

        public static async Task ExecuteScriptAsync(
            this SqlConnection sqlConnection,
            string script,
            SqlTransaction transaction = null)
        {
            var queries = SplitSqlScriptByGoStatement(script).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            foreach (var query in queries)
            {
                using (var command = new SqlCommand(query, sqlConnection, transaction))
                {
                    await command.ExecuteNonQueryAsync();
                }
            }
        }

        public static async Task ExecuteScriptAsync(
            this SqlConnection sqlConnection,
            Stream scriptFile,
            SqlTransaction transaction = null)
        {
            var script = new StreamReader(scriptFile).ReadToEnd();
            await sqlConnection.ExecuteScriptAsync(script, transaction);
        }
    }
}
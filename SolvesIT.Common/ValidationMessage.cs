﻿using System.Runtime.Serialization;

namespace SolvesIT.Common
{
    [DataContract]
    public sealed class ValidationMessage
    {
        [DataMember]
        public string ObjectName { get; set; }

        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public int Length { get; set; }

        [DataMember]
        public int Line { get; set; }

        [DataMember]
        public int Column { get; set; }

        [DataMember]
        public string Message { get; set; }

        public ValidationMessageType MessageType { get; set; }
    }
}

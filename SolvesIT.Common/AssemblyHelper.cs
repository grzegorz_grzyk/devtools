﻿using System.Reflection;

namespace SolvesIT.Common
{
    internal sealed class AssemblyHelper : IAssemblyHelper
    {
        /// <summary>
        /// Gets the full path or UNC location of the loaded file that contains the manifest.
        /// </summary>
        /// <returns>The location of the loaded file that contains the manifest. If the loaded file was shadow-copied, the location is that of the file after being shadow-copied. If the assembly is loaded from a byte array, such as when using the Load(Byte[]) method overload, the value returned is an empty string ("").</returns>
        public string GetLocationOfExecutingAssembly
        {
            get { return Assembly.GetExecutingAssembly().Location; }
        }

        public string GetNameOfEntryAssembly
        {
            get
            {
                var entryAssembly = Assembly.GetEntryAssembly();
                return entryAssembly == null ? null : entryAssembly.GetName().Name;
            }
        }

        public string GetNameOfExecutingAssembly
        {
            get { return Assembly.GetExecutingAssembly().GetName().Name; }
        }
    }
}

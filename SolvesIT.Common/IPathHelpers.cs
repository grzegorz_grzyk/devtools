﻿namespace SolvesIT.Common
{
    public interface IPathHelpers
    {
        string ApplicationDirectoryPath { get; }

        string ApplicationName { get; }

        string TempApplicationPath { get; }

        string LocalApplicationPath { get; }

        string DecodePath(string path);

        string GetAbsolutePath(
            string filePath,
            string defaultRootPath);
    }
}
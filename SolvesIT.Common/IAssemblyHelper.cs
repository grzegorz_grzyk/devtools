﻿namespace SolvesIT.Common
{
    public interface IAssemblyHelper
    {
        string GetLocationOfExecutingAssembly { get; }

        string GetNameOfEntryAssembly { get; }

        string GetNameOfExecutingAssembly { get; }
    }
}

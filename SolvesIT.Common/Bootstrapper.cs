﻿using Qdarc.Utilities.Ioc;

namespace SolvesIT.Common
{
    public sealed class Bootstrapper : IBootstrapper
    {
        public double Order
        {
            get { return 0; }
        }

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterSingle<IAssemblyHelper, AssemblyHelper>();
            registrar.RegisterSingle<IPathHelpers, PathHelpers>();
        }
    }
}

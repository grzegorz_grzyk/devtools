﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SolvesIT.Common
{
    public sealed class PathHelpers
        : IPathHelpers
    {
        private readonly IAssemblyHelper _assemblyHelper;

        public PathHelpers(IAssemblyHelper assemblyHelper)
        {
            _assemblyHelper = assemblyHelper;
        }

        public string ApplicationDirectoryPath
        {
            get
            {
                var appDirectoryPath = Path.GetDirectoryName(_assemblyHelper.GetLocationOfExecutingAssembly);
                if (appDirectoryPath == null)
                    throw new InvalidOperationException();
                return appDirectoryPath;
            }
        }

        public string ApplicationName
        {
            get { return _assemblyHelper.GetNameOfEntryAssembly ?? _assemblyHelper.GetNameOfExecutingAssembly; }
        }

        public string TempApplicationPath
        {
            get
            {
                var applicationName = _assemblyHelper.GetNameOfEntryAssembly ??
                                      _assemblyHelper.GetNameOfExecutingAssembly;
                var tempDirPath = Path.Combine(Path.GetTempPath(), applicationName);
                if (!Directory.Exists(tempDirPath))
                    Directory.CreateDirectory(tempDirPath);
                return tempDirPath;
            }
        }

        public string LocalApplicationPath
        {
            get
            {
                var localAppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);

                var applicationName = _assemblyHelper.GetNameOfEntryAssembly ??
                                      _assemblyHelper.GetNameOfExecutingAssembly;
                var appDataPath = Path.Combine(localAppDataPath, applicationName);

                if (!Directory.Exists(appDataPath))
                    Directory.CreateDirectory(appDataPath);

                return appDataPath;
            }
        }

        /// <summary>
        /// Replace enviroment variables (ex. %TEMP%, %USERPROFILE%) with theirs values. 
        /// </summary>
        /// <param name="path">The path to decode.</param>
        /// <returns>Decoded path.</returns>
        public string DecodePath(string path)
        {
            var result = path;
            var variables = Regex.Matches(path, @"%\w+%").Cast<Match>().Select(x => x.Value).ToArray();

            if (!variables.Any())
            {
                return result;
            }

            foreach (var variable in variables)
            {
                result = result.Replace(variable, Environment.GetEnvironmentVariable(variable.Replace("%", string.Empty)));
            }

            return result;
        }

        /// <summary>
        /// Method decodes file path. If decoded path is relative,
        /// the result will be a combination of defaultRootPath and decoded path.
        /// </summary>
        /// <param name="filePath">Path to file.</param>
        /// <param name="defaultRootPath">Default root path.</param>
        /// <returns>Absolute path to file.</returns>
        public string GetAbsolutePath(
            string filePath,
            string defaultRootPath)
        {
            var decodedPath = DecodePath(filePath);
            var result = decodedPath;
            if (string.IsNullOrEmpty(Path.GetPathRoot(decodedPath)))
            {
                result = Path.Combine(defaultRootPath, decodedPath);
            }

            return result;
        }
    }
}
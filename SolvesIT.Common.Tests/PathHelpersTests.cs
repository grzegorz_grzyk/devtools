﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;

namespace SolvesIT.Common.Tests
{
    [TestClass]
    public class PathHelpersTests
    {
        [TestMethod]
        public void GetAppDirectoryPathShouldThrowExceptionWhenLocationIsEmpty()
        {
            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetLocationOfExecutingAssembly).Returns(String.Empty);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);
            QAssert.ShouldThrow<ArgumentException>(() => { var x = pathHelpers.ApplicationDirectoryPath; });
        }

        [TestMethod]
        public void GetAppDirectoryPathShouldThrowExceptionWhenLocationIndicatesRootDirectory()
        {
            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetLocationOfExecutingAssembly).Returns(@"C:\");
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);
            QAssert.ShouldThrow<InvalidOperationException>(() => { var x = pathHelpers.ApplicationDirectoryPath; });
        }

        [TestMethod]
        public void GetAppDirectoryPathShouldReturnValidPath()
        {
            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetLocationOfExecutingAssembly).Returns(@"C:\x");
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);
            var actualValue = pathHelpers.ApplicationDirectoryPath;
            actualValue.ShouldBeEqual(@"C:\");
        }

        [TestMethod]
        public void GetApplicationNameShouldReturnExecutingAssemblyNameWhenEntryNameIsNUll()
        {
            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns<string>(null);
            mockAssemblyHelper.Setup(x => x.GetNameOfExecutingAssembly).Returns(@"TestExecutingName");
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);
            var actualValue = pathHelpers.ApplicationName;
            actualValue.ShouldBeEqual(@"TestExecutingName");
        }

        [TestMethod]
        public void GetApplicationNameShouldReturnEntryAssemblyNameWhenEntryNameIsNotNUll()
        {
            const string applicationName = @"TestApplicationName";
            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns(applicationName);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);
            var actualValue = pathHelpers.ApplicationName;
            actualValue.ShouldBeEqual(applicationName);
        }

        [TestMethod]
        public void GetTempApplicationPathShouldReturnPathToExistingDirectory()
        {
            const string applicationName = @"TestApplicationName";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns(applicationName);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var tempDirPath = Path.Combine(
                Path.GetTempPath(),
                applicationName);
            if (!Directory.Exists(tempDirPath))
                Directory.CreateDirectory(tempDirPath);

            var tempApplicationPath = pathHelpers.TempApplicationPath;
            Directory.Exists(tempApplicationPath).ShouldBeTrue();

            Directory.Delete(tempApplicationPath, true);
        }

        [TestMethod]
        public void GetTempApplicationPathShouldCreateDirAndReturnPathToExistingDirectory()
        {
            const string applicationName = @"TestApplicationName";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns(applicationName);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var tempDirPath = Path.Combine(
                Path.GetTempPath(),
                applicationName);
            if (Directory.Exists(tempDirPath))
                Directory.Delete(tempDirPath, true);

            var tempApplicationPath = pathHelpers.TempApplicationPath;
            Directory.Exists(tempApplicationPath).ShouldBeTrue();

            Directory.Delete(tempApplicationPath, true);
        }

        [TestMethod]
        public void GetLocalApplicationPathShouldReturnPathToExistingDirectory()
        {
            const string applicationName = @"TestApplicationName";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns(applicationName);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var tempDirPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                applicationName);
            if (!Directory.Exists(tempDirPath))
                Directory.CreateDirectory(tempDirPath);

            var tempApplicationPath = pathHelpers.LocalApplicationPath;
            Directory.Exists(tempApplicationPath).ShouldBeTrue();

            Directory.Delete(tempApplicationPath, true);
        }

        [TestMethod]
        public void GetLocalApplicationPathShouldCreateDirAndReturnPathToExistingDirectory()
        {
            const string applicationName = @"TestApplicationName";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            mockAssemblyHelper.Setup(x => x.GetNameOfEntryAssembly).Returns(applicationName);
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var tempDirPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                applicationName);
            if (Directory.Exists(tempDirPath))
                Directory.Delete(tempDirPath, true);

            var tempApplicationPath = pathHelpers.LocalApplicationPath;
            Directory.Exists(tempApplicationPath).ShouldBeTrue();

            Directory.Delete(tempApplicationPath, true);
        }

        [TestMethod]
        public void DecodePathShouldReturnSamePathAsInputIfNoEnviromentVariableWasUsed()
        {
            const string testPath = @"test\path";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var decodedPath = pathHelpers.DecodePath(testPath);

            decodedPath.ShouldBeEqual(testPath);
        }

        [TestMethod]
        public void DecodePathShouldReturnPathIfSingleEnviromentVariableWasUsed()
        {
            const string testPath = @"%TMP%\test\path%";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var decodedPath = pathHelpers.DecodePath(testPath);

            var enviromentTmp = Environment.GetEnvironmentVariable("TMP");
            var expectedPath = testPath.Replace("%TMP%", enviromentTmp);

            decodedPath.ShouldBeEqual(expectedPath);
        }

        [TestMethod]
        public void DecodePathShouldReturnPathIfMultipleEnviromentVariableWasUsed()
        {
            const string testPath = @"%TMP%\test\path%TMP%";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var decodedPath = pathHelpers.DecodePath(testPath);

            var enviromentTmp = Environment.GetEnvironmentVariable("TMP");
            var expectedPath = testPath.Replace("%TMP%", enviromentTmp);

            decodedPath.ShouldBeEqual(expectedPath);
        }

        [TestMethod]
        public void DecodePathShouldReplaceNoExistanceEnviromentVariableByEmptyString()
        {
            const string testPath = @"%NOEXIST%\test\path";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var decodedPath = pathHelpers.DecodePath(testPath);

            var expectedPath = testPath.Replace("%NOEXIST%", string.Empty);

            decodedPath.ShouldBeEqual(expectedPath);
        }

        [TestMethod]
        public void GetAbsolutePathShouldReturnFilePathWhenFilePathContainsRootDirectory()
        {
            const string filePath = @"c:\file\path";
            const string defaultRootPath = @"\root\path\";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var absolutePath = pathHelpers.GetAbsolutePath(filePath, defaultRootPath);

            absolutePath.ShouldBeEqual(filePath);
        }

        [TestMethod]
        public void GetAbsolutePathShouldReturnDecodedFilePathWhenFilePathContainsRootDirectory()
        {
            const string filePath = @"%TMP%\file\path";
            const string defaultRootPath = @"\root\path\";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var absolutePath = pathHelpers.GetAbsolutePath(filePath, defaultRootPath);

            var expectedPath = pathHelpers.DecodePath(filePath);

            absolutePath.ShouldBeEqual(expectedPath);
        }

        [TestMethod]
        public void GetAbsolutePathShouldReturnCombinationOfDefaultRootPathAndFilePathWhenFilePathIsRelative()
        {
            const string filePath = @"file\path";
            const string defaultRootPath = @"\root\path\";

            var mockAssemblyHelper = new Mock<IAssemblyHelper>();
            var fakeAssemblyHelpers = mockAssemblyHelper.Object;
            var pathHelpers = new PathHelpers(fakeAssemblyHelpers);

            var absolutePath = pathHelpers.GetAbsolutePath(filePath, defaultRootPath);

            var expectedPath = Path.Combine(defaultRootPath, filePath);

            absolutePath.ShouldBeEqual(expectedPath);
        }
    }
}

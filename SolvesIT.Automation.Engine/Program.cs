﻿using System.Linq;
using CommonServiceLocator;
using Qdarc.Utilities.Ioc;
using Qdarc.Utilities.Ioc.Unity;
using SolvesIT.Automation.Templating;
using SolvesIT.Automation.Templating.Abstraction;
using Unity;
using Unity.Lifetime;
using Unity.ServiceLocation;

namespace SolvesIT.Automation.Engine
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            using (var container = new UnityContainer())
            {
                var registrar = new UnityRegistrar(container, () => new PerResolveLifetimeManager());

                var locator = new UnityServiceLocator(container);
                ServiceLocator.SetLocatorProvider(() => locator);

                var moduleDlls = new []
                {
                    typeof(Bootstrapper).Assembly,
                    typeof(SolvesIT.Common.Bootstrapper).Assembly,
                };
                var bootstrapper = new SystemBootstrapper(registrar, moduleDlls);
                registrar.EnableEnumerableResolution();
                bootstrapper.Bootstrap();

                var templateFile = args[0];
                var service = container.Resolve<ITemplateService>();
                var result = service.CreateCsGeneratorFiles(templateFile);

                return result.ValidationMessages.Count();
            }
        }
    }
}

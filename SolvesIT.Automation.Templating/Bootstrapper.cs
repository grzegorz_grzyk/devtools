﻿using Irony.Parsing;
using Qdarc.Utilities.Ioc;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Automation.Templating.Logic;

namespace SolvesIT.Automation.Templating
{
    public class Bootstrapper : IBootstrapper
    {
        public double Order
        {
            get { return 0; }
        }

        public void Bootstrap(IRegistrar registrar)
        {
            registrar.RegisterSingle<ICompilationExecutor, CompilationExecutor>();
            registrar.RegisterSingle<IGeneratorLoader, GeneratorLoader>();
            registrar.RegisterSingle<IBodyParser, BodyParser>();
            registrar.RegisterSingle<ITemplateService, TemplateService>();
            registrar.RegisterSingle<IResolversGenerator, ResolversGenerator>();
            registrar.RegisterSingle<Grammar, TemplateGrammar>();
        }

    }
}
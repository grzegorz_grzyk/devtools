﻿using System.Collections.Generic;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface ITemplateService
    {
        ValidatedResult<IEnumerable<string>> CreateCsGeneratorFiles(string filePath);

        ValidatedResult<string> CreateGeneratorAssembly(string filePath);
    }
}

﻿using System.Collections.Generic;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface ITemplateDataResolver<TParameter, TItem> : ITemplateDataResolver
    {
        IEnumerable<TItem> GetCollection(TParameter parameter);

        object GetParameterValue(string parameterName, TItem item);

        string GetTargetFilePathValue(TItem item);
    }
}

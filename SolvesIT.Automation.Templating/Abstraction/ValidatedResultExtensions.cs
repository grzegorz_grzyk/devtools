﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public static class ValidatedResultExtensions
    {

        public static ValidatedResult<IEnumerable<TResult>> Merge<TResult>(this IEnumerable<ValidatedResult<TResult>> results)
            where TResult : class
        {
            return new ValidatedResult<IEnumerable<TResult>>(
                results.Select(x => x.Result).Where(x => x != null).ToArray(),
                results.Where(x => x.ValidationMessages != null).SelectMany(x => x.ValidationMessages));
        }
    }
}
﻿using System.Collections.Generic;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface ICompilationExecutor
    {
        ValidatedResult<string> Compile(IEnumerable<string> files, IEnumerable<string> assemblies);
    }
}
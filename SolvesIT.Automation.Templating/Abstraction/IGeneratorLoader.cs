﻿using SolvesIT.Automation.Templating.Model;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface IGeneratorLoader
    {
        ValidatedResult<Generator> Load(string filePath);
    }
}
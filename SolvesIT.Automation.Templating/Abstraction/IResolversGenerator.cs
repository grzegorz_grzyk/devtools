﻿using System.Collections.Generic;
using SolvesIT.Automation.Templating.Model;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface IResolversGenerator
    {
        ValidatedResult<string> CreateTemplateResolverClass(Template descriptor, string generatorName);

        string CreateTemplateResolversBootstrapper(Generator generator);

        string CreateGeneratorRunner(Generator generator);
    }
}

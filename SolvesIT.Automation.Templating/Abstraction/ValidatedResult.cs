﻿using System;
using System.Collections.Generic;
using System.Linq;
using SolvesIT.Common;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public class ValidatedResult<TResult>
        where TResult : class
    {
        public ValidatedResult(IEnumerable<ValidationMessage> validationMessages)
        {
            ValidationMessages = validationMessages?.ToArray() ?? throw new ArgumentNullException(nameof(validationMessages));

            if (!ValidationMessages.Any())
            {
                throw new ArgumentException(
                    "At least one validation message is required.",
                    nameof(validationMessages));
            }
        }

        public ValidatedResult(TResult result)
        {
            Result = result ?? throw new ArgumentNullException(nameof(result));
        }

        public ValidatedResult(TResult result, IEnumerable<ValidationMessage> validationMessages)
        {
            Result = result ?? throw new ArgumentNullException(nameof(result));
            ValidationMessages = validationMessages?.ToArray() ?? throw new ArgumentNullException(nameof(validationMessages));
        }

        public TResult Result { get; }

        public IEnumerable<ValidationMessage> ValidationMessages { get; }

        public bool IsValid => ValidationMessages == null || !ValidationMessages.Any();
    }
}
using System.Collections.Generic;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface ITemplateDataResolver
    {
        string Name { get; }

        IEnumerable<dynamic> GetCollection(dynamic parameter);

        object GetParameterValue(string parameterName, dynamic item);

        string GetTargetFilePathValue(dynamic item);

        string Run(dynamic parameter, ITemplateRunner templateRunner);
    }
}
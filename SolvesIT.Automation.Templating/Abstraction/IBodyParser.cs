﻿using Irony.Parsing;
using SolvesIT.Automation.Templating.Model;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface IBodyParser
    {
        ValidatedResult<ParseTree> Parse(Template templateObject);
    }
}
namespace SolvesIT.Automation.Templating.Abstraction
{
    public interface ITemplateRunner
    {
        string Run(string name, dynamic parameter);
    }
}
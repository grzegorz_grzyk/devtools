﻿using System.Collections.Generic;
using System.Linq;

namespace SolvesIT.Automation.Templating.Abstraction
{
    public abstract class TemplateDataResolver<TParameter, TItem> : ITemplateDataResolver<TParameter, TItem>
    {
        public abstract string Name { get; }

        public IEnumerable<dynamic> GetCollection(dynamic parameter)
        {
            return GetCollection((TParameter)parameter).Select(x => (dynamic)x).ToArray();
        }

        public object GetParameterValue(string parameterName, dynamic item)
        {
            return GetParameterValue(parameterName, (TItem)item);
        }

        public string GetTargetFilePathValue(dynamic item)
        {
            return GetTargetFilePathValue((TItem)item);
        }

        public string Run(dynamic parameter, ITemplateRunner templateRunner)
        {
            return Run((TParameter)parameter, templateRunner);
        }

        public abstract IEnumerable<TItem> GetCollection(TParameter parameter);

        public abstract object GetParameterValue(string parameterName, TItem item);

        public abstract string GetTargetFilePathValue(TItem item);

        public abstract string Run(TParameter parameter, ITemplateRunner templateRunner);
    }
}
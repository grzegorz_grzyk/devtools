﻿using System.Collections.Generic;
using System.Linq;
using Irony;
using Irony.Parsing;

namespace SolvesIT.Automation.Templating.Logic
{
    public class TemplateTextTerminal : Terminal
    {
        private char[] _stopChars;

        public TemplateTextTerminal(string name)
            : base(name)
        {
            Priority = TerminalPriority.Low;
        }

        public override void Init(GrammarData grammarData)
        {
            base.Init(grammarData);
            var stopCharSet = new CharHashSet();
            foreach (var term in grammarData.Terminals)
            {
                var firsts = term.GetFirsts();
                if (firsts == null) continue;
                foreach (var first in firsts)
                    if (!string.IsNullOrEmpty(first))
                        stopCharSet.Add(first[0]);
            }
            _stopChars = stopCharSet.ToArray();
        }

        public override IList<string> GetFirsts()
        {
            return null;
        }

        public override Token TryMatch(ParsingContext context, ISourceStream source)
        {
            var stopIndex = source.Text.IndexOfAny(_stopChars, source.Location.Position + 1);
            if (stopIndex == source.Location.Position) return null;
            if (stopIndex < 0) stopIndex = source.Text.Length;
            source.PreviewPosition = stopIndex;
            return source.CreateToken(this.OutputTerminal);
        }
    }
}

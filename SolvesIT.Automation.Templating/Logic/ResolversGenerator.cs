﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Irony.Parsing;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Automation.Templating.Model;

namespace SolvesIT.Automation.Templating.Logic
{
    public class ResolversGenerator
        : IResolversGenerator
    {
        private readonly IBodyParser _parser;

        public ResolversGenerator(IBodyParser parser)
        {
            _parser = parser;
        }

        public ValidatedResult<string> CreateTemplateResolverClass(Template descriptor, string generatorName)
        {
            var getParameterValue = GetParameterValueMethodSwitchBody(descriptor);
            var getTargetFilePathValue = !string.IsNullOrWhiteSpace(descriptor.TargetFilePath)
                ? descriptor.TargetFilePath
                : "null";
            var parsedBody = _parser.Parse(descriptor);
            var runMethod = EmitRunMethod(parsedBody.Result, descriptor);
            var usings = FormatUsings(descriptor.Imports);
            string baseTemplate;

            var isDynamicTemplate = descriptor.ParameterType == null;

            if (isDynamicTemplate)
            {
                baseTemplate = "ITemplateDataResolver";
            }
            else
            {
                baseTemplate =
                    $"TemplateDataResolver<{descriptor.ParameterType}, {descriptor.ItemType}>";
            }

            var source =
                $@"namespace SolvesIT.Automation.Templating.Generators.{generatorName} {{
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using SolvesIT.Automation.Templating.Abstraction;
{usings}

    class {descriptor.Name}TemplateResolver 
: {baseTemplate} {{

        public {(isDynamicTemplate ? "" : "override")} string Name {{ get {{ return ""{descriptor.Name}""; }} }}

        public {(isDynamicTemplate ? "" : "override")} IEnumerable<{
                        (isDynamicTemplate ? "dynamic" : descriptor.ItemType)
                    }> GetCollection({(isDynamicTemplate ? "dynamic" : descriptor.ParameterType)} item)
        {{
            var elements = {descriptor.Collection};
            return elements;
        }}

        public {(isDynamicTemplate ? "" : "override")} object GetParameterValue(string parameterName, {
                        (isDynamicTemplate ? "dynamic" : descriptor.ItemType)
                    } item)
        {{
            switch(parameterName)
            {{
                {getParameterValue}
                default:
                    throw new InvalidOperationException($""Parameter named {{parameterName}} is not defined in template {{Name}}."");
            }}
        }}

        public {(isDynamicTemplate ? "" : "override")} string GetTargetFilePathValue({
                        (isDynamicTemplate ? "dynamic" : descriptor.ItemType)
                    } item)
        {{
            return {getTargetFilePathValue};
        }}

        {runMethod}
    }}
}}";
            return new ValidatedResult<string>(source);
        }

        public string EmitRunMethod(ParseTree tree, Template template)
        {
            var method = new StringBuilder($"public {(template.ParameterType != null ? "override" : string.Empty)} string Run({template.ParameterType ?? "dynamic"} parameter, ITemplateRunner templateRunner) {{");
            method.AppendLine("var collection = GetCollection(parameter);");
            method.AppendLine("var builder = new StringBuilder();");
            method.AppendLine("foreach(var item in collection) {");
            method.AppendLine("var innerBuilder = new StringBuilder();");

            EmitRunMethod(method, tree.Root);

            method.AppendLine($"var targetFilePath = GetTargetFilePathValue(item);");
            method.AppendLine("if(!string.IsNullOrEmpty(targetFilePath)) {");
            method.AppendLine("File.WriteAllText(targetFilePath, innerBuilder.ToString());");

            // todo : add to project support
            //    var projectPath = templateResolver.GetAddToProjectValue(ddlObject);
            //    _csProjectService.AddFileToProject(projectPath, filePath);

            method.AppendLine("} else {");
            method.AppendLine("builder.AppendLine(innerBuilder.ToString());");

            method.AppendLine("}");
            method.AppendLine("}");
            method.AppendLine("return builder.ToString();");
            method.AppendLine("}");

            return method.ToString();
        }

        private void EmitRunMethod(StringBuilder method, ParseTreeNode startNode)
        {
            var stack = new Stack<ParseTreeNode>();
            stack.Push(startNode);
            while (stack.Any())
            {
                var node = stack.Pop();
                switch (node.Term.Name)
                {
                    case "templateBody":
                    case "body":
                        if (node.ChildNodes.Count > 1)
                        {
                            stack.Push(node.ChildNodes[1]);
                        }
                        stack.Push(node.ChildNodes[0]);
                        break;
                    case "parameter":
                        method.AppendLine(
                            $"innerBuilder.Append(GetParameterValue(\"{node.ChildNodes[1].Token.Text}\", item));");
                        break;
                    case "method":
                        var childTemplateName = node.ChildNodes[1].Token.Text;
                        method.AppendLine($@"innerBuilder.Append(templateRunner.Run(""{childTemplateName}"", item));");
                        break;
                    case "text":
                        method.AppendLine($"innerBuilder.Append(@\"{ node.Token.Text.Replace("\"", "\"\"") }\");");
                        break;
                    case "if":
                        var boolVariableName = node.ChildNodes[0].ChildNodes[1].Token.Text;
                        method.AppendLine($"if((bool)GetParameterValue(\"{boolVariableName}\", item)){{");
                        EmitRunMethod(method, node.ChildNodes[1]);
                        method.AppendLine("}");
                        if (node.ChildNodes.Count > 3)
                        {
                            method.AppendLine("else {");
                            EmitRunMethod(method, node.ChildNodes[3]);
                            method.AppendLine("}");
                        }
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public string CreateTemplateResolversBootstrapper(Generator generator)
        {
            string registrations = string.Join(
                Environment.NewLine,
                generator.Templates.Select(x => "registrar.RegisterMultiple" +
                                            $"<ITemplateDataResolver, {x.Name}TemplateResolver>(\"{x.Name}\");"));
            var file = $@"namespace SolvesIT.Automation.Templating.Generators.{generator.Name}
{{
    using SolvesIT.Automation.Templating.Abstraction;

    public class Bootstrapper : Qdarc.Utilities.Ioc.IBootstrapper
    {{
        public void Bootstrap(Qdarc.Utilities.Ioc.IRegistrar registrar)
        {{
            {registrations}
            registrar.RegisterSingle<IGeneratorRunner, GeneratorRunner>();
        }}

        public double Order {{ get; }} = 0;
    }}
}}
";

            return file;
        }

        public string CreateGeneratorRunner(Generator generator)
        {
            var generatorParameterType = generator.Templates.Single(x => x.Name == generator.StartingTemplateName).ParameterType ?? "dynamic";
            var usings = FormatUsings(generator.Imports);

            var generatorRunner = $@"namespace SolvesIT.Automation.Templating.Generators.{generator.Name}
{{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SolvesIT.Automation.Templating.Abstraction;
    {usings}

    public interface IGeneratorRunner 
    {{
        string Run({generatorParameterType} parameter);
    }}

    public class GeneratorRunner: ITemplateRunner, IGeneratorRunner
    {{
        private readonly Dictionary<string, ITemplateDataResolver> _resolvers;
        public GeneratorRunner(IEnumerable<ITemplateDataResolver> resolvers)
        {{
            _resolvers = resolvers.ToDictionary(x => x.Name, x => x);
        }}

        public string Run({generatorParameterType} parameter)
        {{
            return Run(""{generator.StartingTemplateName}"" , parameter);
        }}

        public string Run(string name, dynamic parameter)
        {{
            if(!_resolvers.ContainsKey(name))
            {{
                throw new InvalidOperationException($""Template named {{name}} is not defined""); 
            }}
            
            return _resolvers[name].Run(parameter, this);
        }}

    }}
}}
";
            return generatorRunner;
        }

        private static string FormatUsings(IEnumerable<ImportData> data)
        {
            if (data != null)
                return string.Join(Environment.NewLine, data.Select(x => $"using {x.Import};"));

            return string.Empty;
        }

        private static string GetParameterValueMethodSwitchBody(Template template)
        {
            if (template.Parameters == null || !template.Parameters.Any())
                return null;
            return template.Parameters.Aggregate("",
                (current, parameter) => current + string.Format(CultureInfo.InvariantCulture, @"
                case ""{0}"": return {1};", parameter.Name, parameter.Value));
        }
    }
}

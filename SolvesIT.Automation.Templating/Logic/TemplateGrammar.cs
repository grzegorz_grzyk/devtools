﻿using Irony.Parsing;

namespace SolvesIT.Automation.Templating.Logic
{
    class TemplateGrammar
        : Grammar
    {
        public override void SkipWhitespace(ISourceStream source)
        {
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)")]
        public TemplateGrammar()
            : base(false)
        {
            var text = new TemplateTextTerminal("text");
            var identifier = new IdentifierTerminal("identifier");

            var parameter = new NonTerminal("parameter")
            {
                Rule = ToTerm("##") + identifier + ToTerm("##")
            };
            var method = new NonTerminal("method")
            {
                Rule = ToTerm("##") + identifier + ToTerm("()##")
            };

            var ifBegin = new NonTerminal("ifBegin")
            {
                Rule = ToTerm("##IF ") + identifier + ToTerm("##")
            };

            var body = new NonTerminal("body");
            var templateBody = new NonTerminal("templateBody")
            {
                Rule = Empty | body
            };

            var ifTerminal = new NonTerminal("if")
            {
                Rule = ifBegin + templateBody + ToTerm("##END##")
                     | ifBegin + templateBody + ToTerm("##ELSE##") + templateBody + ToTerm("##END##")
            };

            body.Rule
                = ifTerminal
                | parameter
                | method
                | text
                | body + ifTerminal
                | body + parameter
                | body + method
                | body + text;

            this.Root = templateBody;

            this.MarkReservedWords("else", "end");
        }
    }
}

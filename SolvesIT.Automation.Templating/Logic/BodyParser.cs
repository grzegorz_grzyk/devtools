﻿using System;
using System.Collections.Generic;
using Irony;
using Irony.Parsing;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Automation.Templating.Model;
using SolvesIT.Common;

namespace SolvesIT.Automation.Templating.Logic
{
    class BodyParser : IBodyParser
    {
        private readonly Grammar _templateGrammar;

        public BodyParser(Grammar templateGrammar)
        {
            _templateGrammar = templateGrammar;
        }

        public ValidatedResult<ParseTree> Parse(Template templateObject)
        {
            var validationMessages = new List<ValidationMessage>();
            var language = new LanguageData(_templateGrammar);
            var parser = new Parser(language);
            if (!parser.Language.CanParse()) return null;

            ParseTree parseTree = null;
            try
            {
                parseTree = parser.Parse(templateObject.Body, templateObject.Name);
                foreach (var treeParserMessage in parseTree.ParserMessages)
                {
                    validationMessages.Add(new ValidationMessage
                    {
                        ObjectName = templateObject.Name,
                        Message = treeParserMessage.Message,
                        Column = treeParserMessage.Location.Column,
                        Line = treeParserMessage.Location.Line,
                        MessageType = GetMessageType(treeParserMessage.Level)
                    });
                }
            }
            catch (Exception ex)
            {
                validationMessages.Add(new ValidationMessage
                {
                    ObjectName = templateObject.Name,
                    Message = ex.Message
                });
            }

            return new ValidatedResult<ParseTree>(parseTree, validationMessages);
        }

        private static ValidationMessageType GetMessageType(ErrorLevel level)
        {
            switch (level)
            {
                case ErrorLevel.Error:
                    return ValidationMessageType.Error;
                case ErrorLevel.Info:
                    return ValidationMessageType.Information;
                case ErrorLevel.Warning:
                    return ValidationMessageType.Warning;
            }

            throw new InvalidOperationException();
        }
    }
}
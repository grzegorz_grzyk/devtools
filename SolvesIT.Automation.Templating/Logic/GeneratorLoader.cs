﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Schema;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Automation.Templating.Model;
using SolvesIT.Automation.Templating.Properties;
using SolvesIT.Common;

namespace SolvesIT.Automation.Templating.Logic
{
    public class GeneratorLoader : IGeneratorLoader
    {
        private readonly IPathHelpers _pathHelpers;

        public GeneratorLoader(IPathHelpers pathHelpers)
        {
            _pathHelpers = pathHelpers;
        }

        public ValidatedResult<Generator> Load(string filePath)
        {
            if (!File.Exists(filePath))
            {
                var error = new ValidationMessage
                {
                    MessageType = ValidationMessageType.Error,
                    Message =
                         $"Generator file does not exist. {filePath}."
                };

                return new ValidatedResult<Generator>(new[] { error });
            }

            var validationMessages = new List<ValidationMessage>();
            if (!IsGeneratorFileValid(filePath, validationMessages))
            {
                return new ValidatedResult<Generator>(validationMessages);
            }

            var generator = DeserializeGenerator(filePath);
            ProcessAssemblies(filePath, generator, validationMessages);
            ProcessUsings(generator);
            LoadTemplateBodies(filePath, generator, validationMessages);

            return new ValidatedResult<Generator>(generator, validationMessages);
        }

        private static Generator DeserializeGenerator(string filePath)
        {
            Generator generator;
            using (var file = File.OpenRead(filePath))
            {
                var dataSerializer = new DataContractSerializer(typeof(Generator));
                generator = (Generator)dataSerializer.ReadObject(file);
            }

            return generator;
        }

        private void LoadTemplateBodies(string filePath, Generator generator, List<ValidationMessage> validationMessages)
        {
            foreach (var template in generator.Templates.Where(x => !string.IsNullOrEmpty(x.BodyFilePath)))
            {
                var templateBodyPath = _pathHelpers.GetAbsolutePath(
                    template.BodyFilePath,
                    Path.GetDirectoryName(filePath));

                if (File.Exists(templateBodyPath))
                {
                    template.Body = File.ReadAllText(templateBodyPath);
                }
                else
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture,
                        "File '{0}' cannot be read.",
                        template.BodyFilePath);

                    validationMessages.Add(new ValidationMessage
                    {
                        ObjectName = filePath,
                        Message = message,
                        MessageType = ValidationMessageType.Error
                    });
                }
            }
        }

        private static void ProcessUsings(Generator generator)
        {
            if (generator.Imports == null)
            {
                return;
            }

            foreach (var template in generator.Templates)
            {
                if (template.Imports != null)
                {
                    template.Imports = generator.Imports.Concat(template.Imports).ToArray();
                }
                else
                {
                    template.Imports = generator.Imports.ToArray();
                }
            }
        }

        private void ProcessAssemblies(string filePath, Generator generator, List<ValidationMessage> validationMessages)
        {
            if (generator.Assemblies == null)
            {
                return;
            }

            foreach (var generatorAssembly in generator.Assemblies)
            {
                if (!Path.IsPathRooted(generatorAssembly.Path))
                {
                    generatorAssembly.Path =
                        _pathHelpers.GetAbsolutePath(generatorAssembly.Path, Path.GetDirectoryName(filePath));
                }

                if (!File.Exists(generatorAssembly.Path))
                {
                    validationMessages.Add(
                        new ValidationMessage
                        {
                            MessageType = ValidationMessageType.Error,
                            Message =
                                $"Included assembly file does not exist. {generatorAssembly.FullName}, {generatorAssembly.Path}."
                        });
                }
            }
        }

        private static bool IsGeneratorFileValid(string filePath, ICollection<ValidationMessage> validationMessages)
        {
            var result = true;

            var schemas = new XmlSchemaSet();
            using (var templateSchemaXsd = new StringReader(Resources.TemplateSchema))
            {
                var schema = XmlSchema.Read(templateSchemaXsd, null);
                schemas.Add(schema);
            }

            var generatorFile = XDocument.Load(filePath);
            generatorFile.Validate(schemas, (sender, e) =>
            {
                validationMessages.Add(new ValidationMessage
                {
                    ObjectName = filePath,
                    Column = e.Exception.LinePosition,
                    Line = e.Exception.LineNumber,
                    Message = e.Exception.Message,
                    MessageType = ValidationMessageType.Error
                });
                result = false;
            });

            return result;
        }
    }
}
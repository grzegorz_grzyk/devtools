using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Common;

namespace SolvesIT.Automation.Templating.Logic
{
    public class CompilationExecutor : ICompilationExecutor
    {
        private readonly IPathHelpers _pathHelper;

        public CompilationExecutor(IPathHelpers pathHelper)
        {
            _pathHelper = pathHelper;
        }

        public ValidatedResult<string> Compile(IEnumerable<string> files, IEnumerable<string> assemblies)
        {
            using (var csc = new Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider())
            //using (var csc = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } }))
            {
                var dataBaseModelAssembly = Assembly.GetExecutingAssembly().GetFiles()[0].Name;
                var directory = Path.GetDirectoryName(dataBaseModelAssembly);

                var assemblyNames = new[]
                    {
                        "mscorlib.dll",
                        "Microsoft.CSharp.dll",
                        "System.dll",
                        "System.Core.dll",
                        "System.Data.dll",
                        Path.Combine(directory, "SolvesIT.AutomationEngine.exe"),
                        Path.Combine(directory, "SolvesIT.Common.dll"),
                        Path.Combine(directory, "Qdarc.Utilities.dll"),
                        dataBaseModelAssembly
                    }
                    .Concat(assemblies)
                    .ToArray();
                var path = Path.Combine(_pathHelper.TempApplicationPath, Path.GetRandomFileName() + ".dll");
                var parameters = new CompilerParameters(assemblyNames, path);
                var results = csc.CompileAssemblyFromSource(parameters, files.ToArray());
                var validationMessages = new List<ValidationMessage>();
                foreach (CompilerError compilerError in results.Errors)
                {
                    validationMessages.Add(new ValidationMessage
                    {
                        Column = compilerError.Column,
                        Line = compilerError.Line,
                        Message = compilerError.ErrorText,
                        MessageType = compilerError.IsWarning
                            ? ValidationMessageType.Warning
                            : ValidationMessageType.Error,
                        ObjectName = compilerError.FileName
                    });
                }
                if (validationMessages.Any())
                    return new ValidatedResult<string>(validationMessages);

                return new ValidatedResult<string>(path);
            }
        }
    }
}
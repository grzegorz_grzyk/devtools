﻿using System.Collections.Generic;
using System.Linq;
using SolvesIT.Automation.Templating.Abstraction;
using SolvesIT.Automation.Templating.Model;

namespace SolvesIT.Automation.Templating.Logic
{
    public class TemplateService
        : ITemplateService
    {
        private readonly IGeneratorLoader _generatorLoader;
        private readonly IResolversGenerator _resolversGenerator;
        private readonly ICompilationExecutor _compilationExecutor;

        public TemplateService(
            IGeneratorLoader generatorLoader,
            IResolversGenerator resolversGenerator,
            ICompilationExecutor compilationExecutor)
        {
            _generatorLoader = generatorLoader;
            _resolversGenerator = resolversGenerator;
            _compilationExecutor = compilationExecutor;
        }

        public ValidatedResult<string> CreateGeneratorAssembly(string filePath)
        {
            var generator = _generatorLoader.Load(filePath);
            if (!generator.IsValid)
            {
                return new ValidatedResult<string>(generator.ValidationMessages);
            }

            var files = CreateCsGeneratorFiles(generator.Result);
            if (!files.IsValid)
            {
                return new ValidatedResult<string>(files.ValidationMessages);
            }

            var assembly = _compilationExecutor.Compile(files.Result, generator.Result.Assemblies.Select(x => x.Path));
            return assembly;
        }

        public ValidatedResult<IEnumerable<string>> CreateCsGeneratorFiles(string filePath)
        {
            var fileLoad = _generatorLoader.Load(filePath);
            if (!fileLoad.IsValid)
            {
                return new ValidatedResult<IEnumerable<string>>(fileLoad.ValidationMessages);
            }

            return CreateCsGeneratorFiles(fileLoad.Result);
        }

        private ValidatedResult<IEnumerable<string>> CreateCsGeneratorFiles(Generator generator)
        {
            var files = generator.Templates.Select(descriptor => _resolversGenerator.CreateTemplateResolverClass(descriptor, generator.Name)).Merge();
            var generatorRunner = _resolversGenerator.CreateGeneratorRunner(generator);
            var bootstrapper = _resolversGenerator.CreateTemplateResolversBootstrapper(generator);
            return new ValidatedResult<IEnumerable<string>>(
                files.Result.Concat(new[] { generatorRunner, bootstrapper }).ToArray(),
                files.ValidationMessages);
        }
    }
}

﻿using System.Runtime.Serialization;

namespace SolvesIT.Automation.Templating.Model
{
    [DataContract]
    public sealed class AssemblyData
    {
        [DataMember(Order = 0)]
        public string FullName { get; set; }

        [DataMember(Order = 1)]
        public string Path { get; set; }
    }
}
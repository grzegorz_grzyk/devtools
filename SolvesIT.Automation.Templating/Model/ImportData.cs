﻿using System.Runtime.Serialization;

namespace SolvesIT.Automation.Templating.Model
{
    [DataContract]
    public sealed class ImportData
    {
        [DataMember(Order = 0)]
        public string Import { get; set; }
    }
}
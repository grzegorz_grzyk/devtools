﻿using System.Runtime.Serialization;

namespace SolvesIT.Automation.Templating.Model
{
    [DataContract]
    public sealed class Parameter
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
    }
}

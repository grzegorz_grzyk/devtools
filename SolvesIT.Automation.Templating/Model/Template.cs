﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SolvesIT.Automation.Templating.Model
{
    [DataContract]
    public sealed class Template
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 5)]
        public string Collection { get; set; }

        [DataMember(Order = 6)]
        public IEnumerable<Parameter> Parameters { get; set; }

        [DataMember(Order = 2)]
        public string BodyFilePath { get; set; }

        [DataMember(Order = 2)]
        public string Body { get; set; }

        [DataMember(Order = 7)]
        public string TargetFilePath { get; set; }

        [DataMember(Order = 3)]
        public string ParameterType { get; set; }

        [DataMember(Order = 4)]
        public string ItemType { get; set; }

        [DataMember(Order = 1)]
        public IEnumerable<ImportData> Imports { get; set; }
    }
}

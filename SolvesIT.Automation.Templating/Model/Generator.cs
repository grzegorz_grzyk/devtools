﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SolvesIT.Automation.Templating.Model
{
    [DataContract]
    public sealed class Generator
    {
        [DataMember(Order = 5)]
        public IEnumerable<Template> Templates { get; set; }

        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public string Description { get; set; }

        [DataMember(Order = 2)]
        public string StartingTemplateName { get; set; }

        [DataMember(Order = 3)]
        public IEnumerable<AssemblyData> Assemblies { get; set; }

        [DataMember(Order = 4)]
        public IEnumerable<ImportData> Imports { get; set; }
    }
}

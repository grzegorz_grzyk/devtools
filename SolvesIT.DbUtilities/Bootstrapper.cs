﻿using Qdarc.Utilities.Ioc;
using SolvesIT.DbAbstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities
{
    public sealed class Bootstrapper : IBootstrapper
    {
        public double Order
        {
            get { return 0; }
        }

        public void Bootstrap(IRegistrar registrar)
        { registrar.RegisterMultiple<IDdObjectReader, AssemblyFileReader>();
            registrar.RegisterMultiple<IDdObjectReader, AssemblyReader>();
            registrar.RegisterMultiple<IDdObjectReader, ColumnReader<DdTable, DdColumn<DdTable>>>();
            registrar.RegisterMultiple<IDdObjectReader, ColumnReader<DdView, DdColumn<DdView>>>();
            registrar.RegisterMultiple<IDdObjectReader, ColumnReader<DdTableType, DdColumn<DdTableType>>>();
            registrar.RegisterMultiple<IDdObjectReader, ForeignKeyReader>();
            registrar.RegisterMultiple<IDdObjectReader, FunctionReader>();
            registrar.RegisterMultiple<IDdObjectReader, IndexColumnReader>();
            registrar.RegisterMultiple<IDdObjectReader, IndexReader>();
            registrar.RegisterMultiple<IDdObjectReader, ParameterReader<DdFunction, DdParameter<DdFunction>>>();
            registrar.RegisterMultiple<IDdObjectReader, ParameterReader<DdProcedure, DdParameter<DdProcedure>>>();
            registrar.RegisterMultiple<IDdObjectReader, PrincipalReader>();
            registrar.RegisterMultiple<IDdObjectReader, ProcedureReader>();
            registrar.RegisterMultiple<IDdObjectReader, SchemaReader>();
            registrar.RegisterMultiple<IDdObjectReader, TableReader>();
            registrar.RegisterMultiple<IDdObjectReader, TypeReader>();
            registrar.RegisterMultiple<IDdObjectReader, ViewReader>();
            registrar.RegisterMultiple<IDdObjectReader, TableTypeReader>();
            registrar.RegisterSingle<ICatalogReader, CatalogReader>();
            registrar.RegisterAsSingleton<IDdRepository, DdRepository>();
            registrar.RegisterAsSingleton<IDdService, DdService>();
           
        }
    }
}

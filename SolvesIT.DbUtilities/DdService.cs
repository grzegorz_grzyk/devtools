﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities
{
    public class DdService
        : IDdService
    {
        private readonly ICatalogReader _catalogReader;

        public DdService(ICatalogReader catalogReader)
        {
            _catalogReader = catalogReader;
        }

        public Task DropAllExceptTables(SqlConnection sqlConnection)
        {
            throw new System.NotImplementedException();
        }

        public async Task<DdCatalog> LoadDatabaseAsync(SqlConnection sqlConnection)
        {
            if (sqlConnection.State != ConnectionState.Open)
            {
                throw new ArgumentException(
                    "Connection state must be open.", "sqlConnection");
            }

            return await _catalogReader.LoadAsync(sqlConnection);
        }
    }
}
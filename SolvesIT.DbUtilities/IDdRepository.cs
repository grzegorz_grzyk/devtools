﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction.DataDefinitionModel;

namespace SolvesIT.DbUtilities
{
    public interface IDdRepository
    {
        Task LoadAsync<TParent, TResult>(
            SqlConnection sqlConnection,
            TParent parent)
            where TResult : DdObject<TParent>
            where TParent : DdObject;
    }
}
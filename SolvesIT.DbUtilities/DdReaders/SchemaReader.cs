﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class SchemaReader
        : DdObjectReader<DdCatalog, DdSchema>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadSchemata_tmpl,
                ExpressionExtensions.GetPropertyName((DdSchema x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdSchema x) => x.Name));

        public SchemaReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdCatalog parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            parent.Schemata = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand);

            foreach (var item in parent.Schemata)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdSchema, DdFunction>(
                    sqlConnection,
                    item);
                await Repository.LoadAsync<DdSchema, DdProcedure>(
                    sqlConnection,
                    item);
                await Repository.LoadAsync<DdSchema, DdTable>(
                    sqlConnection,
                    item);
                await Repository.LoadAsync<DdSchema, DdType>(
                    sqlConnection,
                    item);
                await Repository.LoadAsync<DdSchema, DdTableType>(
                    sqlConnection,
                    item);
                await Repository.LoadAsync<DdSchema, DdView>(
                    sqlConnection,
                    item);
            }
        }
    }
}
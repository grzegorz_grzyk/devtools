﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class ViewReader
        : DdObjectReader<DdSchema, DdView>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadViews_tmpl,
                ExpressionExtensions.GetPropertyName((DdView x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdView x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdView x) => x.PrincipalId));

        public ViewReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdSchema parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Views = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.Views)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdView, DdColumn<DdView>>(
                    sqlConnection,
                    item);
                ////await DdlService.LoadAsync<DdlView, DdlIndex<DdlView>>(item);
                ////await DdlService.LoadAsync<DdlView, DdlTrigger<DdlView>>(item);
            }
        }
    }
}
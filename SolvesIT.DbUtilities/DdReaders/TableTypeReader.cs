﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class TableTypeReader
        : DdObjectReader<DdSchema, DdTableType>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadTableTypes_tmpl,
                ExpressionExtensions.GetPropertyName((DdTable x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdTable x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdTable x) => x.PrincipalId));

        public TableTypeReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdSchema parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.TableTypes = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.TableTypes)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdTableType, DdColumn<DdTableType>>(
                    sqlConnection,
                    item);
            }
        }
    }
}
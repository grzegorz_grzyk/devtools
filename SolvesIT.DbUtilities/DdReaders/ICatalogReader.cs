﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction.DataDefinitionModel;

namespace SolvesIT.DbUtilities.DdReaders
{
    public interface ICatalogReader
    {
        Task<DdCatalog> LoadAsync(SqlConnection sqlConnection);
    }
}
﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class ProcedureReader
        : DdObjectReader<DdSchema, DdProcedure>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadProcedures_tmpl,
                ExpressionExtensions.GetPropertyName((DdProcedure x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdProcedure x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdProcedure x) => x.Definition));

        public ProcedureReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdSchema parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Procedures = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.Procedures)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdProcedure, DdParameter<DdProcedure>>(
                    sqlConnection,
                    item);
            }
        }
    }
}
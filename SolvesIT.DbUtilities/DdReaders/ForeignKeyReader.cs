﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class ForeignKeyReader
        : DdObjectReader<DdTable, DdForeignKey>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadForeignKeys_tmpl,
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.IsDisabled),
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.ParentColumnId),
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.ReferenceTableId),
                ExpressionExtensions.GetPropertyName((DdForeignKey x) => x.ReferenceColumnId));

        public ForeignKeyReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdTable parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.ForeignKeys = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);
            foreach (var item in parent.ForeignKeys)
            {
                item.Parent = parent;
            }
        }
    }
}
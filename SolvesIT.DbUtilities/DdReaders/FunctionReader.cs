﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class FunctionReader
        : DdObjectReader<DdSchema, DdFunction>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadFunctions_tmpl,
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.FunctionType),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.AssemblyId),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.AssemblyClass),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.AssemblyMethod),
                ExpressionExtensions.GetPropertyName((DdFunction x) => x.Definition));

        public FunctionReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdSchema parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Functions = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);
            foreach (var item in parent.Functions)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdFunction, DdParameter<DdFunction>>(
                    sqlConnection,
                    item);
            }
        }
    }
}
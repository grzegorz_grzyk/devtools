﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class ParameterReader<TParent, TResult>
        : DdObjectReader<TParent, TResult>
        where TParent : DdObject<DdSchema>, IHaveParameters<TParent>
        where TResult : DdParameter<TParent>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadParameters_tmpl,
                ExpressionExtensions.GetPropertyName((TResult x) => x.Id),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Name),
                ExpressionExtensions.GetPropertyName((TResult x) => x.TypeId),
                ExpressionExtensions.GetPropertyName((TResult x) => x.MaxLength),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Precision),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Scale),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsOutput),
                ExpressionExtensions.GetPropertyName((TResult x) => x.HasDefaultValue),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsXmlDocument),
                ExpressionExtensions.GetPropertyName((TResult x) => x.DefaultValues),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsReadOnly),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsNullable));

        public ParameterReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            TParent parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Parameters = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.Parameters)
            {
                item.Parent = parent;
            }
        }
    }
}
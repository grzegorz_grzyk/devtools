﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class ColumnReader<TParent, TResult>
        : DdObjectReader<TParent, TResult>
        where TParent : DdObject<DdSchema>, IHaveColumns<TParent>
        where TResult : DdColumn<TParent>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadColumns_tmpl,
                ExpressionExtensions.GetPropertyName((TResult x) => x.Id),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Name),
                ExpressionExtensions.GetPropertyName((TResult x) => x.TypeId),
                ExpressionExtensions.GetPropertyName((TResult x) => x.MaxLength),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Default),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Precision),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Scale),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsNullable),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsIdentity),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsComputed),
                ExpressionExtensions.GetPropertyName((TResult x) => x.Definition),
                ExpressionExtensions.GetPropertyName((TResult x) => x.IsPersisted));

        public ColumnReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            TParent parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Columns = await ReadDdObjectsAsync(sqlConnection, SqlCommand, parameters);

            foreach (var item in parent.Columns)
            {
                item.Parent = parent;
            }
        }
    }
}
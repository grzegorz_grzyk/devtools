﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class PrincipalReader
        : DdObjectReader<DdCatalog, DdPrincipal>
    {
        private static readonly string SqlCommand =
    string.Format(
        CultureInfo.InvariantCulture,
        Resources.ReadPrincipals_tmpl,
        ExpressionExtensions.GetPropertyName((DdPrincipal x) => x.Id),
        ExpressionExtensions.GetPropertyName((DdPrincipal x) => x.Name),
        ExpressionExtensions.GetPropertyName((DdPrincipal x) => x.SchemaId));

        public PrincipalReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdCatalog parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            parent.Principals = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand);

            foreach (var column in parent.Principals)
            {
                column.Parent = parent;
            }
        }
    }
}
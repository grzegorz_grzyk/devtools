﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal abstract class DdObjectReader<TParent, TResult>
        : IDdObjectReader
        where TParent : DdObject
        where TResult : DdObject<TParent>
    {
        public Type HandledType
        {
            get { return typeof(TResult); }
        }

        protected IDdRepository Repository { get; private set; }

        protected DdObjectReader(
            IDdRepository repository)
        {
            Repository = repository;
        }

        async Task IDdObjectReader.LoadAsync(
            SqlConnection sqlConnection,
            object parent)
        {
            await ReadObjectsAsync(
                sqlConnection,
                (TParent)parent);
        }

        protected abstract Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            TParent parent);

        protected async Task<IEnumerable<TResult>> ReadDdObjectsAsync(
            SqlConnection sqlConnection,
            string commandText,
            IEnumerable<SqlParameter> commandParameters = null)
        {
            var result = new List<TResult>();

            using (var command = new SqlCommand(commandText, sqlConnection))
            {
                if (commandParameters != null)
                {
                    var sqlParameters = commandParameters.ToArray();
                    command.Parameters.AddRange(sqlParameters);
                }

                using (var reader = await command.ExecuteReaderAsync())
                {
                    var columnsNames = typeof(TResult).GetProperties(
                            BindingFlags.Instance |
                            BindingFlags.Public |
                            BindingFlags.DeclaredOnly)
                        .Where(
                            p => p.SetMethod != null
                                 && !(p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                        .Select(p => p.Name)
                        .ToArray();

                    while (await reader.ReadAsync())
                    {
                        var dataObject = (TResult)Activator.CreateInstance(typeof(TResult), reader[ExpressionExtensions.GetPropertyName((DdObject x) => x.Name)]);
                        dataObject.Id = (int)reader[ExpressionExtensions.GetPropertyName((DdObject x) => x.Id)];
                       
                        foreach (var columnName in columnsNames)
                        {
                            var columnValue = reader[columnName] != DBNull.Value ? reader[columnName] : null;
                            dataObject
                                .GetType()
                                .GetProperty(columnName)
                                .SetValue(dataObject, columnValue);
                        }

                        result.Add(dataObject);
                    }

                    return result;
                }
            }
        }
    }
}
﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class AssemblyFileReader
        : DdObjectReader<DdAssembly, DdAssemblyFile>
    {
        private static readonly string SqlCommand =
            string.Format(
            CultureInfo.InvariantCulture,
            Resources.ReadAssemblyFiles_tmpl,
            ExpressionExtensions.GetPropertyName((DdAssemblyFile x) => x.Id),
            ExpressionExtensions.GetPropertyName((DdAssemblyFile x) => x.Name),
            ExpressionExtensions.GetPropertyName((DdAssemblyFile x) => x.Content));

        public AssemblyFileReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdAssembly parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.AssemblyFiles = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.AssemblyFiles)
            {
                item.Parent = parent;
            }
        }
    }
}
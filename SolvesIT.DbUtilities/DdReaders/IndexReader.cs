﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class IndexReader
        : DdObjectReader<DdTable, DdIndex>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadIndexes_tmpl,
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.Type),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.TypeDescription),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.IsUnique),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.IsPrimaryKey),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.IsUniqueConstraint),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.IsDisabled),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.HasFilter),
                ExpressionExtensions.GetPropertyName((DdIndex x) => x.FilterDefinition));

        public IndexReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdTable parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Indexes = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.Indexes)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdIndex, DdIndexColumn>(
                    sqlConnection,
                    item);
            }
        }
    }
}
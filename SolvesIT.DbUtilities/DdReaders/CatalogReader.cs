﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class CatalogReader : ICatalogReader
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadCatalog_tmpl,
                ExpressionExtensions.GetPropertyName((DdCatalog x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdCatalog x) => x.Name));

        private readonly IDdRepository _repository;

        public CatalogReader(
            IDdRepository repository)
        {
            _repository = repository;
        }

        async Task<DdCatalog> ICatalogReader.LoadAsync(SqlConnection sqlConnection)
        {
            int catalogId;
            string catalogName;
            using (var sqlCommand = new SqlCommand(SqlCommand, sqlConnection))
            {
                using (var reader = await sqlCommand.ExecuteReaderAsync())
                {
                    await reader.ReadAsync();
                    catalogId = (int)reader[ExpressionExtensions.GetPropertyName((DdCatalog x) => x.Id)];
                    catalogName = (string)reader[ExpressionExtensions.GetPropertyName((DdCatalog x) => x.Name)];
                }
            }

            var catalog = new DdCatalog(catalogName) { Id = catalogId };
            await _repository.LoadAsync<DdCatalog, DdAssembly>(sqlConnection, catalog);
            await _repository.LoadAsync<DdCatalog, DdPrincipal>(sqlConnection, catalog);
            await _repository.LoadAsync<DdCatalog, DdSchema>(sqlConnection, catalog);
            return catalog;
        }
    }
}
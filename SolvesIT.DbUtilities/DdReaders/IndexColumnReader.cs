﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class IndexColumnReader
        : DdObjectReader<DdIndex, DdIndexColumn>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadIndexColumns_tmpl,
                ExpressionExtensions.GetPropertyName((DdIndexColumn x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdIndexColumn x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdIndexColumn x) => x.ColumnId),
                ExpressionExtensions.GetPropertyName((DdIndexColumn x) => x.IsDescendingKey),
                ExpressionExtensions.GetPropertyName((DdIndexColumn x) => x.IsIncludedColumn));

        public IndexColumnReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdIndex parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@TableId", parent.Parent.Id),
                new SqlParameter("@parentId", parent.Id)
            };

            parent.IndexColumns = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.IndexColumns)
            {
                item.Parent = parent;
            }
        }
    }
}
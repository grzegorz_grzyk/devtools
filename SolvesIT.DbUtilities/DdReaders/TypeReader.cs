﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class TypeReader
        : DdObjectReader<DdSchema, DdType>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadTypes_tmpl,
                ExpressionExtensions.GetPropertyName((DdType x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdType x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdType x) => x.PrincipalId),
                ExpressionExtensions.GetPropertyName((DdType x) => x.MaxLength),
                ExpressionExtensions.GetPropertyName((DdType x) => x.Precision),
                ExpressionExtensions.GetPropertyName((DdType x) => x.Scale),
                ExpressionExtensions.GetPropertyName((DdType x) => x.IsNullable),
                ExpressionExtensions.GetPropertyName((DdType x) => x.IsUserDefined),
                ExpressionExtensions.GetPropertyName((DdType x) => x.IsAssemblyType),
                ExpressionExtensions.GetPropertyName((DdType x) => x.Default),
                ExpressionExtensions.GetPropertyName((DdType x) => x.IsTableType),
                ExpressionExtensions.GetPropertyName((DdType x) => x.ParentTypeId));

        public TypeReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdSchema parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            var parameters = new[]
            {
                new SqlParameter("@parentId", parent.Id)
            };

            parent.Types = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand,
                parameters);

            foreach (var item in parent.Types)
            {
                item.Parent = parent;
            }
        }
    }
}
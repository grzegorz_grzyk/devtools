﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading.Tasks;
using Qdarc.Utilities;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.Properties;

namespace SolvesIT.DbUtilities.DdReaders
{
    internal class AssemblyReader
        : DdObjectReader<DdCatalog, DdAssembly>
    {
        private static readonly string SqlCommand =
            string.Format(
                CultureInfo.InvariantCulture,
                Resources.ReadAssemblies_tmpl,
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.Id),
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.Name),
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.PrincipalId),
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.Permission),
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.IsVisible),
                ExpressionExtensions.GetPropertyName((DdAssembly x) => x.ReferenceAssemblyId));

        public AssemblyReader(IDdRepository repository)
            : base(repository)
        {
        }

        protected override async Task ReadObjectsAsync(
            SqlConnection sqlConnection,
            DdCatalog parent)
        {
            if (parent == null)
                throw new ArgumentNullException("parent");

            parent.Assemblies = await ReadDdObjectsAsync(
                sqlConnection,
                SqlCommand);

            foreach (var item in parent.Assemblies)
            {
                item.Parent = parent;
                await Repository.LoadAsync<DdAssembly, DdAssemblyFile>(
                    sqlConnection,
                    item);
            }
        }
    }
}
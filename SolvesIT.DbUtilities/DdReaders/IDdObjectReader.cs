﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SolvesIT.DbUtilities.DdReaders
{
    public interface IDdObjectReader
    {
        Type HandledType { get; }

        Task LoadAsync(
            SqlConnection sqlConnection,
            object parent);
    }
}
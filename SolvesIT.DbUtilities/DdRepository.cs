﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using SolvesIT.DbAbstraction.DataDefinitionModel;
using SolvesIT.DbUtilities.DdReaders;

namespace SolvesIT.DbUtilities
{
    internal class DdRepository : IDdRepository
    {
        private readonly Func<IEnumerable<IDdObjectReader>> _objectReadersFactory;

        private Dictionary<Type, IDdObjectReader> _objectReaders;

        public DdRepository(
            Func<IEnumerable<IDdObjectReader>> objectReadersFactory)
        {
            _objectReadersFactory = objectReadersFactory;
        }

        private Dictionary<Type, IDdObjectReader> DdObjectReaders
        {
            get
            {
                return _objectReaders ?? (_objectReaders = _objectReadersFactory()
                    .ToDictionary(x => x.HandledType, x => x));
            }
        }

        public async Task LoadAsync<TParent, TResult>(
            SqlConnection sqlConnection,
            TParent parent)
            where TResult : DdObject<TParent>
            where TParent : DdObject
        {
            var resultType = typeof(TResult);
            await DdObjectReaders[resultType].LoadAsync(sqlConnection, parent);
        }
    }
}
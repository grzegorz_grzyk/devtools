SELECT
    [a].[assembly_id] As [{0}],
    [a].[name] AS [{1}],
    [a].[principal_id] AS [{2}],
    REPLACE([a].[permission_set_desc],'_ACCESS','') AS [{3}],
    [a].[is_visible] AS [{4}],
    [ar].[referenced_assembly_id] AS [{5}]
FROM sys.assemblies AS [a]
LEFT JOIN sys.assembly_references as [ar] on [ar].[assembly_id] = [a].[assembly_id]
WHERE [a].[is_user_defined] = 1
SELECT
    [fk].[object_id] AS [{0}],
    [fk].[name] AS [{1}],
    [fk].[is_disabled] AS [{2}],
    [fkc].[parent_column_id] AS [{3}],
    [fk].[referenced_object_id] AS [{4}],
    [fkc].[referenced_column_id] AS [{5}]
FROM sys.foreign_keys AS [fk]
LEFT JOIN sys.foreign_key_columns AS [fkc] ON [fkc].[constraint_object_id] = [fk].[object_id]
WHERE [fk].[parent_object_id] = @parentId
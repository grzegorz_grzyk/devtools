﻿SELECT
    [v].[object_id] AS [{0}]
  , [v].[name] AS [{1}]
  , [v].[principal_id] AS [{2}]
FROM [sys].[views] AS [v]
WHERE [v].[schema_id] = @parentId;
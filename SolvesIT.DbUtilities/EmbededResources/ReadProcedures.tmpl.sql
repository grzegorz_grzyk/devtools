﻿SELECT
    [p].object_id AS [{0}]
  , [p].[name] AS [{1}]
  , [asm].[definition] AS [{2}]
FROM [sys].[procedures] AS [p]
JOIN [sys].[sql_modules] AS [asm] ON [asm].object_id = [p].object_id
WHERE [p].schema_id = @parentId;
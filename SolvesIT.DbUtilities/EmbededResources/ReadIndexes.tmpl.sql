SELECT
    [i].[index_id] AS [{0}]
  , COALESCE([i].[name], 'HEAP') AS [{1}]
  , [i].[type] AS [{2}]
  , [i].[type_desc] AS [{3}]
  , [i].[is_unique] AS [{4}]
  , [i].[is_primary_key] AS [{5}]
  , [i].[is_unique_constraint] AS [{6}]
  , [i].[is_disabled] AS [{7}]
  , [i].[has_filter] AS [{8}]
  , [i].[filter_definition] AS [{9}]
FROM [sys].[indexes] AS [i]
WHERE [i].[object_id] = @parentId;
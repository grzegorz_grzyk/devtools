SELECT
    [s].[schema_id] AS [{0}]
  , [s].[name] AS [{1}]
FROM [sys].[schemas] s
WHERE ([s].[schema_id] > 4 OR [s].[name] in ('dbo', 'sys'))
  AND [s].[schema_id] < 16384
SELECT
    [dp].[principal_id] AS [{0}]
  , [dp].[name] AS [{1}]
  , [s].[schema_id] As [{2}]
FROM sys.database_principals AS [dp]
INNER JOIN sys.schemas AS [s] ON [s].[name] = [dp].[default_schema_name];
﻿SELECT
    [t].[object_id] AS [{0}]
  , [t].[name] AS [{1}]
  , [t].[principal_id] AS [{2}]
FROM [sys].[tables] AS [t]
WHERE [t].[schema_id] = @parentId;
﻿SELECT
    [tt].[type_table_object_id] AS [{0}]
  , [tt].[name] AS [{1}]
  , [tt].[principal_id] AS [{2}]
FROM [sys].[table_types] AS [tt]
WHERE [tt].[schema_id] = @parentId;
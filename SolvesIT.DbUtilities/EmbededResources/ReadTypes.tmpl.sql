﻿SELECT
    [t].[user_type_id] AS [{0}]
  , [t].[name] AS [{1}]
  , [t].[principal_id] AS [{2}]
  , [t].[max_length] AS [{3}]
  , [t].[precision] AS [{4}]
  , [t].[scale] AS [{5}]
  , [t].[is_nullable] AS [{6}]
  , [t].[is_user_defined] AS [{7}]
  , [t].[is_assembly_type] AS [{8}]
  , OBJECT_DEFINITION([t].[default_object_id])AS [{9}]
  , [t].[is_table_type] AS [{10}]
  , IIF ([t].[system_type_id] = [t].[user_type_id] OR [t].[is_assembly_type] = 1, null, [t].[system_type_id]) AS [{11}]
FROM [sys].[types] AS [t]
WHERE [t].[is_table_type] = 0
  AND [t].schema_id = @ParentId;
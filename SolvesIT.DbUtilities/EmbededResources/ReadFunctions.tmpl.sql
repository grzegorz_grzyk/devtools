SELECT
    [o].[object_id] AS [{0}],
    [o].[Name] AS [{1}],
    [o].[type] AS [{2}],
    [am].[assembly_id] AS [{3}],
    [am].[assembly_class] AS [{4}],
    [am].[assembly_method] AS [{5}],
    [sm].[definition] as [{6}]
FROM sys.objects as [o]
LEFT JOIN sys.sql_modules as [sm] ON [sm].[object_id]= [o].[object_id]
LEFT JOIN sys.assembly_modules AS [am] ON [am].[object_id] = [o].[object_id]
LEFT JOIN sys.assemblies AS [a] ON [a].[assembly_id] = [am].[assembly_id]
WHERE [o].[type] IN (N'AF', N'FN', N'FS', N'FT', N'IF', N'TF')
  AND [o].[schema_id] = @parentId
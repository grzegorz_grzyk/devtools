SELECT
    [file_id] As [{0}],
    name As [{1}],
    content AS [{2}]
FROM sys.assembly_files
WHERE assembly_id = @parentId
﻿SELECT
    [ic].[index_column_id] AS [{0}]
  , '' AS [{1}]
  , [ic].[column_id] AS [{2}]
  , [ic].[is_descending_key] AS [{3}]
  , [ic].[is_included_column] AS [{4}]
FROM [sys].[index_columns] AS [ic]
WHERE [ic].[index_id] = @ParentId
  AND [ic].object_id = @TableId;